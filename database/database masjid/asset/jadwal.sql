-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:18 PM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `judul`, `keterangan`, `tanggal`, `hari`, `masjid_id`) VALUES
(32, 'jumatan', 'jumat', '2018-01-01 03:00:00', 'Friday', 1),
(38, 'ngaji', 'ngaji bersama', '2018-01-01 09:10:00', 'Sunday', 28),
(39, 'ngaji gambar', 'ngaji gambar', '2018-01-01 12:30:00', 'Thursday', 28),
(40, 'tes', 'tes', '2018-01-01 09:00:00', 'Friday', 28),
(41, 'tes', 'tes', '2018-01-01 09:00:00', 'Sunday', 28);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
