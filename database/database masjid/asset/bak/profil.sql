-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2018 at 10:10 AM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `nama`, `email`, `jenis`, `password`, `fcm_token`, `regencies_id`, `masjid_id`) VALUES
(1, 'user', 'user', 'user', 'user', 'cTlsb6666rA:APA91bFLy1yx8Rn8drPtjyAjIyjP8g0hWdk3sCxshe4uqa8VlyjFxVdcDUzD4UfkC7daJEZXoAYd0fXnMzLtz1enVI8z1vqfXGedPkjJf9-MTr05vwLnHxUckoTNbO7jMKACdYJaeFET', 3578, NULL),
(2, 'masjid', 'masjid', 'masjid', 'masjid', 'cTlsb6666rA:APA91bFLy1yx8Rn8drPtjyAjIyjP8g0hWdk3sCxshe4uqa8VlyjFxVdcDUzD4UfkC7daJEZXoAYd0fXnMzLtz1enVI8z1vqfXGedPkjJf9-MTr05vwLnHxUckoTNbO7jMKACdYJaeFET', 3578, 1),
(3, 'alharam', 'alharam', 'masjid', 'alharam', '', 3578, 2),
(8, 'user3', 'user3', 'user', 'user3', '', 3578, NULL),
(11, 'user2', 'user2', 'user', 'user2', 'cTlsb6666rA:APA91bFLy1yx8Rn8drPtjyAjIyjP8g0hWdk3sCxshe4uqa8VlyjFxVdcDUzD4UfkC7daJEZXoAYd0fXnMzLtz1enVI8z1vqfXGedPkjJf9-MTr05vwLnHxUckoTNbO7jMKACdYJaeFET', 3578, NULL),
(13, 'text', 'text', 'user', 'text', NULL, 5203, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
