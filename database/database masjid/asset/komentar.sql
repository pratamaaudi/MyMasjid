-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:21 PM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id`, `komentar`, `profil_id`, `status_id`, `hapus`) VALUES
(13, 'tes', 1, 1, 0),
(14, 'tes', 1, 1, 0),
(15, 'tes', 1, 1, 0),
(16, 'tes', 1, 1, 0),
(17, 'tes', 1, 1, 0),
(18, 'tes', 1, 1, 0),
(19, 'tes', 1, 1, 0),
(20, 'tes', 1, 1, 0),
(21, 'tes', 1, 1, 0),
(22, 'audi', 1, 1, 0),
(25, 'tes', 1, 1, 0),
(26, 'bla', 1, 1, 0),
(27, 'ya', 1, 1, 0),
(28, 'ga', 1, 1, 0),
(33, 'coba komen user 2', 11, 2, 0),
(34, 'oke', 1, 2, 0),
(39, 'tes', 1, 2, 0),
(43, '1', 1, 2, 1),
(56, '3', 1, 2, 0),
(57, '3', 1, 2, 1),
(58, '4', 1, 2, 0),
(59, 'coba komen', 11, 7, 1),
(60, 'tes', 1, 11, 0),
(61, 'coba lagi', 1, 11, 0),
(62, 'saya ikur', 1, 12, 1),
(63, 'tes lagi', 1, 12, 1),
(64, 'rrrr', 11, 12, 0),
(65, 'tes a', 25, 12, 0),
(66, 'apa ini ?', 25, 18, 1),
(67, 'tes', 23, 18, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
