-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:27 PM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `topik`, `tanggal`, `masjid_id`) VALUES
(1, 'test status 1', 'test topik', '2017-11-26 00:00:00', 1),
(2, 'test status 2', 'coba topik', '2017-11-30 00:00:00', 1),
(7, 'coba', 'lainnya', '2018-01-24 15:03:00', 1),
(8, 'coba isi', 'coba topik', '2018-01-24 15:07:00', 1),
(9, 'coba isi', 'coba topik', '2018-01-24 15:09:00', 1),
(11, 'coba topik lainnya', 'topik lainnya', '2018-02-19 09:14:00', 1),
(12, 'diharapkan datang tepat waktu', 'sholat jumat', '2018-02-22 13:06:00', 1),
(15, 'tes', 'hari besar islam', '2018-03-14 20:00:00', 28),
(18, '', 'pengajian rutin', '2018-03-14 20:06:00', 28);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
