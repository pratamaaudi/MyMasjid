-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:24 PM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `lapor`
--

INSERT INTO `lapor` (`id`, `keterangan`, `status`, `masjid_id`, `profil_id`, `komentar_id`) VALUES
(1, 'coba keterangan', 'selesai', 1, 1, 33),
(2, 'coba lapor', 'selesai', 1, 1, 59),
(3, 'coba lapor', 'selesai', 1, 11, 43),
(4, 'lapor', 'proses', 1, 11, 60),
(5, 'spam', 'proses', 1, 11, 61),
(6, 'coba lapor', 'proses', 1, 11, 63),
(7, 'coba lapor', 'proses', 1, 11, 63),
(8, 'lapor', 'proses', 1, 11, 60),
(9, 'tes aaa', 'proses', 1, 25, 64),
(10, 'bego nih', 'proses', 28, 1, 66);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
