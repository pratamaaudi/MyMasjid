-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:24 PM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `nama`, `email`, `jenis`, `password`, `fcm_token`, `masjid_id`, `districts_id`) VALUES
(1, 'user', 'user', 'user', 'user', 'efhl8jWA-T8:APA91bF2lUYI6YeT8EDn5ihD8h6MAnjbu6LK8cFeV4F6680_y6eZYl8GfjANuXp8YmoFg-n9xA0O1i1DJEUkq0E_lHtBz1lr8eRmWaPBaxm2mKfq4ALzRyVYrLLIvPCbhbAozZzlDObq', NULL, 3578070),
(2, 'masjid', 'masjid', 'masjid', 'masjid', 'efhl8jWA-T8:APA91bF2lUYI6YeT8EDn5ihD8h6MAnjbu6LK8cFeV4F6680_y6eZYl8GfjANuXp8YmoFg-n9xA0O1i1DJEUkq0E_lHtBz1lr8eRmWaPBaxm2mKfq4ALzRyVYrLLIvPCbhbAozZzlDObq', 1, 3578070),
(3, 'alharam', 'alharam', 'masjid', 'alharam', '', 2, 3578070),
(8, 'user3', 'user3', 'user', 'user3', '', NULL, 3578070),
(11, 'user2', 'user2', 'user', 'user2', 'efhl8jWA-T8:APA91bF2lUYI6YeT8EDn5ihD8h6MAnjbu6LK8cFeV4F6680_y6eZYl8GfjANuXp8YmoFg-n9xA0O1i1DJEUkq0E_lHtBz1lr8eRmWaPBaxm2mKfq4ALzRyVYrLLIvPCbhbAozZzlDObq', NULL, 3578070),
(13, 'text', 'text', 'user', 'text', 'clxKGWAvwj8:APA91bF_yxlg1Phov-uguPXPF-_ps1Grsa5sy210a6qHwfV4mS-o_gEs8xV8FqZcB_i4foEAerm5kCf_4JntFOu4NYt11EcmahXx7op4KSC6WLLBWAmrQnHVhjjeiuDT8ToEuZ4rKeHb', NULL, 3578070),
(14, 'super', 'super', 'superadmin', 'super', 'fz7NR0P-j-s:APA91bHardZ_A74_vInU2vgUrc4y_LDDocUj_U9Xo_xwi49tNtav1-L1ySpUz3Mp4dD0S3qasp46_KpWVUJufubR_fVs2fn3j_t03yYycvBBhYs1zuukgbdRFVXPNPqVe7NzpnehdipE', NULL, 3578070),
(23, 'alfalah', 'alfalah@gmail.com', 'masjid', 'alfalah', 'efhl8jWA-T8:APA91bF2lUYI6YeT8EDn5ihD8h6MAnjbu6LK8cFeV4F6680_y6eZYl8GfjANuXp8YmoFg-n9xA0O1i1DJEUkq0E_lHtBz1lr8eRmWaPBaxm2mKfq4ALzRyVYrLLIvPCbhbAozZzlDObq', 28, 3515110),
(24, 'a', 'a', 'user', 'a', NULL, NULL, 1406060),
(25, 'audi', 'audi@gmail.com', 'user', 'audi', 'efhl8jWA-T8:APA91bF2lUYI6YeT8EDn5ihD8h6MAnjbu6LK8cFeV4F6680_y6eZYl8GfjANuXp8YmoFg-n9xA0O1i1DJEUkq0E_lHtBz1lr8eRmWaPBaxm2mKfq4ALzRyVYrLLIvPCbhbAozZzlDObq', NULL, 1403051);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
