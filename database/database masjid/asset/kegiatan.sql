-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 03:18 PM
-- Server version: 5.5.35-1ubuntu1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mymasjid`
--

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `nama`, `tanggal_mulai`, `tanggal_selesai`, `deskripsi`, `hapus`, `masjid_id`) VALUES
(1, 'test kegiatan', '2017-10-24 07:00:00', '2017-10-31 07:00:00', '', 1, 1),
(2, 'test kegiatan 2', '2018-01-05 03:00:00', '2018-01-09 03:00:00', '', 1, 2),
(3, 'kegiatan 3 bersama 3', '2018-11-15 00:00:00', '2018-12-20 00:00:00', 'coba deskripsi', 0, 1),
(4, 'kegiatan expired', '2017-11-01 00:00:00', '2017-11-08 00:00:00', '', 0, 1),
(17, 'coba', '2017-12-05 00:00:00', '2017-12-28 00:00:00', 'coba', 0, 1),
(18, 'coba2', '2017-12-20 00:00:00', '2017-12-30 00:00:00', 'coba\n2\n2\n2\n2\n2', 0, 1),
(19, '2018-2-20', '0000-00-00 00:00:00', '2018-02-21 00:00:00', 'coba kegiatan', NULL, 1),
(20, 'ngopi', '2018-02-21 00:00:00', '2018-02-23 00:00:00', 'ngopi', 1, 1),
(21, 'ngopi', '2018-02-20 00:00:00', '2018-02-21 00:00:00', 'ngopi', NULL, 1),
(22, 'ngopi', '2018-02-20 00:00:00', '2018-02-21 00:00:00', 'ngopi', NULL, 1),
(23, 'coba jam edit', '2018-02-23 01:50:00', '2018-02-24 15:00:00', 'coba jam edit', NULL, 1),
(24, 'coba cleaning', '2018-02-22 06:00:00', '2018-02-23 09:00:00', 'coba cleaning', NULL, 1),
(26, 'doa bersama', '2018-03-16 09:00:00', '2018-03-16 19:30:00', 'doa untuk bapak andi', 0, 28),
(27, 'tes', '2018-03-15 09:30:00', '2018-03-16 07:30:00', 'tes', NULL, 1),
(28, 'a', '2018-03-15 00:00:00', '2018-03-16 00:00:00', 'a', 0, 28);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
