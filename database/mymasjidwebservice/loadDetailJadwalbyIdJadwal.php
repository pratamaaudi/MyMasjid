<?php

include_once './koneksi.php';

$jadwalId = $_POST['jadwalId'];

$sql = "SELECT id, hari, HOUR(tanggal) as 'jam', MINUTE(tanggal) as 'menit' FROM `detail_jadwal` where jadwal_id=$jadwalId and hapus = 0";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

