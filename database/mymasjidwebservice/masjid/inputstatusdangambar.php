<?php

//load file konfigurasi database
include_once './koneksi.php';

//set waktu menjadi wib
date_default_timezone_set("Asia/Jakarta");

//tampung data kiriman dari aplikasi
$msg = str_replace("'", "''", $_POST['msg']);
$mid = $_POST['mid'];
$tpk = str_replace("'", "''", $_POST['tpk']);
$foto = $_POST['foto'];
$tgl = date('Y-m-d H:i');

//persiapkan query yang akan dijalankan
$sql = "INSERT INTO `status` (`id`, `status`, `topik`, `tanggal`, `masjid_id`) VALUES (NULL, '$msg', '$tpk', '$tgl', '$mid');";

//jalankan query dan tampung hasilnya
$result = mysqli_query($conn, $sql);

//cek hasil query dan lanjutkan proses berikutnya
if ($result) {

	$status_id;

    //persiapkan query yang akan dijalankan
	$sql = "SELECT * FROM `status` ORDER BY id DESC LIMIT 1";

	//jalankan query dan tampung hasilnya
	$result2 = $conn->query($sql);

	//cek hasil query dan tampung di variabel
	if ($result2->num_rows > 0) {
		while ($row = $result2->fetch_assoc()) {
			$status_id = $row["id"];
		}

		$lokasigambar = "Foto_Status/" . $status_id . ".jpg";

		//cek hasil query dan tuliskan output database
		if (!file_put_contents($lokasigambar, base64_decode($foto))) {
			echo 'false';
		} else {
			echo 'true';
		}
	}
} else{
	echo "false";
}
?>