<?php
include_once './koneksi.php';

$mid = $_POST['mid'];

$sql = "SELECT p.id, p.nama, p.email, p.jenis, p.masjid_id, pv.name as 'provinsi', r.name as 'kotakab', d.name as 'kecamatan' FROM blokir b inner join profil p on b.profil_id = p.id INNER JOIN districts d on p.districts_id = d.id INNER JOIN regencies r on r.id = d.regency_id INNER JOIN provinces pv on pv.id = r.province_id WHERE b.masjid_id = $mid";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>