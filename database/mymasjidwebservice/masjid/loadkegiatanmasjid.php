<?php
include_once './koneksi.php';

$idm = $_POST['idm'];

$sql = "SELECT k.id, k.nama, DATE(k.tanggal_mulai) as tanggal_mulai, TIME(k.tanggal_mulai) as jam_mulai, DATE(k.tanggal_selesai) as tanggal_selesai, TIME(k.tanggal_selesai) as jam_selesai, m.id as masjid_id, m.nama as 'nama_masjid', k.deskripsi from kegiatan k INNER JOIN masjid m on k.masjid_id = m.id where m.id = $idm and tanggal_selesai > now() and hapus = 0 order by tanggal_mulai DESC";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

