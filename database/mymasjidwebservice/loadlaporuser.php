<?php

include_once './koneksi.php';

$pid = $_POST['pid'];

$sql = "SELECT l.id, l.masjid_id, l.profil_id, l.keterangan,l.status,p2.nama as pelapor, p.nama as terlapor, k.komentar, p.id as idterlapor, k.id as idkomen, k.hapus FROM lapor l inner JOIN komentar k on l.komentar_id = k.id INNER JOIN profil p on k.profil_id = p.id INNER JOIN profil p2 on l.profil_id = p2.id where l.profil_id = $pid order by l.status";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

