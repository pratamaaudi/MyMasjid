<?php

include_once './koneksi.php';

//MARK : tampung kiriman id masjid
$ids = $_POST["ids"];

//MARK : load data dari database
$sql = "SELECT p.nama, k.komentar, p.id as 'idp', k.id as 'idk' FROM komentar k INNER JOIN status s on k.status_id = s.id INNER JOIN profil p on k.profil_id = p.id WHERE s.id = $ids and k.hapus = 0 ORDER BY k.id DESC LIMIT 1";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

