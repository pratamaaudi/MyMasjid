<?php

include_once './koneksi.php';

$idProfil = $_POST["id"];

$sql = "select s.id as statusId, s.status, s.topik, s.tanggal, m.id as masjidId, m.nama from profil p INNER JOIN profil_follow_masjid pfm on p.id = pfm.profil_id INNER JOIN status s on s.masjid_id = pfm.masjid_id INNER JOIN masjid m on pfm.masjid_id = m.id where p.id = $idProfil and s.hapus = 0 GROUP BY s.id order by s.tanggal DESC";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

