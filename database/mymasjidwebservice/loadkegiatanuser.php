<?php
include_once './koneksi.php';

$id = $_POST['id'];

$sql = "Select k.id, k.nama, DATE(k.tanggal_mulai) as tanggal_mulai, TIME(k.tanggal_mulai) as jam_mulai, DATE(k.tanggal_selesai) as tanggal_selesai, TIME(tanggal_selesai) as jam_selesai, k.masjid_id, m.nama as nama_masjid, deskripsi from profil_follow_kegiatan pfk inner join kegiatan k on pfk.kegiatan_id = k.id inner join masjid m on k.masjid_id = m.id where profil_id = $id and tanggal_selesai > now()";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

