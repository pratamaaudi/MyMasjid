 <?php

include_once './koneksi.php';

$idk = $_POST['idk'];

$sql = "select p.nama, p.jenis, p.id, p.email, p.masjid_id, pr.name as 'provinsi', r.name as 'kotakab', d.name as 'kecamatan' from kegiatan INNER JOIN profil_follow_kegiatan on kegiatan.id = profil_follow_kegiatan.kegiatan_id INNER JOIN profil p on profil_follow_kegiatan.profil_id = p.id INNER JOIN districts d on p.districts_id = d.id INNER JOIN regencies r on d.regency_id = r.id INNER JOIN provinces pr on pr.id = r.province_id WHERE kegiatan.id = $idk";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post'=>$rows));
} else {
    echo 'false';
}
?>