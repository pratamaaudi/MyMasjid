<?php

//load file konfigurasi database
include_once './koneksi.php';

//persiapkan query yang akan dijalankan
$sql = "SELECT * FROM `masjid`";

//jalankan query dan tampung hasilnya
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();

    //mengubah hasil query menjadi json
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

