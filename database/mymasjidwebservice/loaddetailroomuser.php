<?php

include_once './koneksi.php';

$idr = $_POST['idr'];

$sql = "SELECT c.profil_id, c.chat, m.nama, m.id, p.fcm_token FROM chat c INNER JOIN room r on c.room_id = r.id INNER JOIN profil_follow_masjid pfm on r.profil_follow_masjid_id = pfm.id INNER JOIN masjid m on pfm.masjid_id = m.id INNER JOIN profil p on m.id = p.masjid_id WHERE r.id = $idr ORDER BY c.id DESC LIMIT 1";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

