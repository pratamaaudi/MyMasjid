<?php

include_once './koneksi.php';

$idProfil = $_POST["id"];

$sql = "select s.id as statusId, s.status, s.topik, s.tanggal, m.id as masjidId, m.nama, MAX(pf.nama) as 'username', MAX(k.komentar) as 'komentar' from profil p INNER JOIN profil_follow_masjid pm on p.id = pm.profil_id INNER JOIN status s on pm.masjid_id = s.masjid_id INNER JOIN masjid m on pm.masjid_id = m.id LEFT JOIN komentar k on s.id = k.status_id left join profil pf on k.profil_id = pf.id WHERE pm.profil_id = $idProfil GROUP BY s.id order by s.tanggal DESC";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

