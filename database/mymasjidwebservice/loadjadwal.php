<?php
//load file konfigurasi database
include_once './koneksi.php';

//tampung data kiriman dari aplikasi
$mid = $_POST['mid'];

//persiapkan query yang akan dijalankan
$sql = "SELECT j.id, judul, keterangan, masjid_id, m.nama FROM jadwal j INNER JOIN masjid m on j.masjid_id = m.id WHERE masjid_id = $mid and j.hapus=0 ORDER BY id DESC";

//jalankan query dan tampung hasilnya
$result = $conn->query($sql);

//mengubah hasil query menjadi json
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>