<?php

include_once './koneksi.php';

$postingId = $_POST["id"];

$sql = "select k.id, k.profil_id, k.komentar, p.nama from komentar k INNER JOIN status s ON k.status_id = s.id "
        . "INNER JOIN profil p on k.profil_id = p.id "
        . "where status_id = '$postingId' and k.hapus = 0 order by k.id";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>