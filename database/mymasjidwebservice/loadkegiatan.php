<?php
include_once './koneksi.php';

$id = $_POST['id'];

$sql = "SELECT k.id, k.nama, DATE(k.tanggal_mulai) as tanggal_mulai, "
        . "TIME(k.tanggal_mulai) as jam_mulai, DATE(k.tanggal_selesai) "
        . "as tanggal_selesai, TIME(k.tanggal_selesai) as jam_selesai, "
        . "pm.masjid_id, m.nama as 'nama_masjid', m.kontak, k.deskripsi FROM kegiatan k "
        . "INNER JOIN masjid m on k.masjid_id = m.id "
        . "INNER JOIN profil_follow_masjid pm on m.id = pm.masjid_id "
        . "WHERE pm.profil_id = '$id' and tanggal_selesai > now() "
        . "order by tanggal_mulai DESC";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>

