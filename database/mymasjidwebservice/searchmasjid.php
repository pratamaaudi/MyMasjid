<?php

include_once './koneksi.php';

$query = str_replace("'", "''", $_POST['query']);

$sql = "SELECT m.id as masjid_id, m.nama as nama, m.alamat, m.kontak FROM masjid m INNER JOIN profil p on m.id = p.masjid_id INNER JOIN districts d on p.districts_id = d.id INNER JOIN regencies r on d.regency_id = r.id INNER JOIN provinces pv on r.province_id = pv.id WHERE m.nama like '%$query%' or m.alamat like '%$query%' or d.name like '%$query%' or r.name like '%$query%' or pv.name LIKE '%$query%'";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    print json_encode(array('post' => $rows));
} else {
    echo 'false';
}
?>