package com.example.audi.mymasjid;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 12/7/2017.
 */

//MARK : class untuk mengurus data di activity detail masjid
public class brainDetailMasjid {
    public classMasjid loadMasjid(String idMasjid) {

        //MARK siapin class masjid penampung data
        classMasjid classMasjid = null;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://103.52.146.34/mymasjid/loadmasjidid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idMasjid", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String nama = c.getString("nama");
                String alamat = c.getString("alamat");
                String kontak = c.getString("kontak");
                classMasjid = new classMasjid(id, nama, alamat, kontak);
            }
        } catch (Exception e) {
        }
        return classMasjid;
    }

}
