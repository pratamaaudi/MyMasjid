package com.example.audi.mymasjid;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 10/7/2017.
 */

public class brain_login {

    public boolean login(String user, String pass) {

        boolean hasil = false;
        if (user.equals("")) {
            hasil = false;
        } else if (pass.equals("")) {
            hasil = false;
        } else {
            String respon;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://103.52.146.34/mymasjid/login.php");
            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("username", user.trim()));
                nameValuePairs.add(new BasicNameValuePair("password", pass.trim()));
                httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
                HttpResponse response = httpClient.execute(httppost);
                respon = EntityUtils.toString(response.getEntity());

                if(respon.equals("true")){
                    hasil=true;
                }

            } catch (Exception e) {
            }
        }
        return hasil;
    }

    public String debughttp() {
        String respon = "blablaba";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://103.52.146.34/mymasjid/login.php");
        //HttpPost httppost = new HttpPost("http://103.52.146.34/penir/penir13/login.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }
        return respon;
    }
}
