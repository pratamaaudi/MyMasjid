package com.example.audi.mymasjid;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class activityBlokir extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blokir);

        //MARK : obj
        final String idMasjid = getIntent().getExtras().getString("mid");

        //region isi arraylist classlogins
        //MARK : obj
        ArrayList<classLogin> classLogins = new ArrayList<>();
        brainBlokir brainBlokir = new brainBlokir();

        //MARK : operation
        classLogins = brainBlokir.loadBlokir(idMasjid, classLogins);
        //endregion

        //region generate recyclerview
        //MARK : UI
        RecyclerView rclBlok = findViewById(R.id.rclBlok);

        //MARK : obj
        final Context context = activityBlokir.this;
        final adapterBlok adapterBlok = new adapterBlok(classLogins, idMasjid, context);

        //MARK : operation
        rclBlok.setLayoutManager(new LinearLayoutManager(context));
        rclBlok.setHasFixedSize(true);
        rclBlok.setAdapter(adapterBlok);
        rclBlok.setNestedScrollingEnabled(true);
        //endregion

        //region constrantnokonten
        //MARK : UI
        final ConstraintLayout constrantnokonten = findViewById(R.id.constrantnokonten);

        //MARK : operation
        if (classLogins.size() < 1)
            constrantnokonten.setVisibility(View.VISIBLE);
        else
            constrantnokonten.setVisibility(View.INVISIBLE);

        //endregion
    }
}
