package com.example.audi.mymasjid;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class adapterBlok extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    //region SetData
    private final ArrayList<classLogin> classLogins;
    private final String idMasjid;
    private final Context context;

    public adapterBlok(ArrayList<classLogin> classLogins, String idMasjid, Context context){
        this.classLogins = classLogins;
        this.idMasjid = idMasjid;
        this.context = context;
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.adapter_blokir, parent, false
        );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : UI
        final TextView txtUsername, txtEmail, txtLokasi;

        //MARK : obj
        final View view = holder.itemView;
        final classLogin classLogin = getClassLogins().get(position);

        //region txtUsername
        //MARK : UI
        txtUsername = view.findViewById(R.id.txtUsername);

        //MARK : operation
        txtUsername.setText(classLogin.getUsername());
        //endregion

        //region txtEmal
        //MARK : UI
        txtEmail = view.findViewById(R.id.txtEmail);

        //MARK : operation
        txtEmail.setText(classLogin.getEmail());
        //endregion

        //region btnUnblok
        //MARK : UI
        final ImageButton btnUnblok = view.findViewById(R.id.btnUnblok);

        //MARK : trigger
        btnUnblok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final brainBlokir brainBlokir = new brainBlokir();

                //MARK : operation
                if(brainBlokir.unblokir(idMasjid,classLogin.getId())) {
                    alert("user berhasil di un-blok");
                    classLogins.remove(position);
                    notifyItemRemoved(position);
                    notifyItemChanged(position, classLogins);
                }
                else
                    alert("user gagal di un-blok");
            }
        });
        //endregion
    }

    @Override
    public int getItemCount() {
        return classLogins.size();
    }

    public ArrayList<classLogin> getClassLogins() {
        return classLogins;
    }

    public String getIdMasjid() {
        return idMasjid;
    }

    public void alert(String msg) {
        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        //MARK : operation
        builder.setMessage(msg).show();
    }

    public Context getContext() {
        return context;
    }
}
