package com.example.audi.mymasjid;

public class classProvinsi {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public classProvinsi(String id, String name){
        setId(id);
        setName(name);
    }
}
