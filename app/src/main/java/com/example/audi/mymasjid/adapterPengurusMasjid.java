package com.example.audi.mymasjid;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class adapterPengurusMasjid extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //region setData
    private ArrayList<classPengurusMasjid> classPengurusMasjids;
    private Context context;
    private Boolean masjid;

    public adapterPengurusMasjid(ArrayList<classPengurusMasjid> classPengurusMasjids, Context context, Boolean masjid) {
        setClassPengurusMasjids(classPengurusMasjids);
        setContext(context);
        setMasjid(masjid);
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_pengurus_masjid, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //region set data pengurus
        //MARK : obj
        final classPengurusMasjid classPengurusMasjid = classPengurusMasjids.get(position);
        final String kontak = classPengurusMasjid.getKntk();

        //MARK : UI
        final View view = holder.itemView;
        final TextView txtNama, txtJbtn, txtKntk;
        //region Textview
        txtNama = view.findViewById(R.id.txtNama);
        txtJbtn = view.findViewById(R.id.txtJbtn);
        txtKntk = view.findViewById(R.id.txtKntk);
        //endregion

        //MARK : operation
        txtNama.setText("Nama : " + classPengurusMasjid.getNama());
        txtJbtn.setText("Sebagai : " + classPengurusMasjid.getJbtn());
        txtKntk.setText("Kontak : " + kontak);
        //endregion

        //region btn call
        //MARK : UI
        final Button btnCall = view.findViewById(R.id.btnCall);

        //MARK : trigger
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : OBJ
                final Intent intent = new Intent(Intent.ACTION_CALL);

                //MARK : operation
                intent.setData(Uri.parse("tel:" + kontak));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    //MARK : obj
                    final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();

                    //MARK : operation
                    alertDialog.setMessage("ada belum menyetujui penggunaan telepon untuk aplikasi ini");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }else{
                    context.startActivity(intent);
                }
            }
        });
        //endregion

        //region if bukan masjid
        //MARK : operation
        if(!masjid){

            //region setButton Hapus
            //MARK : UI
            final FrameLayout frmHapus = view.findViewById(R.id.frmHapus);

            //MARK : operation
            frmHapus.setVisibility(View.GONE);
            //endregion

            //region set weight linear layout
            //MARK : UI
            final LinearLayout llButton = view.findViewById(R.id.llButton);

            //MARK : obj
            final ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 8.0f);

            //MARK : operation
            llButton.setLayoutParams(layoutParams);
            //endregion
        }
        //endregion
    }

    @Override
    public int getItemCount() {
        return getClassPengurusMasjids().size();
    }

    public ArrayList<classPengurusMasjid> getClassPengurusMasjids() {
        return classPengurusMasjids;
    }

    public void setClassPengurusMasjids(ArrayList<classPengurusMasjid> classPengurusMasjids) {
        this.classPengurusMasjids = classPengurusMasjids;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Boolean getMasjid() {
        return masjid;
    }

    public void setMasjid(Boolean masjid) {
        this.masjid = masjid;
    }
}
