package com.example.audi.mymasjid;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;

import java.util.ArrayList;

/**
 * Created by MOFOSEFES on 10/15/2017.
 */

public class adapterKomentar extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private String profil_id;
    private ArrayList<classKomentar> classKomentars;
    private String idMasjid;
    private Boolean masjid;
    String temp;

    public adapterKomentar(Context context, ArrayList<classKomentar> classKomentars, String profil_id, String idMasjid, Boolean masjid) {
        setContext(context);
        setClassKomentars(classKomentars);
        setProfil_id(profil_id);
        setIdMasjid(idMasjid);
        setMasjid(masjid);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.adapter_komentar, parent, false
        );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : obj
        final View view = holder.itemView;
        final classKomentar classKomentar = classKomentars.get(position);
        final String username, idProfil, komentar, idKomen;
        //region String
        username = classKomentar.getUsername();
        idProfil = classKomentar.getProfil_id();
        komentar = classKomentar.getKomentar();
        idKomen = classKomentar.getIdkomen();
        //endregion

        //region txtUsername
        //MARK : UI
        TextView txtUsername = view.findViewById(R.id.txtUsername);

        //MARK : operation
        txtUsername.setText(classKomentars.get(position).getUsername());
        //endregion

        //region edtKomen
        //MARK : UI
        final EditText edtKomen = view.findViewById(R.id.edtKomen);

        //MARK : operation
        edtKomen.setText(komentar);
        edtKomen.setFocusable(false);
        //endregion

        //region imgmore

        //region setmenu
        //MARK : obj
        final Integer menu;

        //MARK : operation
        if (getProfil_id().equals(idProfil)) {
            menu = R.menu.komentar_drop_down;

        } else if (masjid) {
            menu = R.menu.komentar_drop_down_masjid;
        } else {
            menu = R.menu.komentar_lapor;
        }
        //endregion

        //region setPopup
        //MARK : UI
        final ImageView imgMore = view.findViewById(R.id.imgMore);

        //MARK : obj
        temp = "";
        final DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(context, imgMore);
        final DroppyMenuPopup droppyMenu = droppyBuilder.fromMenu(menu)
                .triggerOnAnchorClick(false)
                .setOnClick(new DroppyClickCallbackInterface() {
                    @Override
                    public void call(View v, final int id) {
                        switch (id) {
                            case R.id.edit:
                                if (temp.equals("")) {

                                    //region unlock edtkomen
                                    //MARK : obj
                                    temp = edtKomen.getText().toString();

                                    //MARK : operation
                                    edtKomen.setFocusableInTouchMode(true);
                                    //endregion

                                    //region btnEdit
                                    //MARK : UI
                                    final ImageButton ibtnEdit = view.findViewById(R.id.ibtnEdit);

                                    //MARK : operation
                                    ibtnEdit.setVisibility(View.VISIBLE);

                                    //MARK : trigger
                                    ibtnEdit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            //MARK : obj
                                            final brainKomentar brainKomentar = new brainKomentar();

                                            //MARK : obj
                                            final String komentarBaru = edtKomen.getText().toString();

                                            if (komentarBaru.equals(""))
                                                alert("mohon mengisi kolom komentar");
                                            else {
                                                if (brainKomentar.editKomentar(idKomen, edtKomen.getText().toString())) {
                                                    temp = "";
                                                    edtKomen.setFocusable(false);
                                                    ibtnEdit.setVisibility(View.GONE);
                                                } else {
                                                    alert("gagal mengubah komentar, periksa koneksi anda");
                                                    edtKomen.setText(temp);
                                                }
                                            }

                                        }
                                    });
                                    //endregion

                                } else {
                                    alert("mohon menyelesaikan edit komen lain terlebih dahulu");
                                }
                                break;
                            case R.id.hapus:
                                final brainKomentar brainKomentar = new brainKomentar();
                                Log("idkomen : " + idKomen);
                                if (brainKomentar.hapusKomentar(idKomen)) {
                                    alert("komentar berhasil di hapus");
                                    Log("ukuran sebelum di hapus : " + classKomentars.size());
                                    Log("hapus urutan ke : " + position);
                                    classKomentars.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemChanged(position, classKomentars);
                                    Log("ukuran setelah di hapus : " + classKomentars.size());
                                } else {
                                    alert("komentar gagal di hapus");
                                }
                                break;
                            case R.id.lapor:

                                //MARK : obj
                                final Intent intent = new Intent(context, activityLapor.class);

                                //MARK : operation
                                intent.putExtra("mid", getIdMasjid());
                                intent.putExtra("kid", classKomentar.getIdkomen());
                                intent.putExtra("pid", profil_id);
                                intent.putExtra("username", username);
                                intent.putExtra("komen", komentar);
                                context.startActivity(intent);
                                break;
                            case R.id.blokir:

                                //MARK : trigger
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:

                                                //MARK : obj
                                                brainBlokir brainBlokir = new brainBlokir();

                                                //MARK : operation
                                                if (brainBlokir.blokir(idProfil, idMasjid))
                                                    alert("user " + username + " berhasil di blokir");
                                                else
                                                    alert("gagal memblokir user");

                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:
                                                //No button clicked
                                                break;
                                        }
                                    }
                                };

                                //MARK : obj
                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);

                                builder.setMessage("Blokir user " + username + " ?").setPositiveButton("Yes", dialogClickListener)
                                        .setNegativeButton("No", dialogClickListener).show();
                                break;
                        }
                    }
                })
                .setOnDismissCallback(new DroppyMenuPopup.OnDismissCallback() {
                    @Override
                    public void call() {
                    }
                })
                .setPopupAnimation(new DroppyFadeInAnimation())
                .setXOffset(5)
                .setYOffset(5)
                .build();

        //MARK : trigger
        imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                droppyMenu.show();
            }
        });
        //endregion

        //endregion
    }

    @Override
    public int getItemCount() {
        return getClassKomentars().size();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<classKomentar> getClassKomentars() {
        return classKomentars;
    }

    public void setClassKomentars(ArrayList<classKomentar> classKomentars) {
        this.classKomentars = classKomentars;
    }

    public String getProfil_id() {
        return profil_id;
    }

    public void setProfil_id(String profil_id) {
        this.profil_id = profil_id;
    }

    private void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }

    private void alert(String msg) {

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //MARK : operation
        builder.setMessage(msg).show();
    }

    public String getIdMasjid() {
        return idMasjid;
    }

    public void setIdMasjid(String idMasjid) {
        this.idMasjid = idMasjid;
    }

    public Boolean getMasjid() {
        return masjid;
    }

    public void setMasjid(Boolean masjid) {
        this.masjid = masjid;
    }
}
