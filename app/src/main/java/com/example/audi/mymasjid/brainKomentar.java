package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MOFOSEFES on 10/15/2017.
 */

public class brainKomentar {

    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classKomentar> loadKomentar(String idPosting, ArrayList<classKomentar> classKomentars) {

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkomentar.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", idPosting.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String username = c.getString("nama");
                String komentar = c.getString("komentar");
                String profil_id = c.getString("profil_id");
                String id = c.getString("id");
                classKomentars.add(new classKomentar(username, komentar, profil_id, id));
            }

        } catch (Exception e) {
        }
        return classKomentars;
    }

    public ArrayList<classKomentar> loadKomentarKegiatan(String kegiatanId, ArrayList<classKomentar> classKomentars) {

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkomentarkegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", kegiatanId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String username = c.getString("nama");
                String komentar = c.getString("komentar");
                String profil_id = c.getString("profil_id");
                String id = c.getString("id");
                classKomentars.add(new classKomentar(username, komentar, profil_id, id));
            }

        } catch (Exception e) {
        }
        return classKomentars;
    }

    public ArrayList<classKomentar> loadKomentarKegiatanRutin(String jadwalId, ArrayList<classKomentar> classKomentars) {

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkomentarkegiatanrutin.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", jadwalId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String username = c.getString("nama");
                String komentar = c.getString("komentar");
                String profil_id = c.getString("profil_id");
                String id = c.getString("id");
                classKomentars.add(new classKomentar(username, komentar, profil_id, id));
            }

        } catch (Exception e) {
        }
        return classKomentars;
    }

    public classKomentar loadKomentarid(String idkomen, classKomentar classKomentar) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkomentarid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idk", idkomen.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String username = c.getString("nama");
                String komentar = c.getString("komentar");
                String profil_id = c.getString("profil_id");
                String id = c.getString("id");
                classKomentar.setUsername(username);
                classKomentar.setKomentar(komentar);
                classKomentar.setProfil_id(profil_id);
                classKomentar.setIdkomen(id);
            }

        } catch (Exception e) {
        }
        return classKomentar;
    }

    public Boolean cekLike(String userId, String statusId) {
        String respon;
        Boolean hasil;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/ceklike.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("uid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("sid", statusId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            } else {
                hasil = false;
            }
        } catch (Exception e) {
            hasil = false;
        }

        return hasil;
    }

    public Boolean like(String userId, String statusId) {
        String respon;
        Boolean hasil;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/like.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("sid", statusId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            } else {
                hasil = false;
            }
        } catch (Exception e) {
            hasil = false;
        }

        return hasil;
    }

    public Boolean dislike(String userId, String statusId) {
        String respon;
        Boolean hasil;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/dislike.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("sid", statusId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            } else {
                hasil = false;
            }
        } catch (Exception e) {
            hasil = false;
        }

        return hasil;
    }

    public String komentar(String komentar, String userId, String statusId) {

        //mempersiapkan variabel
        String idkomen;
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/komentarstatus.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("sid", statusId.trim()));
            nameValuePairs.add(new BasicNameValuePair("komen", komentar.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("false")) {
                idkomen = null;
            } else {
                idkomen = respon;
                Log.e("mymasjidlog", "idkomen dari server = " + idkomen);
            }
        } catch (Exception e) {
            idkomen = null;
        }

        //mengembalikan hasil
        return idkomen;
    }

    public String komentarKegiatan(String komentar, String userId, String idKegiatan) {

        //mempersiapkan variabel
        String idkomen;
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/komentarkegiatan.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("kid", idKegiatan.trim()));
            nameValuePairs.add(new BasicNameValuePair("komen", komentar.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("false")) {
                idkomen = null;
            } else {
                idkomen = respon;
                Log.e("mymasjidlog", "idkomen dari server = " + idkomen);
            }
        } catch (Exception e) {
            idkomen = null;
        }

        //mengembalikan hasil
        return idkomen;
    }

    public String komentarKegiatanRutin(String komentar, String userId, String idJadwal) {

        //mempersiapkan variabel
        String idkomen;
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/komentarKegiatanRutin.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("jid", idJadwal.trim()));
            nameValuePairs.add(new BasicNameValuePair("komen", komentar.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("false")) {
                idkomen = null;
            } else {
                idkomen = respon;
                Log.e("mymasjidlog", "idkomen dari server = " + idkomen);
            }
        } catch (Exception e) {
            idkomen = null;
        }

        //mengembalikan hasil
        return idkomen;
    }

    public Boolean hapusKomentar(String kid) {
        String respon;
        Boolean hasil;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/hapuskomentar.php");

        Log.e("mymasjidlog", "kid : " + kid);

        try {
            Log.e("mymasjidlog", "hapus komen : " + kid);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("kid", kid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            Log.e("mymasjidlog", "respon : " + respon);

            if (respon.equals("true")) {
                hasil = true;
            } else {
                hasil = false;
            }
        } catch (Exception e) {
            hasil = false;
        }

        return hasil;
    }

    public Boolean editKomentar(String kid, String komen) {
        String respon;
        Boolean hasil;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/updatekomen.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("kid", kid.trim()));
            nameValuePairs.add(new BasicNameValuePair("komen", komen.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            } else {
                hasil = false;
            }
        } catch (Exception e) {
            hasil = false;
        }

        return hasil;
    }

    public Boolean laporKomentar(String userId, String masjidId, String komentarId, String keterangan) {

        //mempersiapkan variabel
        String respon;
        Boolean hasil;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/inputlapor.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("mid", masjidId.trim()));
            nameValuePairs.add(new BasicNameValuePair("kid", komentarId.trim()));
            nameValuePairs.add(new BasicNameValuePair("pid", userId.trim()));
            nameValuePairs.add(new BasicNameValuePair("ket", keterangan.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            } else {
                hasil = false;
            }
        } catch (Exception e) {
            hasil = false;
        }

        //mengembalikan hasil
        return hasil;
    }

    public classKomentar loadKomentarTerakhir(String idstatus) {
        classKomentar classKomentar = new classKomentar("","","","");
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkomentarterakhir.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("ids", idstatus.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            if (respon.equals("false")) {
            } else {
                JSONObject json = new JSONObject(respon);
                JSONArray json2 = json.getJSONArray("post");

                for (int i = 0; i < json2.length(); i++) {
                    JSONObject c = json2.getJSONObject(i);
                    String username = c.getString("nama");
                    String komentar = c.getString("komentar");
                    String idProfil = c.getString("idp");
                    String idKomentar = c.getString("idp");
                    classKomentar.setUsername(username);
                    classKomentar.setKomentar(komentar);
                    classKomentar.setProfil_id(idProfil);
                    classKomentar.setIdkomen(idKomentar);
                }
            }

        } catch (Exception e) {
        }
        return classKomentar;
    }

    public String loadjmllikeposting(String ids) {
        String respon;
        String jumlah="0";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjmllikeposting.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("ids", ids.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                jumlah = c.getString("jumlah");
            }

        } catch (Exception e) {
        }
        return jumlah;
    }
}
