package com.example.audi.mymasjid;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

public class activityPengikutMasjid extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengikut_masjid);

        //region isi class logins
        //MARK : obj
        final brainMasjid brainMasjid = new brainMasjid();
        final ArrayList<classLogin> classLogins;

        //MARK : operation
        classLogins = brainMasjid.loadPengikutMasjid(getIntent().getExtras().getString("idm"));
        //endregion

        //region load data recyclerview
        //MARK : obj
        final adapterPengikutMasjid adapterPengikutMasjid = new adapterPengikutMasjid(classLogins);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activityPengikutMasjid.this);

        //MARK : UI
        final RecyclerView rclPengikutMasjid = findViewById(R.id.rclPengikutMasjid);

        //MARK : operation
        rclPengikutMasjid.setLayoutManager(linearLayoutManager);
        rclPengikutMasjid.setHasFixedSize(true);
        rclPengikutMasjid.setAdapter(adapterPengikutMasjid);
        rclPengikutMasjid.setNestedScrollingEnabled(true);
        //endregion

        //region constrantnokonten
        //MARK : UI
        final ConstraintLayout constrantnokonten = findViewById(R.id.constrantnokonten);

        //MARK : operation
        if (classLogins.size() < 1)
            constrantnokonten.setVisibility(View.VISIBLE);
        else
            constrantnokonten.setVisibility(View.INVISIBLE);

        //endregion
    }

}
