package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MOFOSEFES on 10/18/2017.
 */

public class brainMasjid {

    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classMasjid> loadFollowMasjid(ArrayList<classMasjid> classMasjids, String uid) {

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadfollowmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", uid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("masjid_id");
                String nama = c.getString("nama");
                String alamat = c.getString("alamat");
                String kontak = c.getString("kontak");
                classMasjids.add(new classMasjid(id, nama, alamat, kontak));
            }
        } catch (Exception e) {
        }
        return classMasjids;
    }

    public ArrayList<classMasjid> loadMasjid(ArrayList<classMasjid> classMasjids) {

        //mempersiapkan variabel
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadmasjid.php");

        try {

            //mempersiapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            //menampung setiap data ke arraylist
            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String nama = c.getString("nama");
                String alamat = c.getString("alamat");
                String kontak = c.getString("kontak");
                classMasjids.add(new classMasjid(id, nama, alamat, kontak));
            }
        } catch (Exception e) {
        }

        //mengembalikan arraylist
        return classMasjids;
    }

    public Boolean tambahMasjid(String uid, String mid, String idk, String idj) {

        brainLog brainLog = new brainLog();
        brainLog.Log("brainmasjid","idk",idk);
        brainLog.Log("brainmasjid","idj",idj);

        //mempersiapkan variabel
        String respon;
        Boolean hasil = false;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/tambahmasjid.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("uid", uid.trim()));
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public classMasjid loadProfilMasjid(String idMasjid) {
        classMasjid classMasjid = null;
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadprofilmasjid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idMasjid", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String nama = c.getString("nama");
                String alamat = c.getString("alamat");
                String kontak = c.getString("kontak");
                Log.e("mymasjidlog", "nama masjid dari json : " + nama);
                classMasjid = new classMasjid(id, nama, alamat, kontak);
            }
        } catch (Exception e) {
        }
        return classMasjid;
    }

    public Boolean hapusMasjid(String uid, String mid) {
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/hapusmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("uid", uid.trim()));
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public String loadFollowerMasjid(String id) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadfollowermasjid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id_masjid", id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }
        return respon;
    }

    public String loadJmlKegiatanMasjid(String id) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjmlkegiatanmasjid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id_masjid", id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }
        return respon;
    }

    public Boolean updateProfilMasjid(String idMasjid, String namaMasjid, String alamatMasjid, String kontakMasjid) {
        Log.e("mymasjidlog", "update pofil masjid");
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/editprofilmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id_masjid", idMasjid.trim()));
            nameValuePairs.add(new BasicNameValuePair("nama_masjid", namaMasjid.trim()));
            nameValuePairs.add(new BasicNameValuePair("alamat_masjid", alamatMasjid.trim()));
            nameValuePairs.add(new BasicNameValuePair("kontak_masjid", kontakMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean tambahPengurusMasjid(String idMasjid, String nama, String jabatan, String kontak) {
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputpengurus.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id_masjid", idMasjid.trim()));
            nameValuePairs.add(new BasicNameValuePair("nama", nama.trim()));
            nameValuePairs.add(new BasicNameValuePair("jabatan", jabatan.trim()));
            nameValuePairs.add(new BasicNameValuePair("kontak", kontak.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public ArrayList<classPengurusMasjid> loadPengurusMasjid(ArrayList<classPengurusMasjid> classPengurusMasjids, String idMasjid) {

        //MARK : OBJ
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadpengurus.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //MARK : Operation
            HttpResponse response = httpClient.execute(httppost);

            //MARK : OBJ
            String respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            //MARK : Operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : OBJ
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String nama = c.getString("nama");
                String jabatan = c.getString("jabatan");
                String kontak = c.getString("kontak");

                //MARK : Operation
                classPengurusMasjids.add(new classPengurusMasjid(id, nama, jabatan, kontak));
            }
        } catch (Exception e) {
        }

        return classPengurusMasjids;
    }

    public Boolean hapusPengurus(String pmid) {
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/hapuspengurus.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pmid", pmid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public ArrayList<classLogin> loadPengikutMasjid(String idMasjid) {

        //MARK : OBJ
        ArrayList<classLogin> classLogins = new ArrayList<>();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadpengikutmasjid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //MARK : Operation
            HttpResponse response = httpClient.execute(httppost);

            //MARK : OBJ
            String respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            //MARK : Operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : OBJ
                JSONObject c = json2.getJSONObject(i);
                String username = c.getString("nama");
                String tipe = c.getString("jenis");
                String id = c.getString("id");
                String email = c.getString("email");
                String masjid_id = c.getString("masjid_id");
                String provinsi = c.getString("provinsi");
                String kotakab = c.getString("kotakab");
                String kecamatan = c.getString("kecamatan");

                //MARK : Operation
                classLogins.add(new classLogin(username, tipe, id, email, tipe, masjid_id, provinsi, kotakab, kecamatan));
            }
        } catch (Exception e) {
        }

        return classLogins;
    }

    public List<String> loadTopikMasjid(String idMasjid, Boolean edit) {

        //MARK : OBJ
        List<String> topik = new ArrayList<>();

        //region tambah item semua topik kl bukan edit
        if(!edit){
            topik.add("semua topik");
        }
        //endregion

        //MARK : obj
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadtopikmasjid.php");

        //region kirim dan terima hasil webservice
        try {

            //region set kiriman ke webservice
            //MARK : obj
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region olah json
            //MARK : OBJ
            HttpResponse response = httpClient.execute(httppost);
            String respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            //MARK : Operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : OBJ
                JSONObject c = json2.getJSONObject(i);
                topik.add(c.getString("topik"));
            }
            //endregion
        } catch (Exception e) {
        }
        //endregion

        return topik;
    }

    public String loadKegiatanTerakhirMasjid(String idm) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkegiatanterakhirmasjidid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }
        return respon;
    }

    public String loadKegiatanRutinTerakhirMasjid(String idm) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkegiatanrutinterakhirmasjidid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }
        return respon;
    }

    public Boolean cekFollowMasjid(String idp, String idm) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekfollowmasjid.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            brainLog brainLog = new brainLog();
            brainLog.Log("brain masjid","respon",respon);

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

}
