package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MOFOSEFES on 10/15/2017.
 */


public class brainBerita {
    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classPosting> loadPosting(String id, ArrayList<classPosting> arrayListPosting) {

        //mempersiapkan variabel
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadposting.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            //memasukkan data ke arraylist
            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String namaMasjid = c.getString("nama");
                String status = c.getString("status");
                String topik = c.getString("topik");
                String idPosting = c.getString("statusId");
                String idMasjid = c.getString("masjidId");
                String tanggal = c.getString("tanggal");

                arrayListPosting.add(new classPosting(namaMasjid, status, idPosting, idMasjid, tanggal, topik));
            }
        } catch (Exception e) {
        }

        //mengembalikan arraylist
        return arrayListPosting;
    }

    public Boolean inputStatus(String msg, String mid, String tpk) {

        //mempersiapkan variabel
        String respon = "";

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputstatus.php");
        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("msg", msg.trim()));
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("tpk", tpk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }

        ////memeriksa hasil dan mengembalikan hasil
        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean inputStatusdanGambar(String msg, String mid, String foto, String tpk) {

        //mempersiapkan variabel
        String respon = "";

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputstatusdangambar.php");
        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("msg", msg.trim()));
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("tpk", tpk.trim()));
            nameValuePairs.add(new BasicNameValuePair("foto", foto));

            //mengirim data dan menampung hasil
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }

        //memeriksa hasil dan mengembalikan hasil
        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<classPosting> loadPostingMasjid(String idm, ArrayList<classPosting> arrayListPosting) {

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadpostingmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String namaMasjid = c.getString("nama");
                String status = c.getString("status");
                String topik = c.getString("topik");
                String idPosting = c.getString("id");
                String idMasjid = c.getString("masjid_id");
                String tanggal = c.getString("tanggal");

                arrayListPosting.add(new classPosting(namaMasjid, status, idPosting, idMasjid, tanggal, topik));
            }
        } catch (Exception e) {
        }
        return arrayListPosting;
    }

    public Boolean updateStatus(String msg, String tpk, String idStatus) {

        //mempersiapkan variabel
        String respon = "";

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/updatestatus.php");
        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("ids", idStatus.trim()));
            nameValuePairs.add(new BasicNameValuePair("msg", msg.trim()));
            nameValuePairs.add(new BasicNameValuePair("tpk", tpk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }

        //memeriksa hasil dan mengembalikan hasil
        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean hapusStatus(String ids) {

        //mempersiapkan variabel
        String respon = "";

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/hapusposting.php");
        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("ids", ids.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }

        //memeriksa hasil dan mengembalikan hasil
        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<classPosting> loadPostingbytopik(String idm, String tpk, ArrayList<classPosting> arrayListPosting) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadpostingmasjidbytopik.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("tpk", tpk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String namaMasjid = c.getString("nama");
                String status = c.getString("status");
                String topik = c.getString("topik");
                String idPosting = c.getString("id");
                String idMasjid = c.getString("masjid_id");
                String tanggal = c.getString("tanggal");

                arrayListPosting.add(new classPosting(namaMasjid, status, idPosting, idMasjid, tanggal, topik));
            }
        } catch (Exception e) {
        }
        return arrayListPosting;
    }
}
