package com.example.audi.mymasjid;

public class classKecamatan {
    private String id;
    private String name;
    private String regency_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegency_id() {
        return regency_id;
    }

    public void setRegency_id(String regency_id) {
        this.regency_id = regency_id;
    }

    public classKecamatan(String id, String name, String regency_id){
        setId(id);
        setName(name);
        setRegency_id(regency_id);
    }
}
