package com.example.audi.mymasjid;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import co.intentservice.chatui.fab.FloatingActionButton;

public class activityKegiatanMasjid extends AppCompatActivity {

    Boolean onCreate;
    fragmentKegiatanMasjid fragmentKegiatanMasjid;
    Boolean masjid;
    String idm, idp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_masjid);

        //region get kiriman dan set kiriman
        //MARK : obj
        final Bundle getBundle = getIntent().getExtras();
        final Bundle bundle = new Bundle();
        masjid = getBundle.getBoolean("masjid");
        idm = getBundle.getString("idm");
        idp = getBundle.getString("idp");
        fragmentKegiatanMasjid = new fragmentKegiatanMasjid();

        refreshData();
        //endregion

        final android.support.design.widget.FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(activityKegiatanMasjid.this, activityTambahKegitan.class);
                intent.putExtra("idm", idm);
                startActivity(intent);
            }
        });

        onCreate = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!onCreate)
            refreshData();
        else onCreate = false;
    }

    private void refreshData() {
        fragmentKegiatanMasjid = new fragmentKegiatanMasjid();

        final Bundle bundle = new Bundle();
        //MARK : operation
        bundle.putBoolean("masjid", masjid);
        bundle.putString("idm", idm);
        bundle.putString("idp", idp);
        fragmentKegiatanMasjid.setArguments(bundle);

        //region load fragment
        //MARK : obj
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //MARK : operation
        transaction.replace(R.id.llKonten, fragmentKegiatanMasjid).commit();
        //endregion
    }
}
