package com.example.audi.mymasjid;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class activityListLapor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_lapor);

        //MARK : obj
        Boolean masjid;
        final Bundle bundle = getIntent().getExtras();

        //region cek masjid / ga
        //MARK : operation
        try {
            masjid = bundle.getBoolean("masjid");
        } catch (Exception e) {
            masjid = false;
        }
        //endregion

        //region isi arraylist
        //MARK : obj
        ArrayList<classLapor> classLapors = new ArrayList<>();
        final brainLapor brainLapor = new brainLapor();

        //MARK : operation
        if (masjid)
            brainLapor.loadLaporMasjid(bundle.getString("mid"), classLapors);
        else
            brainLapor.loadLapor(bundle.getString("pid"), classLapors);
        //endregion

        //region set recyclerview
        //MARK : obj
        final Context context = activityListLapor.this;
        final adapterLapor adapterLapor = new adapterLapor(context, classLapors, masjid);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        //MARK : UI
        final RecyclerView rclLapor = findViewById(R.id.rclLapor);

        //MARK : operation isi recyclerview
        rclLapor.setLayoutManager(linearLayoutManager);
        rclLapor.setHasFixedSize(true);
        rclLapor.setAdapter(adapterLapor);
        rclLapor.setNestedScrollingEnabled(true);
        //endregion

        //region constrantnokonten
        //MARK : UI
        final ConstraintLayout constrantnokonten = findViewById(R.id.constrantnokonten);

        //MARK : operation
        if (classLapors.size() < 1)
            constrantnokonten.setVisibility(View.VISIBLE);
        else
            constrantnokonten.setVisibility(View.INVISIBLE);

        //endregion
    }
}
