package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 10/22/2017.
 */

public class brainKegiatan {

    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classKegiatan> loadKegiatan(String id, ArrayList<classKegiatan> classKegiatans) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String idKegiatan = c.getString("id");
                String nama = c.getString("nama");
                String tanggal_mulai = c.getString("tanggal_mulai");
                String jam_mulai = c.getString("jam_mulai");
                String tanggal_selesai = c.getString("tanggal_selesai");
                String jam_selesai = c.getString("jam_selesai");
                String masjid_id = c.getString("masjid_id");
                String nama_masjid = c.getString("nama_masjid");
                String deskripsi = c.getString("deskripsi");

                classKegiatans.add(new classKegiatan(
                        idKegiatan,
                        nama,
                        tanggal_mulai,
                        jam_mulai,
                        tanggal_selesai,
                        jam_selesai,
                        masjid_id,
                        nama_masjid,
                        deskripsi
                ));
            }
        } catch (Exception e) {
        }
        return classKegiatans;
    }

    public ArrayList<classKegiatan> loadKegiatanMasjid(String idm, ArrayList<classKegiatan> classKegiatans) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadkegiatanmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String idKegiatan = c.getString("id");
                String nama = c.getString("nama");
                String tanggal_mulai = c.getString("tanggal_mulai");
                String jam_mulai = c.getString("jam_mulai");
                String tanggal_selesai = c.getString("tanggal_selesai");
                String jam_selesai = c.getString("jam_selesai");
                String masjid_id = c.getString("masjid_id");
                String nama_masjid = c.getString("nama_masjid");
                String deskripsi = c.getString("deskripsi");

                classKegiatans.add(new classKegiatan(
                        idKegiatan,
                        nama,
                        tanggal_mulai,
                        jam_mulai,
                        tanggal_selesai,
                        jam_selesai,
                        masjid_id,
                        nama_masjid,
                        deskripsi
                ));
            }
        } catch (Exception e) {
        }
        return classKegiatans;
    }

    public ArrayList<classKegiatan> loadKegiatanUser(String id, ArrayList<classKegiatan> classKegiatans) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkegiatanuser.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String idKegiatan = c.getString("id");
                String nama = c.getString("nama");
                String tanggal_mulai = c.getString("tanggal_mulai");
                String jam_mulai = c.getString("jam_mulai");
                String tanggal_selesai = c.getString("tanggal_selesai");
                String jam_selesai = c.getString("jam_selesai");
                String masjid_id = c.getString("masjid_id");
                String nama_masjid = c.getString("nama_masjid");
                String deskripsi = c.getString("deskripsi");

                classKegiatans.add(new classKegiatan(
                        idKegiatan,
                        nama,
                        tanggal_mulai,
                        jam_mulai,
                        tanggal_selesai,
                        jam_selesai,
                        masjid_id,
                        nama_masjid,
                        deskripsi
                ));
            }
        } catch (Exception e) {
        }
        return classKegiatans;
    }

    public Boolean ikutKegiatan(String id_user, String id_kegiatan) {

        //mempersiapkan variabel
        String respon;
        Boolean hasil = false;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/ikutKegiatan.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id_user", id_user.trim()));
            nameValuePairs.add(new BasicNameValuePair("id_kegiatan", id_kegiatan.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public Boolean batalKegiatan(String id_user, String id_kegiatan) {
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/batalkegiatan.php");

        Log.e("mymasjidlog", "id user : " + id_user);
        Log.e("mymasjidlog", "id kegiatan : " + id_kegiatan);

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id_user", id_user.trim()));
            nameValuePairs.add(new BasicNameValuePair("id_kegiatan", id_kegiatan.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean tambahKegiatan(String namaKegiatan, String tglMulai, String tglSelesai, String deskripsi, String idMasjid) {

        //mempersiapkan variabel
        String respon;
        Boolean hasil = false;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputKegiatan.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("namakeg", namaKegiatan.trim()));
            nameValuePairs.add(new BasicNameValuePair("tglmul", tglMulai.trim()));
            nameValuePairs.add(new BasicNameValuePair("tglsel", tglSelesai.trim()));
            nameValuePairs.add(new BasicNameValuePair("deskripsi", deskripsi.trim()));
            nameValuePairs.add(new BasicNameValuePair("id_masjid", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public Boolean tambahKegiatanGambar(String namaKegiatan, String tglMulai, String tglSelesai, String deskripsi, String idMasjid, String foto) {

        //mempersiapkan variabel
        String respon;
        Boolean hasil = false;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputKegiatangambar.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("namakeg", namaKegiatan.trim()));
            nameValuePairs.add(new BasicNameValuePair("tglmul", tglMulai.trim()));
            nameValuePairs.add(new BasicNameValuePair("tglsel", tglSelesai.trim()));
            nameValuePairs.add(new BasicNameValuePair("deskripsi", deskripsi.trim()));
            nameValuePairs.add(new BasicNameValuePair("id_masjid", idMasjid.trim()));
            nameValuePairs.add(new BasicNameValuePair("foto", foto.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public Boolean hapusKegiatanMasjid(String idKegiatan, String idMasjid) {

        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/hapuskegiatanmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idKegiatan", idKegiatan.trim()));
            nameValuePairs.add(new BasicNameValuePair("id_masjid", idMasjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public classKegiatan loadKegiatanMasjidId(String idk) {
        classKegiatan classKegiatan = null;
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadkegiatanmasjidid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String idKegiatan = c.getString("id");
                String nama = c.getString("nama");
                String tanggal_mulai = c.getString("tanggal_mulai");
                String jam_mulai = c.getString("jam_mulai");
                String tanggal_selesai = c.getString("tanggal_selesai");
                String jam_selesai = c.getString("jam_selesai");
                String masjid_id = c.getString("masjid_id");
                String nama_masjid = c.getString("nama_masjid");
                String deskripsi = c.getString("deskripsi");

                classKegiatan = new classKegiatan(idKegiatan, nama, tanggal_mulai, jam_mulai, tanggal_selesai, jam_selesai, masjid_id, nama_masjid, deskripsi);
            }
        } catch (Exception e) {
        }
        return classKegiatan;
    }

    public Boolean updateKegiatan(String jud, String tglm, String tgls, String desk, String idk) {

        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/updatekegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("jud", jud.trim()));
            nameValuePairs.add(new BasicNameValuePair("tglm", tglm.trim()));
            nameValuePairs.add(new BasicNameValuePair("tgls", tgls.trim()));
            nameValuePairs.add(new BasicNameValuePair("desk", desk.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    //MARK : unfinished
    public Boolean cekLike(String idp, String idk) {
        brainLog brainLog = new brainLog();
        brainLog.Log("brainkegiatan", "idp", idp);
        brainLog.Log("brainkegiatan", "idk", idk);
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/ceklikekegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            brainLog.Log("brainkegiatan", "respon", respon);
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean like(String idp, String idk) {

        //mempersiapkan variabel
        String respon;
        Boolean hasil = false;


        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/likekegiatan.php");

        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public Boolean unlike(String idp, String idk) {
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/unlikekegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean updateReadKegiatan(String idm, String idp, String idk) {
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/updatereadkegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public String loadUnreadKegiatan(String idm, String idk) {

        brainLog brainLog = new brainLog();
        brainLog.Log("brainkegiatan","idm",idm);
        brainLog.Log("brainkegiatan","idk",idk);

        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadunreadkegiatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            brainLog.Log("brainkegiatan","respon",respon);
        } catch (Exception e) {
        }
        return respon;
    }

    public List<String> loadDaftarMasjidKegiatanUser(String idp) {
        List<String> strings = new ArrayList<>();
        strings.add("semua masjid");
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loaddaftarmasjidkegiatan.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String nama = c.getString("nama");

                strings.add(nama);
            }
        } catch (Exception e) {
        }
        return strings;
    }

    public ArrayList<classKegiatan> loadKegiatanbyMasjid(String id, String masjid, ArrayList<classKegiatan> classKegiatans) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkegiatanuserbymasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("id", id.trim()));
            nameValuePairs.add(new BasicNameValuePair("masjid", masjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String idKegiatan = c.getString("id");
                String nama = c.getString("nama");
                String tanggal_mulai = c.getString("tanggal_mulai");
                String jam_mulai = c.getString("jam_mulai");
                String tanggal_selesai = c.getString("tanggal_selesai");
                String jam_selesai = c.getString("jam_selesai");
                String masjid_id = c.getString("masjid_id");
                String nama_masjid = c.getString("nama_masjid");
                String deskripsi = c.getString("deskripsi");

                classKegiatans.add(new classKegiatan(
                        idKegiatan,
                        nama,
                        tanggal_mulai,
                        jam_mulai,
                        tanggal_selesai,
                        jam_selesai,
                        masjid_id,
                        nama_masjid,
                        deskripsi
                ));
            }
        } catch (Exception e) {
        }
        return classKegiatans;
    }

    public List<String> cekJamTabrakan(String tglmulai, String tglselesai, String idp) {
        List<String> strings = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekjamtabrakan.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("tglmulai", tglmulai.trim()));
            nameValuePairs.add(new BasicNameValuePair("tglselesai", tglselesai.trim()));
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String nama = c.getString("nama");
                String tanggal_mulai = c.getString("tanggal_mulai");
                String tanggal_selesai = c.getString("tanggal_selesai");
                String nama_masjid = c.getString("nama_masjid");

                strings.add(nama);
                strings.add(tanggal_mulai);
                strings.add(tanggal_selesai);
                strings.add(nama_masjid);
            }
        } catch (Exception e) {
        }
        return strings;
    }

    public Boolean cekFollowKegiatan(String idp, String idk) {
        List<String> strings = new ArrayList<>();
        String respon;
        Boolean hasil = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekfollowkegiatan.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }
        return hasil;
    }

    public String loadjmllikekegiatan(String idk) {
        String respon;
        String jumlah = "0";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjmllikekegiatan.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                jumlah = c.getString("jumlah");
            }

        } catch (Exception e) {
        }
        return jumlah;
    }

    public String loadjmlfollowkegiatan(String idk) {
        String respon;
        String jumlah = "0";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjmlfollowkegiatan.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idk", idk.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                jumlah = c.getString("jumlah");
            }

        } catch (Exception e) {
        }
        return jumlah;
    }

    public ArrayList<classLogin> loadPengikutKegiatan(String idKegiatan) {

        brainLog brainLog = new brainLog();
        brainLog.Log("brain kegiatan","idk",idKegiatan);

        //MARK : OBJ
        ArrayList<classLogin> classLogins = new ArrayList<>();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadpengikutkegiatan.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idk", idKegiatan.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //MARK : Operation
            HttpResponse response = httpClient.execute(httppost);

            //MARK : OBJ
            String respon = EntityUtils.toString(response.getEntity());

            brainLog.Log("brain kegiatan","respon",respon);

            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            //MARK : Operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : OBJ
                JSONObject c = json2.getJSONObject(i);
                String username = c.getString("nama");
                String tipe = c.getString("jenis");
                String id = c.getString("id");
                String email = c.getString("email");
                String masjid_id = c.getString("masjid_id");
                String provinsi = c.getString("provinsi");
                String kotakab = c.getString("kotakab");
                String kecamatan = c.getString("kecamatan");

                //MARK : Operation
                classLogins.add(new classLogin(username, tipe, id, email, tipe, masjid_id, provinsi, kotakab, kecamatan));
            }
        } catch (Exception e) {
        }

        brainLog.Log("brain kegiatan","classLogins",String.valueOf(classLogins.size()));

        return classLogins;
    }
}
