package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class adapterKegiatanUser extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //region set data
    final Context context;
    final ArrayList<classKegiatan> classKegiatans;
    final String iduser;

    public adapterKegiatanUser(Context context, ArrayList<classKegiatan> classKegiatans, String iduser) {
        this.context = context;
        this.classKegiatans = classKegiatans;
        this.iduser = iduser;
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_kegiatan_user, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : obj
        final View itemView = holder.itemView;
        final classKegiatan classKegiatan = classKegiatans.get(position);

        //region set data ke UI
        //MARK : UI
        final TextView txtJudulKegiatan, txtNamaMasjid, txtTanggalMulai, txtTanggalSelesai, txtJamMulai,
                txtJamSelesai, txtDeskripsi;
        //region Textview
        txtJudulKegiatan = itemView.findViewById(R.id.txtJudulKegiatan);
        txtNamaMasjid = itemView.findViewById(R.id.txtNamaMasjid);
        txtTanggalMulai = itemView.findViewById(R.id.txtTanggalMulai);
        txtTanggalSelesai = itemView.findViewById(R.id.txtTanggalSelesai);
        txtJamMulai = itemView.findViewById(R.id.txtJamMulai);
        txtJamSelesai = itemView.findViewById(R.id.txtJamSelesai);
        txtDeskripsi = itemView.findViewById(R.id.txtDeskripsi);
        //endregion

        //MARK : operation
        txtJudulKegiatan.setText(classKegiatan.getNamaKegiatan());
        txtTanggalMulai.setText(classKegiatan.getTanggalMulai());
        txtTanggalSelesai.setText(classKegiatan.getTanggalSelesai());
        txtJamMulai.setText(classKegiatan.getJamMulai());
        txtJamSelesai.setText(classKegiatan.getJamSelesai());
        txtDeskripsi.setText(classKegiatan.getDeskripsi());
        //endregion

        //region btnBatal
        //MARK : UI
        final ImageButton btnBatal = itemView.findViewById(R.id.btnBatal);

        //MARK : trigger
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final brainKegiatan brainKegiatan = new brainKegiatan();
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);

                //MARK : operation
                if(brainKegiatan.batalKegiatan(iduser,classKegiatan.getIdKegiatan())){

                    //MARK : operation
                    builder.setMessage("kegiatan telah dibatalkan");
                    classKegiatans.remove(position);
                    notifyItemRemoved(position);
                    notifyItemChanged(position, classKegiatans);
                }else{
                    builder.setMessage("terjadi masalah saat penghapusan kegiatan");
                }
                builder.show();

            }
        });
        //endregion

        //region img kegiatan
        //MARK : UI
        final ImageView img = itemView.findViewById(R.id.img);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/masjid/Foto_Kegiatan/" + classKegiatan.getIdKegiatan() + ".jpg";

        //MARK : operation
        Glide.with(context).load(url).thumbnail(1.0f).into(img);

        //MARK : trigger
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : OBJ
                final Intent i = new Intent(context, activityDetailGambar.class);

                //MARK : operation
                i.putExtra("url", url);
                context.startActivity(i);
            }
        });
        //endregion

        //region load Image Masjid
        //MARK : obj
        final ImageView imgMasjid = itemView.findViewById(R.id.imgMasjid);

        //MARK : obj
        final String urlImgMasjid = "http://103.52.146.34/mymasjid/Foto_Masjid/"+classKegiatan.getIdMasjid()+".jpg";

        //MARK : operation
        Glide.with(context).load(urlImgMasjid).thumbnail(1.0f).into(imgMasjid);
        //endregion
    }

    @Override
    public int getItemCount() {
        return classKegiatans.size();
    }

    private void ubahLike(ImageButton btnLike, Boolean like) {

        //MARK : operation
        if (like) {
            btnLike.setBackground(context.getResources().getDrawable(R.color.green));
            btnLike.setTag(false);
        } else {
            btnLike.setBackground(context.getResources().getDrawable(R.color.blue));
            btnLike.setTag(true );
        }

    }

    public void Log(String msg){
        Log.e("mymasjidlog",msg);
    }
}
