package com.example.audi.mymasjid;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class activityKegiatanUser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kegiatan_user);

        //MARK : OBJ
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        final fragmentKegiatanUser fragmentKegiatanUser = new fragmentKegiatanUser();
        final Bundle bundle = new Bundle();

        //MARK : operaion
        bundle.putString("uid", getIntent().getExtras().getString("uid"));
        fragmentKegiatanUser.setArguments(bundle);
        transaction.replace(R.id.llKont, fragmentKegiatanUser).commit();
    }
}
