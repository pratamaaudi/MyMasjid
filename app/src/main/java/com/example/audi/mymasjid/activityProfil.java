package com.example.audi.mymasjid;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pixplicity.easyprefs.library.Prefs;

public class activityProfil extends AppCompatActivity {

    Boolean edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        //MARK : global obj
        edit = false;

        //MARK : UI
        final EditText edtUsername, edtEmail;
        edtUsername = findViewById(R.id.edtUsername);
        edtEmail = findViewById(R.id.edtEmail);

        //MARK : obj
        final classLogin classLogin = getIntent().getExtras().getParcelable("classLogin");

        //region btn edit
        //MARK : UI
        final ImageButton btnEdit = findViewById(R.id.btnEdit);

        //MARK : trigger
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit) {
                    edit = false;

                    //MARK : obj
                    final String username = edtUsername.getText().toString();
                    final String email = edtEmail.getText().toString();
                    final TextView txteror = findViewById(R.id.txteror);

                    if (username.equals("") || email.equals(""))
                        txteror.setVisibility(View.VISIBLE);
                    else {

                        //MARK : obj
                        final brainProfil brainProfil = new brainProfil();

                        //MARK : operation
                        classLogin.setUsername(username);
                        classLogin.setEmail(email);
                        if (brainProfil.cekEmailEdit(email, classLogin.getId())) {
                            txteror.setText("email sudah digunakan akun lain");
                            txteror.setVisibility(View.VISIBLE);
                        } else {
                            if (brainProfil.updateProfil(classLogin)) {

                                //MARK : obj
                                final Context context = activityProfil.this;
                                new Prefs.Builder().setContext(context).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();
                                Intent intent = new Intent(context, activityLogin.class);

                                //MARK : operation
                                edtEmail.setFocusable(false);
                                edtUsername.setFocusable(false);
                                btnEdit.setBackground(getResources().getDrawable(R.color.blue));
                                btnEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_white_24dp));
                                Prefs.putString("akun", "");
                                startActivity(intent);
                            }
                        }
                    }
                } else {

                    //MARK : operation
                    edtEmail.setFocusableInTouchMode(true);
                    edtUsername.setFocusableInTouchMode(true);
                    btnEdit.setBackground(getResources().getDrawable(R.color.green));
                    btnEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_black_24dp));
//                    btnEdit.setText("simpan");
                    edit = true;
                }
            }
        });
        //endregion

        //region set edittext dan textview
        //MARK : UI
        TextView txtJenisAkun = findViewById(R.id.txtJenisAkun);

        //MARK : operation
        edtUsername.setText(classLogin.getUsername());
        edtEmail.setText(classLogin.getEmail());
        txtJenisAkun.setText(classLogin.getJenis());
        edtEmail.setFocusable(false);
        edtUsername.setFocusable(false);
        //endregion
    }
}
