package com.example.audi.mymasjid;

/**
 * Created by prata on 9/22/2017.
 */

public class classJadwal_old {
    private String judul;
    private String tanggal;
    private String uztad;

    public classJadwal_old(String judul, String tanggal, String uztad) {
        setJudul(judul);
        setTanggal(tanggal);
        setUztad(uztad);
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getUztad() {
        return uztad;
    }

    public void setUztad(String uztad) {
        this.uztad = uztad;
    }
}
