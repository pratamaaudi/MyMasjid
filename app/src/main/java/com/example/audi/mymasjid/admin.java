package com.example.audi.mymasjid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class admin extends AppCompatActivity {

    RecyclerView rclMasjid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        initvalue();

        Integer[] myIntArray = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

        MasjidAdapter masjidAdapter = new MasjidAdapter(getApplicationContext(), myIntArray);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rclMasjid.setLayoutManager(llm);
        rclMasjid.setHasFixedSize(true);
        rclMasjid.setAdapter(masjidAdapter);
    }

    public void initvalue(){
        rclMasjid = (RecyclerView) findViewById(R.id.rclMasjid);
    }
}
