package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class activityDaftarMasjid extends AppCompatActivity {

    ImageView imgFotoMasjid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_masjid);

        //region btnUpload
        //MARK : UI
        final Button btnUpload = findViewById(R.id.btnUpload);

        //MARK : trigger
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //region select image
                //MARK : obj
                Intent intent = new Intent();

                //MARK : operation
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "select image from gallery"), 1);
                //endregion
            }
        });
        //endregion

        //region btnDaftar
        //koneksi ke tombol daftar dan gambar masjid
        //MARK : UI
        final Button btnDaftar = findViewById(R.id.btnDaftar);
        imgFotoMasjid = findViewById(R.id.imgFotoMasjid);

        //MARK : trigger
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ubah gambar jadi bitmap
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgFotoMasjid.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();

                //region daftar masjid
                //siapkan variabel
                //MARK : UI
                final EditText edtAlamat, edtKontak;
                edtAlamat = findViewById(R.id.edtAlamat);
                edtKontak = findViewById(R.id.edtKontak);

                //MARK : obj
                final Bundle extra = getIntent().getExtras();
                final String username, password, email, alamat, kontak;
                username = extra.getString("username");
                password = extra.getString("password");
                email = extra.getString("email");
                alamat = edtAlamat.getText().toString();
                kontak = edtKontak.getText().toString();

                //cek inputan user
                if (alamat.equals("") || kontak.equals("")) {

                    //MARK : obj
                    final TextView txteror = findViewById(R.id.txteror);

                    txteror.setVisibility(View.VISIBLE);
                } else
                    //kirim data pendaftaran
                    //MARK function
                    new daftarMasjid().execute(username, email, password, imageToString(bitmap), username, edtAlamat.getText().toString(), edtKontak.getText().toString(), extra.getString("idd"));
                //endregion
            }
        });
        //endregion
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //region olah hasil pilih gambar
        //MARK : operation
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {

                //region set imageview sesuai gambar
                //MARK : obj
                final Uri uri = data.getData();
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                //MARK : operation
                imgFotoMasjid.setImageBitmap(bitmap);
                //endregion
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //endregion
    }

    private class daftarMasjid extends AsyncTask<String, String, String> {
        private ProgressDialog pDialog;
        private Boolean sukses;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //region show loading
            //MARK : obj
            pDialog = new ProgressDialog(activityDaftarMasjid.this);

            //MARK : operation
            pDialog.setMessage("Menyimpan . . .");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            //endregion
        }

        @Override
        protected String doInBackground(String... data) {

            //region insert database
            //MARK : obj
            brainDaftar brain = new brainDaftar();

            //MARK : operation
            if (!brain.cekEmailMasjid(data[1])) {
                if (brain.daftarMasjid(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7])) {
                    sukses = true;
                } else {
                    sukses = false;
                    //gagal upload data
                }
            } else {
                sukses = false;
                //ada imel sama
            }
            //endregion
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //region cek sukses / ga
            //MARK : obj
            final AlertDialog alertDialog = new AlertDialog.Builder(activityDaftarMasjid.this).create();

            //MARK : operation
            pDialog.dismiss();
            if (!sukses) {

                //region set alert gagal
                //MARK : operation
                alertDialog.setMessage("data gagal diinputkan");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                //endregion
            } else {
                //region set alert berhasil
                //MARK : operation
                alertDialog.setMessage("data berhasil diinputkan");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                //MARK : obj
                final Intent intent = new Intent(activityDaftarMasjid.this, activityLogin.class);
                startActivity(intent);
                //endregion
            }
            alertDialog.show();
            //endregion
        }
    }

    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }
}
