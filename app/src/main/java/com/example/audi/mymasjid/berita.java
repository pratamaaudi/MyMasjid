package com.example.audi.mymasjid;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class berita extends android.support.v4.app.Fragment {


    ArrayList<Posting> arrayListPosting;
    RecyclerView recyclerView;
    NewsAdapter newsAdapter;
    LinearLayoutManager llm;

    public berita() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {

        View view = inflater.inflate(
                R.layout.fragment_berita,
                container, false
        );

        buatvariabel(view);
        isiarray();
        setRecyclerView();

        return view;
    }

    public void buatvariabel(View view) {
        arrayListPosting = new ArrayList<Posting>();
        recyclerView = (RecyclerView) view.findViewById(R.id.rclberita);

        newsAdapter = new NewsAdapter(
                view.getContext(),
                arrayListPosting);

        llm = new LinearLayoutManager(view.getContext());
    }

    public void isiarray() {
        arrayListPosting.add(new Posting("Masjid Al-Ridho", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Al-Kudus", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Al-Madani", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Arroyan", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Baitul fala", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Al-Falah", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Manarul Ilmi", "menyalurkan kurban kambing dan sapi", 1));
        arrayListPosting.add(new Posting("Masjid Al-Madani", "pengajian bersama akan diadakan pukul 19.00 hari rabu", 2));
        arrayListPosting.add(new Posting("Masjid Al-Falah", "pengajian bersama akan diadakan pukul 20.00 hari senin", 2));
    }

    public void setRecyclerView() {
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(newsAdapter);
        recyclerView.setNestedScrollingEnabled(true);
    }

}
