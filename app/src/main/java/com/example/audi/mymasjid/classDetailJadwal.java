package com.example.audi.mymasjid;

public class classDetailJadwal {
    private String id;
    private String hari;
    private String jam;
    private String menit;

    public classDetailJadwal(String id, String hari, String jam, String menit){
        setId(id);
        setHari(hari);
        setJam(jam);
        setMenit(menit);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getMenit() {
        return menit;
    }

    public void setMenit(String menit) {
        this.menit = menit;
    }
}
