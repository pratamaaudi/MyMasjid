package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class brainSearch {

    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classMasjid> searchMasjid(ArrayList<classMasjid> classMasjids, String query) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/searchmasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            nameValuePairs.add(new BasicNameValuePair("query", query.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            Log.e("mymasjidlog","respon : "+respon);
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            brainLog brainLog = new brainLog();
            brainLog.Log("brainsearch","respon",respon);

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("masjid_id");
                String nama = c.getString("nama");
                String alamat = c.getString("alamat");
                String kontak = c.getString("kontak");
                classMasjids.add(new classMasjid(id, nama, alamat, kontak));
            }
        } catch (Exception e) {
        }
        return classMasjids;
    }
}
