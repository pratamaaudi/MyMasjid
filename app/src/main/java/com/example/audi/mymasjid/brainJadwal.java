package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class brainJadwal {

    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classjadwal> loadJadwal(String mid, ArrayList<classjadwal> classjadwals) {

        //mempersiapkan koneksi ke webservice
        //MARK : obj
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjadwal.php");

        //MARK : operation
        try {

            //memperisapkan kiriman data
            //region set post
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //mengirim data dan menampung hasil
            //region send and get result
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());
            final JSONObject json = new JSONObject(respon);
            final JSONArray json2 = json.getJSONArray("post");

            //memasukkan data ke dalam arraylist
            //MARK : operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : obj
                final JSONObject c = json2.getJSONObject(i);
                final String id = c.getString("id");
                final String judul = c.getString("judul");
                final String keterangan = c.getString("keterangan");
//                final String jam = c.getString("jam");
//                final String menit = c.getString("menit");
//                final String hari = c.getString("hari");
                final String masjid_id = c.getString("masjid_id");
                final String namaMasjid = c.getString("nama");

                //MARK : operation
                classjadwals.add(new classjadwal(id, judul, keterangan, masjid_id, namaMasjid));
            }
            //endregion

        } catch (Exception e) {
        }

        //mengembalikan arraylist
        return classjadwals;
    }

    public ArrayList<classjadwal> loadJadwalId(String mid, ArrayList<classjadwal> classjadwals) {

        //MARK : obj
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjadwal.php");

        //MARK : operation
        try {

            //region set post
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region send and get result
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());
            final JSONObject json = new JSONObject(respon);
            final JSONArray json2 = json.getJSONArray("post");

            //MARK : operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : obj
                final JSONObject c = json2.getJSONObject(i);
                final String id = c.getString("id");
                final String judul = c.getString("judul");
                final String keterangan = c.getString("keterangan");
//                final String jam = c.getString("jam");
//                final String menit = c.getString("menit");
//                final String hari = c.getString("hari");
                final String masjid_id = c.getString("masjid_id");
                final String namaMasjid = c.getString("namaMasjid");

                //MARK : operation
                classjadwals.add(new classjadwal(id, judul, keterangan, masjid_id, namaMasjid));
            }
            //endregion

        } catch (Exception e) {
        }
        return classjadwals;
    }

    public ArrayList<classjadwal> loadJadwalUser(String idp, ArrayList<classjadwal> classjadwals) {

        //MARK : obj
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjadwaluser.php");

        //MARK : operation
        try {

            //region set post
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region send and get result
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());
            final JSONObject json = new JSONObject(respon);
            final JSONArray json2 = json.getJSONArray("post");

            //MARK : operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : obj
                final JSONObject c = json2.getJSONObject(i);
                final String id = c.getString("id");
                final String judul = c.getString("judul");
                final String keterangan = c.getString("keterangan");
//                final String jam = c.getString("jam");
//                final String menit = c.getString("menit");
//                final String hari = c.getString("hari");
                final String masjid_id = c.getString("masjid_id");
                final String namaMasjid = c.getString("nama");

                //MARK : operation
                classjadwals.add(new classjadwal(id, judul, keterangan, masjid_id, namaMasjid));
            }
            //endregion

        } catch (Exception e) {
        }
        return classjadwals;
    }

    public Boolean unreminder(String idp, String idj) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/hapusreminder.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean tambahJadwalGambar(String ket, String idm, String jud, String foto) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputjadwalgambar.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("ket", ket.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("jud", jud.trim()));
//            nameValuePairs.add(new BasicNameValuePair("tgl", tgl.trim()));
//            nameValuePairs.add(new BasicNameValuePair("hari", hari.trim()));
            nameValuePairs.add(new BasicNameValuePair("foto", foto));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean tambahJadwal(String ket, String idm, String jud) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputjadwal.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("ket", ket.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("jud", jud.trim()));
//            nameValuePairs.add(new BasicNameValuePair("tgl", tgl.trim()));
//            nameValuePairs.add(new BasicNameValuePair("hari", hari.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean tambahDetailJadwal(String jadwalId, String tgl, String hari){
        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputdetailjadwal.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("jadwalId", jadwalId.trim()));
            nameValuePairs.add(new BasicNameValuePair("tgl", tgl.trim()));
            nameValuePairs.add(new BasicNameValuePair("hari", hari.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean hapusJadwal(String jid) {
        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/hapusjadwal.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("jid", jid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean followJadwal(String pid, String jid) {

        boolean hasil = false;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/followjadwal.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("pid", pid.trim()));
            nameValuePairs.add(new BasicNameValuePair("jid", jid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        return hasil;
    }

    public Boolean cekReminder(String idp, String idj) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekreminder.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean cekNotification(String idp, String idj) {

        brainLog brainLog = new brainLog();
        brainLog.Log("brainjadwal","ceknotif","cek notif");

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/ceknotificationreminder.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            brainLog.Log("brainjadwal","respon",respon);

            //MARK : operation
            if (respon.equals("1")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean updateNotif(String idp, String idj, String notif) {

        brainLog brainLog = new brainLog();
        brainLog.Log("brainjadwal","notif",notif);

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/updateremindernotif.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            nameValuePairs.add(new BasicNameValuePair("notif", notif.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean updateJadwal(String jud, String jid, String ket) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/updatejadwal.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("jud", jud.trim()));
            nameValuePairs.add(new BasicNameValuePair("jid", jid.trim()));
            nameValuePairs.add(new BasicNameValuePair("ket", ket.trim()));
//            nameValuePairs.add(new BasicNameValuePair("tgl", tgl.trim()));
//            nameValuePairs.add(new BasicNameValuePair("hari", hari.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public Boolean updateUnreadKegiatanRutin(String idm, String idp, String idj) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/updatereadkegiatanrutin.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("1")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

    public String loadUnreadKegiatanRutin(String idm, String idj) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadunreadkegiatanrutin.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("idj", idj.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }
        return respon;
    }

    public List<String> loadDaftarMasjidJadwalUser(String idp) {
        List<String> strings = new ArrayList<>();
        strings.add("semua masjid");
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loaddaftarmasjidjadwal.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String nama = c.getString("nama");

                strings.add(nama);
            }
        } catch (Exception e) {
        }
        return strings;
    }

    public ArrayList<classjadwal> loadJadwalUserbyMasjid(String idp, String masjid, ArrayList<classjadwal> classjadwals) {

        //MARK : obj
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadjadwaluserbymasjid.php");

        //MARK : operation
        try {

            //region set post
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("masjid", masjid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region send and get result
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());
            final JSONObject json = new JSONObject(respon);
            final JSONArray json2 = json.getJSONArray("post");

            //MARK : operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : obj
                final JSONObject c = json2.getJSONObject(i);
                final String id = c.getString("id");
                final String judul = c.getString("judul");
                final String keterangan = c.getString("keterangan");
//                final String jam = c.getString("jam");
//                final String menit = c.getString("menit");
//                final String hari = c.getString("hari");
                final String masjid_id = c.getString("masjid_id");
                final String namaMasjid = c.getString("nama");

                //MARK : operation
                classjadwals.add(new classjadwal(id, judul, keterangan, masjid_id, namaMasjid));
            }
            //endregion

        } catch (Exception e) {
        }
        return classjadwals;
    }

    public ArrayList<classDetailJadwal> loadDetailJadwalbyIdJadwal(String jadwalId) {


        //MARK : obj
        ArrayList<classDetailJadwal> classDetailJadwals = new ArrayList<>();
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadDetailJadwalbyIdJadwal.php");


        //MARK : operation
        try {

            //region set post
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("jadwalId", jadwalId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region send and get result
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());
            final JSONObject json = new JSONObject(respon);
            final JSONArray json2 = json.getJSONArray("post");

            //MARK : operation
            for (int i = 0; i < json2.length(); i++) {

                //MARK : obj
                final JSONObject c = json2.getJSONObject(i);
                final String id = c.getString("id");
                final String hari = c.getString("hari");
                final String jam = c.getString("jam");
                final String menit = c.getString("menit");

                //MARK : operation
                classDetailJadwals.add(new classDetailJadwal(id,hari,jam,menit));
            }
            //endregion

        } catch (Exception e) {
        }
        return classDetailJadwals;
    }

    public Boolean hapusDetailJadwal(String detailJadwalId) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/hapusDetailJadwal.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("detailJadwalId", detailJadwalId.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }
        return hasil;
    }

}
