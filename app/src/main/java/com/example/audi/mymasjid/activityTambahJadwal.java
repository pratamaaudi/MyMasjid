package com.example.audi.mymasjid;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class activityTambahJadwal extends AppCompatActivity {

    ImageView img;
//    String jamString, menitString, hari;
    Boolean edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_jadwal);

        //MARK : glb obj
        img = findViewById(R.id.img);
//        hari = "Sunday";
        final Bundle bundle = getIntent().getExtras();
        final String idmasjid = bundle.getString("idmasjid");
        final Context context = activityTambahJadwal.this;

        //region spinner hari
        //MARK : UI
//        final MaterialSpinner spnHari = findViewById(R.id.spnHari);
//
//        //MARK : operation
//        spnHari.setItems("Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu");
//        spnHari.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                switch (position) {
//                    case 0:
//                        hari = "Monday";
//                        break;
//                    case 1:
//                        hari = "Tuesday";
//                        break;
//                    case 2:
//                        hari = "Wednesday";
//                        break;
//                    case 3:
//                        hari = "Thursday";
//                        break;
//                    case 4:
//                        hari = "Friday";
//                        break;
//                    case 5:
//                        hari = "Saturday";
//                        break;
//                    case 6:
//                        hari = "Sunday";
//                        break;
//                }
//            }
//        });
        //endregion

        //region txtPilihJam
        //MARK : UI
//        final TextView txtPilihJam = findViewById(R.id.txtPilihJam);
//
//        //MARK : trigger
//        txtPilihJam.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //MARK : obj
//                final Calendar calendar = Calendar.getInstance();
//                final Integer jam = calendar.get(Calendar.HOUR_OF_DAY);
//                final Integer menit = calendar.get(Calendar.MINUTE);
//                final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//
//                        //MARK: operation
//                        if (hourOfDay < 10) {
//                            jamString = "0" + String.valueOf(hourOfDay);
//                        } else {
//                            jamString = String.valueOf(hourOfDay);
//                        }
//
//                        if (minute < 10) {
//                            menitString = "0" + String.valueOf(minute);
//                        } else {
//                            menitString = String.valueOf(minute);
//                        }
//                        txtPilihJam.setText("Jam : " + jamString + ":" + menitString);
//                    }
//                }, jam, menit, true);
//
//                //MARK : operation
//                timePickerDialog.show();
//
//            }
//        });
        //endregion

        //region framelayout
        //MARK : UI
        final FrameLayout frm = findViewById(R.id.frm);

        //MARK : trigger
        frm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                Intent intent = new Intent();

                //MARK : operation
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "select image from gallery"), 1);
            }
        });
        //endregion

        //region cek edit
        //MARK : UI
        final EditText edtJudul = findViewById(R.id.edtJudul);
        final EditText edtKeterangan = findViewById(R.id.edtKeterangan);

        edit = false;

        try {
            edit = bundle.getBoolean("edit");
        } catch (Exception e) {
        }

        if (edit) {
            edtJudul.setText(bundle.getString("judul"));
            edtKeterangan.setText(bundle.getString("ket"));
//            jamString = bundle.getString("jam");
//            menitString = bundle.getString("menit");
//            txtPilihJam.setText("jam : " + jamString + ":" + menitString);
            frm.setVisibility(View.INVISIBLE);
        }
        //endregion

        //region btn submit
        //koneksi dengan tombol simpan
        //MARK : UI
        final ImageButton btnSubmit = findViewById(R.id.btnSubmit);

        //MARK : trigger
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //siapkan variabel
                //MARK : obj
                final String judul = edtJudul.getText().toString();

                //cek inputan user
                //MARK : operation
                if (judul.equals(""))
                    alert("mohon mengisi judul jadwal", context);
                else {
                    final brainJadwal brainJadwal = new brainJadwal();
                    final String keterangan, tgl;
                    //region String
                    keterangan = edtKeterangan.getText().toString();
//                    tgl = "2018-1-1 " + jamString + ":" + menitString;
                    //endregion

                    //update jadwal jika mode edit
                    //MARK : operation
                    if (edit) {
                        if (brainJadwal.updateJadwal(judul, bundle.getString("idj"), keterangan)) {
                            alert("data berhasil di ubah", context);
                            finish();
                        }
                    }
                    //tambah jadwal
                    else {

                        //region cek & ambil gambar
                        //MARK : obj
                        final BitmapDrawable bitmapDrawable = (BitmapDrawable) img.getDrawable();
                        Bitmap bitmap = null;
                        Boolean gambar;

                        //MARK : operation
                        try {
                            bitmap = bitmapDrawable.getBitmap();
                            gambar = true;
                        } catch (Exception e) {
                            gambar = false;
                        }
                        //endregion

                        //region insert data
                        //MARK : operation
                        if (gambar) {
                            if (brainJadwal.tambahJadwalGambar(keterangan, idmasjid, judul, imageToString(bitmap)))
                                cleanForm(edtKeterangan, edtJudul, context);
                        } else {
                            if (brainJadwal.tambahJadwal(keterangan, idmasjid, judul))
                                cleanForm(edtKeterangan, edtJudul, context);
                        }
                        //endregion
                    }
                }
            }
        });
        //endregion
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //MARK : operation
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {

                //MARK : obj
                final Uri uri = data.getData();
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                //MARK : UI
                final TextView txtTekan = findViewById(R.id.txtTekan);

                //MARK : operation
                img.setImageBitmap(bitmap);
                txtTekan.setVisibility(View.INVISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //region FUNCTION
    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void alert(String msg, Context context) {
        final AlertDialog alertDialog = new AlertDialog.Builder(activityTambahJadwal.this).create();

        //MARK : operation
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }

    private void cleanForm(EditText edtKeterangan, EditText edtJudul, Context context) {
        //MARK : function
        alert("jadwal berhasil disimpan", context);

        //MARK : UI
        final TextView txtTekan = findViewById(R.id.txtTekan);

        //MARK : operation
        edtKeterangan.setText("");
        edtJudul.setText("");
//        txtPilihJam.setText("Jam . . .");
        img.setImageResource(0);
        txtTekan.setVisibility(View.VISIBLE);
    }
    //endregion
}
