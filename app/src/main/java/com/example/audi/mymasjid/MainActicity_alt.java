package com.example.audi.mymasjid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by prata on 8/24/2017.
 */

public class MainActicity_alt extends AppCompatActivity {

    //untuk menu slet kanan, biar bisa di coding bukatutup
    public DrawerLayout drawer;

    //untuk coding waku ada menu yang di pilih, termasuk ambil item apa yang di klik
    public NavigationView nv;

    public ImageView imgmenu;

    public TabLayout tab;

    public Button btncari;

    public TextView txtcari;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_alt);

        initvalue();

        setlistener();

        setfragment("berita");
    }

    public void initvalue() {
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        imgmenu = (ImageView) findViewById(R.id.imgmenu);
        nv = (NavigationView) findViewById(R.id.nav_view);
        tab = (TabLayout) findViewById(R.id.tablayout);
        btncari = (Button) findViewById(R.id.btncari);
        txtcari = (TextView) findViewById(R.id.txtcari);
    }

    public void setlistener() {
        //hamburg menu open drawer, imgview listener
        imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        //navigation item on touch, navigation item listener
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawer.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.itemprofil:
                        break;
                    case R.id.itemfollow:
                        TabLayout.Tab tabitem = tab.getTabAt(1);
                        tabitem.select();
                        setfragment("masjid");
                        break;
                    case R.id.itemreminder:
                        break;
                    case R.id.itemlogout:
                        Intent i = new Intent(
                                getApplicationContext(),
                                Search.class
                        );
                        startActivity(i);
                        break;
                }
                return false;
            }
        });

        //codingan TAB
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //coding waktu Tab di pilih
                switch (tab.getPosition()) {
                    case 0:
                        setfragment("berita");
                        break;
                    case 1:
                        setfragment("masjid");
                        break;
                    case 2:
                        setfragment("dakwah");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        btncari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                        view.getContext(),
                        Search.class
                );

                Pair<View, String> p1 = Pair.create(
                        (View) txtcari,
                        ViewCompat.getTransitionName(txtcari)
                );

                Pair<View, String> p2 = Pair.create(
                        (View) imgmenu,
                        ViewCompat.getTransitionName(imgmenu)
                );

                ActivityOptionsCompat option = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        MainActicity_alt.this,
                        p1,
                        p2
                );

                startActivity(
                        i,
                        option.toBundle()
                );

                MainActicity_alt
                        .this
                        .overridePendingTransition(
                                android.R.anim.fade_in,
                                android.R.anim.fade_out
                        );
            }
        });
    }

    public void setfragment(String konten) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(
                R.anim.fade_in,
                R.anim.fade_out
        );

        switch (konten) {
            case "berita":
                berita berita = new berita();

                transaction
                        .replace(
                                R.id.llkonten,
                                berita
                        )
                        .commit();
                break;
            case "masjid":
                masjid masjid = new masjid();

                transaction
                        .replace(
                                R.id.llkonten,
                                masjid
                        )
                        .commit();
                break;
            case "dakwah":
                dakwah dakwah = new dakwah();

                transaction
                        .replace(
                                R.id.llkonten,
                                dakwah
                        )
                        .commit();
                break;
        }
    }

    public void header(View view) {

    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }
}
