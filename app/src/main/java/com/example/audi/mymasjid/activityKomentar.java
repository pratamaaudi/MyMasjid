package com.example.audi.mymasjid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

public class activityKomentar extends AppCompatActivity {

    ArrayList<classKomentar> classKomentars;
    RecyclerView rclKomen;
    adapterKomentar adapterKomentar;
    LinearLayoutManager llm;
    brainKomentar brain;
    Intent intent;
    String statusId, userId, username;
    ImageButton ibtnLike, ibtnSend;
    EditText txtKomentar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komentar);
        initvalue();
        setlistener();

        if (brain.cekLike(userId, statusId)) {
            ibtnLike.setSelected(true);
            ibtnLike.setBackgroundResource(R.drawable.ic_thumb_up_white_24dp);
        } else {
            ibtnLike.setSelected(false);
        }

        classKomentars = brain.loadKomentar(statusId, classKomentars);

        rclKomen.setLayoutManager(llm);
        rclKomen.setHasFixedSize(true);
        rclKomen.setAdapter(adapterKomentar);
        rclKomen.setNestedScrollingEnabled(true);
    }

    public void initvalue() {
        rclKomen = findViewById(R.id.rclKomen);
        llm = new LinearLayoutManager(activityKomentar.this);
        brain = new brainKomentar();

        intent = getIntent();
        statusId = intent.getStringExtra("statusid");
        userId = intent.getStringExtra("userid");
        username = intent.getStringExtra("username");

        classKomentars = new ArrayList<>();
        adapterKomentar = new adapterKomentar(activityKomentar.this, classKomentars, userId, "1",false);
        ibtnLike = findViewById(R.id.ibtnLike);
        ibtnSend = findViewById(R.id.ibtnSend);
        txtKomentar = findViewById(R.id.txtKomentar);
    }

    public void setlistener() {
        ibtnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ibtnLike.isSelected()) {

                    if (brain.dislike(userId, statusId)) {
                        ibtnLike.setBackgroundResource(R.drawable.ic_thumb_up_black_24dp);
                        ibtnLike.setSelected(false);
                    }

                } else {

                    if (brain.like(userId, statusId)) {
                        ibtnLike.setSelected(true);
                        ibtnLike.setBackgroundResource(R.drawable.ic_thumb_up_white_24dp);
                    }
                }
            }
        });

        ibtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String komen = txtKomentar.getText().toString();
                String idkomen = brain.komentar(komen, userId, statusId);
                if (!idkomen.equals(null)) {
                    int position = classKomentars.size();
                    classKomentars.add(new classKomentar(username, komen, userId, idkomen));
                    adapterKomentar.notifyItemChanged(position);
                    rclKomen.scrollToPosition(position);
                    txtKomentar.setText("");
                }
            }
        });
    }
}
