package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class activityDetailKegiatan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kegiatan);

        final Intent intent = getIntent();
        final brainKomentar brain = new brainKomentar();
        final String idKegiatan = intent.getStringExtra("idKegiatan");
        final Context context = activityDetailKegiatan.this;
        final String userId = intent.getStringExtra("idUser");
        final Boolean masjid = intent.getBooleanExtra("masjid",false);

        //region set Foto Masjid
        ImageView imgFotoMasjid = findViewById(R.id.imgFotoMasjid);

        //MARK : obj
        final String masjidId = intent.getStringExtra("masjidId");
        final String urlprof = "http://103.52.146.34/mymasjid/Foto_Masjid/" + masjidId + ".jpg";

        //MARK : operation
        Glide.with(this).load(urlprof).thumbnail(0.1f).into(imgFotoMasjid);
        //endregion

        //region set Nama Masjid
        final TextView txtNamaMasjid = findViewById(R.id.txtNamaMasjid);

        //MARK : operation
        txtNamaMasjid.setText(intent.getStringExtra("namaMasjid"));
        //endregion

        //region set Deskripsi Kegiatan
        final TextView txtStatus = findViewById(R.id.txtStatus);
        String deskripsi = intent.getStringExtra("deskripsi");

        txtStatus.setText("\n" + deskripsi);
        //endregion

        //region Set Gambar Kegiatan
        //MARK : UI
        final ImageView imgPost = findViewById(R.id.imgPost);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/masjid/Foto_Kegiatan/" + idKegiatan + ".jpg";

        //MARK : operation
        Glide.with(this).load(url).thumbnail(0.1f).into(imgPost);

        //MARK : trigger
        imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MARK : OBJ
                Intent i = new Intent(activityDetailKegiatan.this, activityDetailGambar.class);
                i.putExtra("url", url);

                //MARK : operation
                startActivity(i);
            }
        });
        //endregion

        //region Button Send Komentar
        final ImageButton ibtnSend = findViewById(R.id.ibtnSend);
        //MARK : trigger
        ibtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //koneksi ke kolom komentar
                //MARK : UI
                final EditText txtKomentar = findViewById(R.id.txtKomentar);
                final NestedScrollView scrPosting = findViewById(R.id.scrPosting);

                //persiapan variabel
                //MARK : obj
                final String komen, username;
                //region String
                komen = txtKomentar.getText().toString();
                username = intent.getStringExtra("username");
                //endregion

                //cek kolom komentar
                //MARK : operation
                if (!komen.equals("")) {

                    //tambahkan komentar di database dan di aplikasi
                    String idkomen = brain.komentarKegiatan(komen, userId, idKegiatan);
                    if (!idkomen.equals(null)) {

                        Log.e("mymasjidlog","idkomen setelah input = "+idkomen);

                        //MARK : add new item + scroll paling bawah
//                        classKomentars.add(new classKomentar(username, komen, userId, idkomen));
//                        adapterKomentar.notifyItemChanged(classKomentars.size());
//                        scrPosting.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                scrPosting.fullScroll(scrPosting.FOCUS_DOWN);
//                            }
//                        });
                        txtKomentar.setText("");
                    }
                }else
                    alert("mohon mengisi kolom komentar terlebihdahulu");
            }
        });
        //endregion

        //region Isi ArrayList Komentar
        brainKomentar brainKomentar = new brainKomentar();
        ArrayList<classKomentar> classKomentars = new ArrayList<>();
        classKomentars = brainKomentar.loadKomentarKegiatan(idKegiatan,classKomentars);
        //endregion

        //region Set Recyclerview Komentar
        RecyclerView rclKomen = findViewById(R.id.rclKomen);
        rclKomen.setLayoutManager(new LinearLayoutManager(context));
        rclKomen.setHasFixedSize(true);
        rclKomen.setAdapter(new adapterKomentar(context,classKomentars,userId,masjidId,masjid));
        rclKomen.setNestedScrollingEnabled(true);
        //endregion
    }

    private void alert(String msg) {

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityDetailKegiatan.this);

        //MARK : operation
        builder.setMessage(msg).show();
    }
}
