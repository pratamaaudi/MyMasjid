package com.example.audi.mymasjid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class activityTambahPengurus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_pengurus);

        //MARK : UI
        Button btnSimpan = findViewById(R.id.btnSimpan);

        //MARK : trigger
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : OBJ
                brainMasjid brainMasjid = new brainMasjid();
                Bundle extras = getIntent().getExtras();
                String idMasjid = extras.getString("idMasjid");

                //MARK UI
                EditText edtNama, edtJbtn, edtKontak;
                edtNama = findViewById(R.id.edtNama);
                edtJbtn = findViewById(R.id.edtJbtn);
                edtKontak = findViewById(R.id.edtKontak);

                //MARK : operation
                brainMasjid.tambahPengurusMasjid(idMasjid, edtNama.getText().toString(), edtJbtn.getText().toString(), edtKontak.getText().toString());
            }
        });
    }
}
