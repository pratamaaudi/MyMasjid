package com.example.audi.mymasjid;

/**
 * Created by MOFOSEFES on 10/14/2017.
 */

public class classPosting {
    private String namaMasjid;
    private String status;
    private String topik;
    private String statusId;
    private String masjidId;
    private String tanggal;

    public classPosting(String namaMasjid, String status, String statusId, String masjidId, String tanggal, String topik) {
        setNamaMasjid(namaMasjid);
        setStatus(status);
        setStatusId(statusId);
        setMasjidId(masjidId);
        setTanggal(tanggal);
        setTopik(topik);
    }

    public String getNamaMasjid() {
        return namaMasjid;
    }

    public void setNamaMasjid(String namaMasjid) {
        this.namaMasjid = namaMasjid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getMasjidId() {
        return masjidId;
    }

    public void setMasjidId(String masjidId) {
        this.masjidId = masjidId;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTopik() {
        return topik;
    }

    public void setTopik(String topik) {
        this.topik = topik;
    }
}
