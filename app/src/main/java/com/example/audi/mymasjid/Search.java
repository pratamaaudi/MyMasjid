package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Search extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //MARK : UI
        final ImageView imgmenu = findViewById(R.id.imgmenu);
        final TextView txtcari = findViewById(R.id.txtcari);

        //MARK : trigger
        imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchMasjid(txtcari.getText().toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Search.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void searchMasjid(final String query) {

        //MARK : obj
        final Context context = Search.this;

        //region show loading
        //MARK : obj
        final ProgressDialog pDialog = new ProgressDialog(context);

        //MARK : operation
        pDialog.setMessage("Loading . . .");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        //endregion

        //region async
        //MARK : operation
        new AsyncJob.AsyncJobBuilder<Boolean>();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //region isi arraylist classmasjids
                //MARK : obj
                final brainSearch brainSearch = new brainSearch();
                ArrayList<classMasjid> classMasjids = new ArrayList<>();

                //MARK : operation
                classMasjids = brainSearch.searchMasjid(classMasjids, query);
                //endregion

                //MARK : obj
                final classLogin classLogin = getIntent().getExtras().getParcelable("classLogin");
                final adapterMasjid adapterMasjid = new adapterMasjid(context, classMasjids, classLogin);
                final LinearLayoutManager llm = new LinearLayoutManager(context);

                //MARK : obj
                final Boolean empty;

                if (classMasjids.size() == 0)
                    empty = true;
                else
                    empty = false;


                //region UI THREAD
                //MARK : UI
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {

                        if (empty) {

                            //MARK : obj
                            final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                            alertDialog.setMessage("maaf, tidak ada masjid yang sesuai dengan pencarian anda");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        } else {
                            //MARK : UI
                            final RecyclerView rclmasjid = findViewById(R.id.rclmasjid);

                            //MARK : operation
                            rclmasjid.setLayoutManager(llm);
                            rclmasjid.setHasFixedSize(true);
                            rclmasjid.setAdapter(adapterMasjid);
                        }

                        pDialog.dismiss();

                    }
                });
                //endregion
            }
        });
        //endregion
    }
}
