package com.example.audi.mymasjid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

public class activityChat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //MARK : global
        final brainChat brainChat = new brainChat();
        String idroom;
        final Bundle bundle = getIntent().getExtras();
        final String idp = bundle.getString("idp");

        //region getidroom
        //MARK : operation
        if (bundle.getBoolean("masjid"))
            idroom = bundle.getString("idr");
        else {
            idroom = bundle.getString("idr");
            if (idroom == null)
                idroom = brainChat.loadIdRoom(idp, bundle.getString("idm"));
        }
        //endregion

        //region chatview
        //MARK : UI
        final ChatView chatView = findViewById(R.id.chat_view);

        //MARK : obj
        final String idr = idroom;

        //MARK : operation
        chatView.addMessages(brainChat.loadChat(idp, idroom));
        if (bundle.getBoolean("kegiatan")) {
            final String msg = "==========================================\nNama Kegiatan : " + bundle.getString("namakeg") + "\nDeskripsi : " + bundle.getString("namakeg") + "\nMulai : " + bundle.getString("mulai") + "\nSelesai : " + bundle.getString("selesai") + "\n==========================================";
            final Long timestamp = System.currentTimeMillis();
            if (brainChat.inputChat(msg, String.valueOf(timestamp), idp, idr, bundle.getString("token")))
                chatView.addMessage(new ChatMessage(msg, timestamp, ChatMessage.Type.SENT));
        }

        //MARK : trigger
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {

                //input pesan ke database dan menampilkan pesan di aplikasi
                return brainChat.inputChat(chatMessage.getMessage(), String.valueOf(chatMessage.getTimestamp()), idp, idr, bundle.getString("token"));

            }
        });
        //endregion

        final TextView txtSubjek = findViewById(R.id.txtSubjek);

        txtSubjek.setText(bundle.getString("subjek"));

    }
}
