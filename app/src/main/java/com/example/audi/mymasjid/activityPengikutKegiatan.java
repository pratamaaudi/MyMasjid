package com.example.audi.mymasjid;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class activityPengikutKegiatan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengikut_kegiatan);

        //region isi class logins
        //MARK : obj
        final brainKegiatan brainKegiatan = new brainKegiatan();
        final ArrayList<classLogin> classLogins;

        //MARK : operation
        classLogins = brainKegiatan.loadPengikutKegiatan(getIntent().getExtras().getString("idk"));
        //endregion

        //region load data recyclerview
        //MARK : obj
        final adapterPengikutMasjid adapterPengikutMasjid = new adapterPengikutMasjid(classLogins);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activityPengikutKegiatan.this);

        //MARK : UI
        final RecyclerView rclPengikutMasjid = findViewById(R.id.rclPengikutMasjid);

        //MARK : operation
        rclPengikutMasjid.setLayoutManager(linearLayoutManager);
        rclPengikutMasjid.setHasFixedSize(true);
        rclPengikutMasjid.setAdapter(adapterPengikutMasjid);
        rclPengikutMasjid.setNestedScrollingEnabled(true);
        //endregion

        //region constrantnokonten
        //MARK : UI
        final ConstraintLayout constrantnokonten = findViewById(R.id.constrantnokonten);

        //MARK : operation
        if (classLogins.size() < 1)
            constrantnokonten.setVisibility(View.VISIBLE);
        else
            constrantnokonten.setVisibility(View.INVISIBLE);

        //endregion
    }
}
