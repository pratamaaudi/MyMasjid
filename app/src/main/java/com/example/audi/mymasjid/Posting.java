package com.example.audi.mymasjid;

/**
 * Created by prata on 8/30/2017.
 */

public class Posting {
    private String namaMasjid;
    private String status;
    private Integer id;

    public Posting(String namaMasjid, String status, Integer id) {
        setNamaMasjid(namaMasjid);
        setStatus(status);
        setId(id);
    }

    public String getNamaMasjid() {
        return namaMasjid;
    }

    public void setNamaMasjid(String namaMasjid) {
        this.namaMasjid = namaMasjid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
