package com.example.audi.mymasjid;

public class classRoom {

    private String id;
    private String counter;

    public classRoom(String id, String counter){
        setId(id);
        setCounter(counter);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }
}
