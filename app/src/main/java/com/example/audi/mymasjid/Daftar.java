package com.example.audi.mymasjid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class Daftar extends AppCompatActivity {

    RadioButton rdoAkunUser, rdoAkunMasjid;
    Button btnDaftar;
    EditText edtUsername, edtPass, edtrepass, edtEmail;
    TextView txtError;
    brain_daftar brain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        initvalue();

        setlistener();
    }

    public void initvalue() {
        rdoAkunUser = (RadioButton) findViewById(R.id.rdoakunuser);
        rdoAkunMasjid = (RadioButton) findViewById(R.id.rdoakunmasjid);

        btnDaftar = (Button) findViewById(R.id.btnDaftar);

        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPass = (EditText) findViewById(R.id.edtPass);
        edtrepass = (EditText) findViewById(R.id.edtRepass);
        edtEmail = (EditText) findViewById(R.id.edtEmail);

        txtError = (TextView) findViewById(R.id.txtError);

        brain = new brain_daftar();
    }

    public void setlistener() {
        rdoAkunUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDaftar.setText("Daftar");
            }
        });

        rdoAkunMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDaftar.setText("Lanjut");
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtUsername.getText().toString().equals("") ||
                        edtPass.getText().toString().equals("") ||
                        edtrepass.getText().toString().equals("") ||
                        edtEmail.getText().toString().equals("")
                        ) {
//ada yang kosong
                    txtError.setText("mohon mengisi semua kolom data");
                    txtError.setVisibility(View.VISIBLE);
                } else {
                    if (edtPass.getText().toString().equals(edtrepass.getText().toString())) {
                        if (rdoAkunUser.isChecked()) {
//user
                            if (brain.cekemail(edtEmail.getText().toString())) {
                                txtError.setText("email sudah terdaftar");
                                txtError.setVisibility(View.VISIBLE);
                            } else {

                                if(brain.daftar_user(edtUsername.getText().toString(),
                                        edtEmail.getText().toString(),
                                        edtPass.getText().toString())){
                                    txtError.setText("data berhasil dimasukkan");
                                    txtError.setVisibility(View.VISIBLE);
                                } else {
                                    txtError.setText("data gagal dimasukkan");
                                    txtError.setVisibility(View.VISIBLE);
                                }
                            }

                        } else {
                            //masjid
                        }
                    } else {
//password dan re pass tidak sama
                        txtError.setText("password dan konfirmasi password tidak sama");
                        txtError.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }
}
