package com.example.audi.mymasjid;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class activityTambahKegitan extends AppCompatActivity {

    ImageView img;
    String jamString, menitString;
    Boolean edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_kegitan);

        //MARK : glb-obj
        final Context context = activityTambahKegitan.this;
        final Bundle bundle = getIntent().getExtras();
        edit = false;
        ///MARK :UI
        final EditText edtNamaKegiatan, edtDeskripsi;
        //region Edittext
        edtDeskripsi = findViewById(R.id.edtDeskripsi);
        edtNamaKegiatan = findViewById(R.id.edtNamaKegiatan);
        //endregion

        //MARK : UI
        img = findViewById(R.id.img);

        //region Textview date picker
        //MARK : UI
        final TextView txtTanggalMulai, txtTanggalSelesai;
        //region TextView
        txtTanggalMulai = findViewById(R.id.txtTanggalMulai);
        txtTanggalSelesai = findViewById(R.id.txtTanggalSelesai);
        //endregion

        //MARK : obj
        final DatePickerDialog.OnDateSetListener dateSetListenerMulai = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                txtTanggalMulai.setText(generateStringDate(dayOfMonth, month, year));
            }
        };
        final DatePickerDialog.OnDateSetListener dateSetListenerSelesai = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                txtTanggalSelesai.setText(generateStringDate(dayOfMonth, month, year));
            }
        };

        //MARK : trigger
        txtTanggalMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDatePicker(dateSetListenerMulai, context);
            }
        });

        txtTanggalSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDatePicker(dateSetListenerSelesai, context);
            }
        });
        //endregion

        //region txtPilihJamMulai
        //MARK : UI
        final TextView txtPilihJamMulai = findViewById(R.id.txtJamMulai);

        //MARK : trigger
        txtPilihJamMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Calendar calendar = Calendar.getInstance();
                final Integer jam = calendar.get(Calendar.HOUR_OF_DAY);
                final Integer menit = calendar.get(Calendar.MINUTE);
                final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        setJam(hourOfDay, minute, txtPilihJamMulai);
                    }
                }, jam, menit, true);

                //MARK : operation
                timePickerDialog.show();

            }
        });
        //endregion

        //region txtPilihJamSelesai
        //MARK : UI
        final TextView txtPilihJamSelesai = findViewById(R.id.txtJamSelesai);

        //MARK : trigger
        txtPilihJamSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Calendar calendar = Calendar.getInstance();
                final Integer jam = calendar.get(Calendar.HOUR_OF_DAY);
                final Integer menit = calendar.get(Calendar.MINUTE);
                final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        setJam(hourOfDay, minute, txtPilihJamSelesai);
                    }
                }, jam, menit, true);

                //MARK : operation
                timePickerDialog.show();

            }
        });
        //endregion

        //region check Edit Mode
        //MARK : operation
        try {
            edit = bundle.getBoolean("edit");
        } catch (Exception e) {
        }

        if (edit) {

            //MARK : obj
            final brainKegiatan brainKegiatan = new brainKegiatan();
            final classKegiatan classKegiatan = brainKegiatan.loadKegiatanMasjidId(bundle.getString("idk"));

            //MARK : operation
            edtNamaKegiatan.setText(classKegiatan.getNamaKegiatan());
            txtTanggalMulai.setText(classKegiatan.getTanggalMulai());
            txtTanggalSelesai.setText(classKegiatan.getTanggalSelesai());
            txtPilihJamMulai.setText(classKegiatan.getJamMulai());
            txtPilihJamSelesai.setText(classKegiatan.getJamSelesai());
            edtDeskripsi.setText(classKegiatan.getDeskripsi());
        }
        //endregion

        //region Framelayout
        //MARK : UI
        final FrameLayout frm = findViewById(R.id.frm);

        //MARK : operation
        if (edit)
            frm.setVisibility(View.INVISIBLE);

        //MARK : trigger
        frm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                Intent intent = new Intent();

                //MARK : operation
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "select image from gallery"), 1);
            }
        });
        //endregion

        //region btn simpan
        //MARK : UI
        final ImageButton btnSimpan = findViewById(R.id.btnSimpan);

        //MARK : operation
//        if (edit)
//            btnSimpan.setText("SIMPAN");

        //MARK : trigger
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                brainLog brainLog = new brainLog();
                brainLog.Log("acttambahkegiatan", "btnsimpan", "onclick");

                //MARK : obh
                final String nama_kegiatan = edtNamaKegiatan.getText().toString();
                final String tgl_mulai = txtTanggalMulai.getText().toString();
                final String jam_mulai = txtPilihJamMulai.getText().toString();
                final String tgl_selesai = txtTanggalSelesai.getText().toString();
                final String jam_selesai = txtPilihJamSelesai.getText().toString();

                if (nama_kegiatan.equals(""))
                    alert("mohon mengisi nama kegiatan", context);
                else {
                    if (tgl_mulai.equals("Pilih Tanggal . . .") || jam_mulai.equals("Pilih Jam . . .") || tgl_selesai.equals("Pilih Tanggal . . .") || jam_selesai.equals("Pilih Jam . . ."))
                        alert("mohon melengkapi waktu pelaksanaan kegiatan", context);
                    else {

                        //region cek & ambil gambar
                        //MARK : obj
                        final BitmapDrawable bitmapDrawable = (BitmapDrawable) img.getDrawable();
                        Bitmap bitmap = null;
                        Boolean gambar;

                        //MARK : operation
                        try {
                            bitmap = bitmapDrawable.getBitmap();
                            gambar = true;
                        } catch (Exception e) {
                            gambar = false;
                        }
                        //endregion

                        //region upload data
                        //MARK : obj
                        final brainKegiatan brainKegiatan = new brainKegiatan();
                        final String namaKegiatan, tglMulai, tglSelesai, deskripsi, idMasjid;
                        namaKegiatan = edtNamaKegiatan.getText().toString();
                        tglMulai = txtTanggalMulai.getText().toString() + " " + txtPilihJamMulai.getText().toString();
                        tglSelesai = txtTanggalSelesai.getText().toString() + " " + txtPilihJamSelesai.getText().toString();
                        deskripsi = edtDeskripsi.getText().toString();
                        idMasjid = bundle.getString("idm");

                        //MARK : operation
                        if (gambar)
                            finishing(brainKegiatan.tambahKegiatanGambar(namaKegiatan, tglMulai, tglSelesai, deskripsi, idMasjid, imageToString(bitmap)), context, edtNamaKegiatan, txtTanggalMulai, txtPilihJamMulai, txtTanggalSelesai, txtPilihJamSelesai, edtDeskripsi, edit);
                        else {
                            if (edit) {
                                finishing(brainKegiatan.updateKegiatan(namaKegiatan, tglMulai, tglSelesai, deskripsi, bundle.getString("idk")), context, edtNamaKegiatan, txtTanggalMulai, txtPilihJamMulai, txtTanggalSelesai, txtPilihJamSelesai, edtDeskripsi, edit);
                            } else
                                finishing(brainKegiatan.tambahKegiatan(namaKegiatan, tglMulai, tglSelesai, deskripsi, idMasjid), context, edtNamaKegiatan, txtTanggalMulai, txtPilihJamMulai, txtTanggalSelesai, txtPilihJamSelesai, edtDeskripsi, edit);
                        }
                        //endregion

                    }
                }
            }
        });
        //endregion
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //MARK : operation
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {

                //MARK : obj
                final Uri uri = data.getData();
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                //MARK : UI
                final TextView txtTekan = findViewById(R.id.txtTekan);

                //MARK : operation
                img.setImageBitmap(bitmap);
                txtTekan.setVisibility(View.INVISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //region FUNCTION
    //MARK : function
    private String generateStringDate(Integer day, Integer month, Integer year) {
        return year + "-" + (month + 1) + "-" + day;
    }

    private void setDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener, Context context) {
        Calendar calendar = Calendar.getInstance();
        Integer tahun = calendar.get(Calendar.YEAR);
        Integer bulan = calendar.get(Calendar.MONTH);
        Integer tanggal = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, android.R.style.Theme_Material_Light_Dialog, onDateSetListener, tahun, bulan, tanggal);

        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        datePickerDialog.show();
    }

    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void setJam(Integer jam, Integer min, TextView txtTarget) {
        if (jam < 10) {
            jamString = "0" + String.valueOf(jam);
        } else {
            jamString = String.valueOf(jam);
        }

        if (min < 10) {
            menitString = "0" + String.valueOf(min);
        } else {
            menitString = String.valueOf(min);
        }
        txtTarget.setText(jamString + ":" + menitString);
    }

    private void alert(String msg, Context context) {
        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //MARK : operation
        builder.setMessage(msg).show();
    }

    private void finishing(Boolean insert, Context context, EditText edtNamaKegiatan, TextView txtTanggalMulai, TextView txtPilihJamMulai, TextView txtTanggalSelesai, TextView txtPilihJamSelesai, EditText edtDeskripsi, Boolean edit) {
        if (insert) {

            //MARK : obj
            final String ts, js;
            ts = "Pilih Tanggal . . .";
            js = "Pilih Jam . . .";

            //MARK : operation
            alert("kegiatan berhasil di tambah", context);
            edtNamaKegiatan.setText("");
            txtTanggalMulai.setText(ts);
            txtPilihJamMulai.setText(js);
            txtTanggalSelesai.setText(ts);
            txtPilihJamSelesai.setText(js);
            edtDeskripsi.setText("");
            img.setImageResource(0);

            if (edit)
                finish();

        } else
            alert("kegiatan gagal di tambah", context);
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
    //endregion
}
