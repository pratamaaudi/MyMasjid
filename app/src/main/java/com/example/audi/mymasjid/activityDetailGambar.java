package com.example.audi.mymasjid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class activityDetailGambar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gambar);

        //MARK : OBJ
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");

        //MARK : UI
        ImageView imgPost = findViewById(R.id.imgPost);

        //MARK : operation
        Glide.with(activityDetailGambar.this).load(url).thumbnail(1.0f).into(imgPost);
    }
}
