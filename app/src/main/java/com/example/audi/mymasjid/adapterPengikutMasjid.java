package com.example.audi.mymasjid;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class adapterPengikutMasjid extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<classLogin> classLogins;

    public adapterPengikutMasjid(ArrayList<classLogin> classLogins){
        this.classLogins = classLogins;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //MARK : obj
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pengikut_masjid, parent, false);
        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        //MARK : operation
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //MARK : gbl-UI
        final TextView txtUsername, txtEmail, txtDaerah;

        //MARK : glb-obj
        final View view = holder.itemView;
        final classLogin classLogin = classLogins.get(position);

        //region txtUsername
        //MARK : UI
        txtUsername = view.findViewById(R.id.txtUsername);

        //MARK : operation
        txtUsername.setText(classLogin.getUsername());
        //endregion

        //region txtEmail
        //MARK : UI
        txtEmail = view.findViewById(R.id.txtEmail);

        //MARK : operation
        txtEmail.setText(classLogin.getEmail());
        //endregion

        //region txtDaerah
        //MARK : UI
        txtDaerah = view.findViewById(R.id.txtDaerah);

        //MARK : operation
        txtDaerah.setText(classLogin.getProvinsi()+", "+classLogin.getKotakab()+", "+classLogin.getKecamatan());
        //endregion

    }

    @Override
    public int getItemCount() {
        return classLogins.size();
    }
}
