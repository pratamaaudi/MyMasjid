package com.example.audi.mymasjid;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;
import com.bumptech.glide.Glide;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.pixplicity.easyprefs.library.Prefs;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;

import java.util.ArrayList;
import java.util.List;

public class activityDetailMasjid extends AppCompatActivity {

    adapterPosting adapterPosting;
    ArrayList<classPosting> arrayListPosting;
    Boolean oncreate = false;
    Boolean masjid, blokir, add;
    String idMasjid, unreadKegiatan, unreadKegiatanRutin;
    classLogin classLogin;
    classMasjid classMasjid;
    List<String> topik;
    int lastFirstVisiblePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_masjid);

        oncreate = true;

        //region async

        //region show loading
        //MARK : obj
//        final ProgressDialog pDialog = new ProgressDialog(activityDetailMasjid.this);
//
//        //MARK : operation
//        pDialog.setMessage("Loading . . .");
//        pDialog.setIndeterminate(false);
//        pDialog.setCancelable(false);
//        pDialog.show();
        //endregion

        //MARK : obj
        final Context context = activityDetailMasjid.this;
        arrayListPosting = new ArrayList<>();

        //MARK : operarion
        new AsyncJob.AsyncJobBuilder<Boolean>();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //MARK : global-obj
                final Bundle extras = getIntent().getExtras();
                final String judul;
                classLogin = extras.getParcelable("classLogin");
                final brainMasjid brainMasjid = new brainMasjid();
                final String idLogin, jmlFollower, jmlKegiatan, url;
                //region String
                idLogin = classLogin.getId();
                idMasjid = extras.getString("id");
                jmlFollower = brainMasjid.loadFollowerMasjid(idMasjid);
                jmlKegiatan = brainMasjid.loadJmlKegiatanMasjid(idMasjid);
                url = "http://103.52.146.34/mymasjid/Foto_Masjid/" + idMasjid + ".jpg";
                //endregion

                classMasjid = brainMasjid.loadProfilMasjid(idMasjid);

                //region cek masjid atau bukan
                //MARK : obj
                final String namaMasjid = classMasjid.getNama();
                final String jenisAkun = extras.getString("jenisAkun");

                //MARK : operation
                if (!jenisAkun.equals("masjid")) {
                    judul = extras.getString("judul");
                    masjid = false;
                } else {

                    //region set judul dan masjid
                    judul = namaMasjid;
                    masjid = true;
                    //endregion

                    //region updateToken
                    //MARK : obj
                    final brainLogin brainLogin = new brainLogin();

                    //MARK : operation
//                    brainLogin.updateToken(FirebaseInstanceId.getInstance().getToken(), idLogin);
                    //endregion
                }
                //endregion

                refreshData();

                //MARK : obj
                adapterPosting = new adapterPosting(context, arrayListPosting, classLogin, masjid, true);

                //MARK : obj
                topik = brainMasjid.loadTopikMasjid(idMasjid, false);

                brainLog brainLog = new brainLog();
//                brainLog.Log("act detail masjid", "add",String.valueOf(add));
                brainLog.Log("act detail masjid", "masjid", String.valueOf(masjid));

                //MARK : operation
                if (!masjid) {


                    final brainBlokir brainBlokir = new brainBlokir();
                    if (brainBlokir.cekBlokir(idLogin, idMasjid)) {

                        blokir = true;
                        add = false;

                    } else {

                        blokir = false;
                        if (brainMasjid.cekFollowMasjid(idLogin, idMasjid)) {

                            //MARK : obj
                            final brainProfil brainProfil = new brainProfil();
                            List<String> strings = brainProfil.loadUnreadKegiatan(idLogin, idMasjid);

                            //MARK : obj
                            final brainKegiatan brainKegiatan = new brainKegiatan();
                            unreadKegiatan = brainKegiatan.loadUnreadKegiatan(idMasjid, strings.get(0));

                            //MARK : obj
                            final brainJadwal brainJadwal = new brainJadwal();
                            unreadKegiatanRutin = brainJadwal.loadUnreadKegiatanRutin(idMasjid, strings.get(1));
                            add = false;

                        } else {

                            unreadKegiatan = "-";
                            unreadKegiatanRutin = "-";

                            add = true;

                        }
                    }
                } else {
                    blokir = false;
                    add = false;
                }


                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {

                        //region open nav drawer
                        //MARK : UI
                        final DrawerLayout drawer = findViewById(R.id.drawer);

                        //MARK : operation
                        drawer.openDrawer(GravityCompat.START);
                        //endregion

                        //region header animation
                        //MARK : UI
                        final AppBarLayout appBarLayout = findViewById(R.id.appbar);
                        final LinearLayout ll, ll2;
                        ll = findViewById(R.id.llbar);
                        ll2 = findViewById(R.id.llbar2);

                        //MARK : operation
                        ll2.setVisibility(View.VISIBLE);
                        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                            boolean isShow = false;
                            int scrollRange = -1;

                            @Override
                            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                                if (scrollRange == -1) {
                                    scrollRange = appBarLayout.getTotalScrollRange();
                                }
                                if (scrollRange + verticalOffset == 0) {
                                    ll.setVisibility(View.VISIBLE);
                                    ll.animate().alpha(1.0f);
                                    ll2.animate().alpha(0.0f);
                                    ll2.setVisibility(View.INVISIBLE);
                                    isShow = true;
                                } else if (isShow) {
                                    ll.animate().alpha(0.0f);
                                    ll.setVisibility(View.INVISIBLE);
                                    ll2.setVisibility(View.VISIBLE);
                                    ll2.animate().alpha(1.0f);
                                    isShow = false;
                                }
                            }
                        });
                        //endregion

                        //region imgMenu
                        //MARK : UI
                        final ImageView imgmenu = findViewById(R.id.imgmenu);

                        //MARK : trigger
                        imgmenu.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                drawer.openDrawer(GravityCompat.START);
                            }
                        });
                        //endregion

                        //region nav drawer
                        //MARK : UI
                        final NavigationView nv = findViewById(R.id.nav_view);
                        final Menu menu = nv.getMenu();

                        //MARK : obj
                        MenuItem namamasjid = menu.findItem(R.id.subheader_masjid);

                        //region set judul nav drawer
                        //MARK : operation
                        namamasjid.setTitle(judul);
                        //endregion

                        //region set menu ke admin jika masjid
                        //MARK : operation
                        if (masjid) {
                            nv.getMenu().clear();
                            nv.inflateMenu(R.menu.navi_drawer_menu_admin);
                        } else {

                            //region set unread kegiatan
                            //MARK : obj
                            final MenuItem itemkegiatan = menu.findItem(R.id.itemkegiatan);

                            //MARK : operation
                            itemkegiatan.setTitle("Kegiatan Insidentil - " + unreadKegiatan);
                            //endregion

                            //region set unread jadwal
                            //MARK : obj
                            final MenuItem jadwal = menu.findItem(R.id.jadwal);

                            //MARK : operation
                            jadwal.setTitle("Kegiatan Reguler - " + unreadKegiatanRutin);
                            //endregion

                        }
                        //endregion

                        //region set nav drawer trigger
                        if (masjid) {
                            nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                                @Override
                                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                                    //MARK : obj
                                    final Intent i;

                                    //MARK : operation

                                    //region tutup drawer
                                    drawer.closeDrawers();
                                    //endregion

                                    //region trigger per item
                                    switch (item.getItemId()) {
                                        case R.id.tambah_jadwal:
                                            i = new Intent(context, activityTambahJadwal.class);
                                            i.putExtra("idmasjid", idMasjid);
                                            startActivity(i);
                                            break;
                                        case R.id.jadwal:
                                            loadJadwal(context, masjid, idLogin, idMasjid);
                                            break;
                                        case R.id.edit_masjid:
                                            i = new Intent(context, activityEditProfilMasjid.class);
                                            i.putExtra("classMasjid", classMasjid);
                                            i.putExtra("classLogin", classLogin);
                                            startActivity(i);
                                            break;
                                        case R.id.tambah_kegiatan:
                                            i = new Intent(context, activityTambahKegitan.class);
                                            i.putExtra("idm", idMasjid);
                                            startActivity(i);
                                            break;
                                        case R.id.itemlapor:
                                            i = new Intent(context, activityListLapor.class);
                                            i.putExtra("masjid", masjid);
                                            i.putExtra("mid", idMasjid);
                                            startActivity(i);
                                            break;
                                        case R.id.itemBlokir:
                                            i = new Intent(context, activityBlokir.class);
                                            i.putExtra("mid", idMasjid);
                                            startActivity(i);
                                            break;
                                        case R.id.itemChat:
                                            loadListChat(context, idMasjid, idLogin);
                                            break;
                                        case R.id.itemkegiatan:
                                            loadKegiatanMasjid(context, masjid, idMasjid, idLogin);
                                            break;
                                        case R.id.itemlogout:
                                            logOut(context);
                                            break;
                                        case R.id.imgHeader:
                                            break;
                                        default:
                                            break;
                                    }
                                    return false;
                                    //endregion
                                }
                            });
                        } else {

                            nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                                @Override
                                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                                    //MARK : obj
                                    final Intent i;

                                    //MARK : operation

                                    //region tutup drawer
                                    drawer.closeDrawers();
                                    //endregion

                                    //region trigger per item
                                    switch (item.getItemId()) {
                                        case R.id.itemChat:
                                            chatButton(context, masjid, idLogin, idMasjid);
                                            break;
                                        case R.id.itemkegiatan:

                                            loadKegiatanMasjid(context, masjid, idMasjid, idLogin);
                                            break;
                                        case R.id.tambah_kegiatan:

                                            //MARK : operation
                                            i = new Intent(context, admin.class);
                                            startActivity(i);
                                            break;
                                        case R.id.jadwal:
                                            //MARK : operation
                                            loadJadwal(context, masjid, idLogin, idMasjid);
                                            break;
                                        case R.id.itemprofil:

                                            //region pindah ke halaman profil
                                            //MARK : obj
                                            i = new Intent(context, activityProfil.class);

                                            //MARK : operation
                                            i.putExtra("classLogin", classLogin);
                                            startActivity(i);
                                            //endregion
                                            break;
                                        case R.id.itemreminder:

                                            //region pindah ke halaman kegiatan user
                                            //MARK : obj
                                            i = new Intent(getApplicationContext(), activityKegiatanUser.class);

                                            //MARK : operation
                                            i.putExtra("uid", idLogin);
                                            startActivity(i);
                                            //endregion
                                            break;
                                        case R.id.itemlogout:

                                            //MARK : function
                                            logOut(context);
                                            break;
                                        default:
                                            break;
                                    }
                                    return false;
                                    //endregion

                                }
                            });
                        }
                        //endregion

                        //region nav drawer header
                        //MARK : UI
                        final View header = nv.getHeaderView(0);
                        final ImageView imgHeader = header.findViewById(R.id.imgHeader);

                        //MARK : trigger
                        imgHeader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        //endregion
                        //endregion

                        //region recyclerview
                        //MARK : UI
                        final RecyclerView recyclerView = findViewById(R.id.rc2);
                        final LinearLayoutManager llm = new LinearLayoutManager(context);

                        //MARK : operation
                        recyclerView.setLayoutManager(llm);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(adapterPosting);
                        recyclerView.setNestedScrollingEnabled(false);
                        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
                        //endregion

                        //region txtJudul / Nama Masjid
                        //MARK : UI
                        final TextView txtnamaMasjid = findViewById(R.id.namaMasjid);

                        //MARK : operation
                        txtnamaMasjid.setText(judul);
                        //endregion

                        //region set tri button
                        //MARK : UI
                        final ImageButton btnChat;
                        final Button btnFollow, btnKegiatan;
                        //region Button
                        btnFollow = findViewById(R.id.btnfollow);
                        btnKegiatan = findViewById(R.id.btnkegiatan);
                        btnChat = findViewById(R.id.btnChat);
                        //endregion
                        final ImageButton ibtnFollow, ibtnKegiatan;
                        //region Image Button
                        ibtnFollow = findViewById(R.id.ibtnfollow);
                        ibtnKegiatan = findViewById(R.id.ibtnkegiatan);
                        //endregion

                        //MARK : operation
                        btnFollow.setText(jmlFollower);
                        btnKegiatan.setText(jmlKegiatan);

                        //MARK : trigger
                        btnFollow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadFollowMasjid(context);
                            }
                        });
                        btnChat.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                chatButton(context, masjid, idLogin, idMasjid);
                            }
                        });
                        btnKegiatan.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadKegiatanMasjid(context, masjid, idMasjid, idLogin);
                            }
                        });
                        ibtnFollow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadFollowMasjid(context);
                            }
                        });
                        ibtnKegiatan.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadKegiatanMasjid(context, masjid, idMasjid, idLogin);
                            }
                        });
                        //endregion

                        //region load image masjid
                        //MARK : UI
                        final ImageView app_bar_image = findViewById(R.id.app_bar_image);

                        //MARK : operation
                        Glide.with(context).load(url).thumbnail(1.0f).into(app_bar_image);
                        //endregion

                        //region FAB
                        //MARK : UI
                        final FloatingActionButton fab = findViewById(R.id.floatingActionButton);

                        //MARK : operation
                        if (masjid) {

                            //MARK : operation
                            fab.setVisibility(View.VISIBLE);

                            //MARK : trigger
                            fab.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    //MARK : obj
                                    Intent intent = new Intent(context, activityTambahPosting.class);

                                    //MARK : operation
                                    intent.putExtra("mid", idMasjid);
                                    startActivity(intent);
                                }
                            });
                        }
                        //endregion

                        //region progressbar
                        //MARK : UI
                        final ProgressBar prb = findViewById(R.id.prb);

                        //MARK : operation
                        prb.setVisibility(View.INVISIBLE);
                        //endregion

                        //region imgDetail
                        //MARK : UI
                        ImageView imgDetail = findViewById(R.id.imgDetail);

                        //MARK : obj
                        final DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(context, imgDetail);

                        final DroppyMenuPopup droppyMenu = droppyBuilder.fromMenu(R.menu.menu_detail_masjid)
                                .triggerOnAnchorClick(false)
                                .setOnClick(new DroppyClickCallbackInterface() {
                                    @Override
                                    public void call(View v, final int id) {
                                        switch (id) {
                                            case R.id.info:
                                                alert(context, "Nama Masjid : " + classMasjid.getNama() + "\nKontak : " + classMasjid.getKontak() + "\nAlamat : " + classMasjid.getAlamat());
                                                break;
                                            case R.id.lapor:
                                                if (masjid)
                                                    alert(context, "tidak dapat melaporkan masjid jika login sebagai masjid");
                                                else {

                                                    //MARK : obj
                                                    final Intent intent = new Intent(context, activityLaporMasjid.class);

                                                    //MARK : operation
                                                    intent.putExtra("pelapor", classLogin.getUsername());
                                                    intent.putExtra("masjid", classMasjid.getNama());
                                                    intent.putExtra("idp", classLogin.getId());
                                                    intent.putExtra("idm", classMasjid.getId());
                                                    startActivity(intent);
                                                }
                                                break;
                                        }
                                    }
                                })
                                .setOnDismissCallback(new DroppyMenuPopup.OnDismissCallback() {
                                    @Override
                                    public void call() {
                                    }
                                })
                                .setPopupAnimation(new DroppyFadeInAnimation())
                                .setXOffset(5)
                                .setYOffset(5)
                                .build();

                        imgDetail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                droppyMenu.show();
                            }
                        });
                        //endregion

                        //region tutup drawer
//                        pDialog.dismiss();
                        //endregion

                        //region spnTopik
                        //MARK : UI
                        MaterialSpinner spnTopik = findViewById(R.id.spnTopik);

                        //MARK : operation
                        spnTopik.setVisibility(View.VISIBLE);
                        spnTopik.setItems(topik);

                        //MARK : trigger
                        spnTopik.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                switch (position) {
                                    case 0:
                                        refreshData();
                                        break;
                                    default:
                                        final brainBerita brainBerita = new brainBerita();
                                        arrayListPosting.clear();
                                        brainBerita.loadPostingbytopik(idMasjid, topik.get(position), arrayListPosting);
                                        break;
                                }
                                adapterPosting = new adapterPosting(context, arrayListPosting, classLogin, masjid, true);
                                recyclerView.setAdapter(adapterPosting);
                                recyclerView.invalidate();
                            }
                        });
                        //endregion

                        if (blokir) {
                            alert(context, "anda terblokir");
                        }

                        final ImageButton btnTambah = findViewById(R.id.btnTambah);
                        if (!add) {
                            btnTambah.setVisibility(View.GONE);
                        } else {
                            btnTambah.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (brainMasjid.tambahMasjid(idLogin, idMasjid, brainMasjid.loadKegiatanTerakhirMasjid(idMasjid), brainMasjid.loadKegiatanRutinTerakhirMasjid(idMasjid))) {
                                        add = false;
                                        btnTambah.setVisibility(View.GONE);
                                        alert(context, "masjid berhasil di tambah");
                                    } else
                                        alert(context, "masjid gagal ditambahkan");
                                }
                            });
                        }
                    }
                });
            }
        });
        //endregion
    }

    @Override
    protected void onPause() {
        super.onPause();

        RecyclerView recyclerView = findViewById(R.id.rc2);
        lastFirstVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!oncreate) {

            //region progresbar
            //MARK : UI
            final ProgressBar prb = findViewById(R.id.prb);

            //MARK : operation
            prb.setVisibility(View.VISIBLE);
            //endregion

            //region async
            //MARK : operation
            new AsyncJob.AsyncJobBuilder<Boolean>();
            AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
                @Override
                public void doOnBackground() {

                    //region empty arraylist
                    //MARK : operation
                    arrayListPosting.clear();
                    //endregion

                    //region refreshclassmasjid
                    if (masjid) {
                        //MARK : obj
                        final brainMasjid brainMasjid = new brainMasjid();

                        classMasjid = brainMasjid.loadProfilMasjid(idMasjid);

                    }
                    //endregion

                    //region refresh topik
                    brainMasjid brainMasjid = new brainMasjid();
                    topik = brainMasjid.loadTopikMasjid(idMasjid, false);
                    //endregion

                    refreshData();

                    //region refresh Unread
                    if (!add) {
                        if (!masjid) {
                            //MARK : obj
                            final brainProfil brainProfil = new brainProfil();
                            List<String> strings = brainProfil.loadUnreadKegiatan(classLogin.getId(), idMasjid);

                            //MARK : obj
                            final brainKegiatan brainKegiatan = new brainKegiatan();
                            unreadKegiatan = brainKegiatan.loadUnreadKegiatan(idMasjid, strings.get(0));

                            //MARK : obj
                            final brainJadwal brainJadwal = new brainJadwal();
                            unreadKegiatanRutin = brainJadwal.loadUnreadKegiatanRutin(idMasjid, strings.get(1));

                            //MARK : obj
                            final Context context = activityDetailMasjid.this;
                            adapterPosting = new adapterPosting(context, arrayListPosting, classLogin, masjid, true);
                        }
                    }
                    //endregion

                    //region UI THREAD
                    //MASK : operation
                    AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                        @Override
                        public void doInUIThread() {

                            //region refresh konten
                            //MARK : UI
                            final RecyclerView recyclerView = findViewById(R.id.rc2);

                            //MARK : operation
                            recyclerView.setAdapter(adapterPosting);
                            recyclerView.invalidate();
                            if (lastFirstVisiblePosition != -1)
                                ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(lastFirstVisiblePosition, 0);
                            lastFirstVisiblePosition = 0;
                            prb.setVisibility(View.INVISIBLE);
                            //endregion

                            //region refresh spinner
                            MaterialSpinner spnTopik = findViewById(R.id.spnTopik);
                            spnTopik.setItems(topik);
                            //endregion

                            //region refresh unread
                            if (!masjid) {
                                final NavigationView nv = findViewById(R.id.nav_view);
                                final Menu menu = nv.getMenu();

                                //region set unread kegiatan
                                //MARK : obj
                                final MenuItem itemkegiatan = menu.findItem(R.id.itemkegiatan);

                                //MARK : operation
                                itemkegiatan.setTitle("Kegiatan Insidentil - " + unreadKegiatan);
                                //endregion

                                //region set unread jadwal
                                //MARK : obj
                                final MenuItem jadwal = menu.findItem(R.id.jadwal);

                                //MARK : operation
                                jadwal.setTitle("Kegiatan Reguler - " + unreadKegiatanRutin);
                                //endregion
                            }
                            //endregion

                        }
                    });
                    //endregion
                }
            });
            //endregion

        } else {
            oncreate = false;
        }

    }

    @Override
    public void onBackPressed() {

        //MARK : UI
        final DrawerLayout drawer = findViewById(R.id.drawer);

        //MARK : operation
        if (drawer.isDrawerOpen(GravityCompat.START)) {

            //MARK : operation
            drawer.closeDrawers();
        } else {
            if (masjid) {

                //MARK : trigger
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                end();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                //MARK : obj
                final AlertDialog.Builder builder = new AlertDialog.Builder(activityDetailMasjid.this);

                //MARK : operation
                builder.setMessage("keluar dari aplikasi ?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
            } else {
                super.onBackPressed();
            }
        }
    }

    public void end() {
        this.finishAffinity();
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }

    //region FUNCTION
    public void logOut(Context context) {

        //MARK : obj
        brainProfil brainProfil = new brainProfil();
        final Intent intent = new Intent(context, activityLogin.class);
        new Prefs.Builder().setContext(activityDetailMasjid.this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();

        //MARK : operation
        if (brainProfil.logout(classLogin.getId())) {
            Prefs.putString("akun", "");
            startActivity(intent);
        }
    }

    public void loadJadwal(Context context, Boolean masjid, String idp, String idm) {

        //MARK : obj
        final Intent intent = new Intent(context, activityJadwal.class);

        //MARK : operation
        intent.putExtra("classlogin", classLogin);
        intent.putExtra("idmasjid", idm);
        intent.putExtra("idp", idp);
        intent.putExtra("masjid", masjid);
        startActivity(intent);

    }

    public void loadKegiatanMasjid(Context context, Boolean masjid, String idm, String idp) {

        //MARK : obj
        final Intent intent = new Intent(context, activityKegiatanMasjid.class);

        //MARK : operation
        intent.putExtra("masjid", masjid);
        intent.putExtra("idm", idm);
        intent.putExtra("idp", idp);
        startActivity(intent);
    }

    private void loadFollowMasjid(Context context) {
        //MARK : obj
        final Intent intent = new Intent(context, activityPengikutMasjid.class);

        //MARK : operation
        intent.putExtra("idm", classMasjid.getId());
        startActivity(intent);
    }

    public void loadListChat(Context context, String idm, String idp) {
        //MARK : obj
        final Intent intent = new Intent(context, activityListChat.class);

        //MARK : operation
        intent.putExtra("idm", idm);
        intent.putExtra("idpm", idp);
        intent.putExtra("masjid", true);
        startActivity(intent);
    }

    public void refreshData() {
        //MARK : obj
        final brainBerita brainBerita = new brainBerita();

        //MARK : operation
        arrayListPosting.clear();
        brainBerita.loadPostingMasjid(idMasjid, arrayListPosting);
    }

    public void chatButton(Context context, Boolean masjid, String idLogin, String idm) {
        if (masjid)
            loadListChat(context, idm, idLogin);
        else {

            //MARK : obj
            final Intent intent = new Intent(context, activityChat.class);

            //MARK operation
            intent.putExtra("masjid", masjid);
            intent.putExtra("idp", idLogin);
            intent.putExtra("idm", idm);
            intent.putExtra("subjek", classMasjid.getNama());
            startActivity(intent);
        }
    }

    private void alert(Context context, String msg) {

        //MARK:obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log("dismiss");
                if (blokir)
                    back();
            }
        });

        //MARK : operation
        builder.setMessage(msg).show();


    }

    private void back() {
        this.finish();
    }
    //endregion
}
