package com.example.audi.mymasjid;


import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class masjid extends android.support.v4.app.Fragment {

    public masjid() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Inflate the layout for this fragment
        final View view2 = inflater.inflate(
                R.layout.fragment_masjid,
                container, false
        );

        final FloatingActionButton masjid = (FloatingActionButton) view2.findViewById(R.id.fabMasjid);
        //FloatingActionButton cancel = (FloatingActionButton) view2.findViewById(R.id.floatingActionButtonCancel);
        FrameLayout frmbg = (FrameLayout) view2.findViewById(R.id.frmbg);

        Integer[] myIntArray = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

        RecyclerView recyclerView = (RecyclerView) view2.findViewById(R.id.rclmasjid);
        MasjidAdapter masjidAdapter = new MasjidAdapter(view2.getContext(), myIntArray);
        LinearLayoutManager llm = new LinearLayoutManager(view2.getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(masjidAdapter);

        /*masjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchfab(view2);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchfab2(view2);
            }
        });*/

        return view2;
    }

    /*public void switchfab(View view) {
        FloatingActionButton cancel = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonCancel);
        FloatingActionButton tambah = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonTambahMasjid);
        FloatingActionButton hapus = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonHapusMasjid);
        FloatingActionButton masjid = (FloatingActionButton) view.findViewById(R.id.fabMasjid);
        LinearLayout llblock = (LinearLayout) view.findViewById(R.id.llblock);
        TextView txtcancel = (TextView) view.findViewById(R.id.txtcancel);
        TextView txthapus = (TextView) view.findViewById(R.id.txthapus);
        TextView txttambah = (TextView) view.findViewById(R.id.txttambah);

        llblock.setVisibility(View.VISIBLE);
        masjid.animate().alpha(0.0f);
        masjid.setVisibility(View.INVISIBLE);
        cancel.setVisibility(View.VISIBLE);
        cancel.animate().alpha(1.0f);
        tambah.setVisibility(View.VISIBLE);
        tambah.animate().alpha(1.0f);
        hapus.setVisibility(View.VISIBLE);
        hapus.animate().alpha(1.0f);
        txtcancel.setVisibility(View.VISIBLE);
        txthapus.setVisibility(View.VISIBLE);
        txttambah.setVisibility(View.VISIBLE);
    }

    public void switchfab2(View view) {
        FloatingActionButton cancel = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonCancel);
        FloatingActionButton tambah = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonTambahMasjid);
        FloatingActionButton hapus = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonHapusMasjid);
        FloatingActionButton masjid = (FloatingActionButton) view.findViewById(R.id.fabMasjid);
        LinearLayout llblock = (LinearLayout) view.findViewById(R.id.llblock);
        TextView txtcancel = (TextView) view.findViewById(R.id.txtcancel);
        TextView txthapus = (TextView) view.findViewById(R.id.txthapus);
        TextView txttambah = (TextView) view.findViewById(R.id.txttambah);

        llblock.setVisibility(View.INVISIBLE);
        cancel.animate().alpha(0.0f);
        cancel.setVisibility(View.INVISIBLE);
        tambah.animate().alpha(0.0f);
        tambah.setVisibility(View.INVISIBLE);
        hapus.animate().alpha(0.0f);
        hapus.setVisibility(View.INVISIBLE);
        masjid.setVisibility(View.VISIBLE);
        masjid.animate().alpha(1.0f);
        txtcancel.setVisibility(View.INVISIBLE);
        txthapus.setVisibility(View.INVISIBLE);
        txttambah.setVisibility(View.INVISIBLE);
    }*/

}
