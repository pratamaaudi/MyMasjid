package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;
import com.bumptech.glide.Glide;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class activityTambahPosting extends AppCompatActivity {

    EditText edtIsi;
    ImageView img;
    Boolean edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_posting);

        //MARK : UI
        edtIsi = findViewById(R.id.edtIsi);
        img = findViewById(R.id.img);
        final EditText edtTopik = findViewById(R.id.edttopik);

        //MARK : obj
        final Bundle bundle = getIntent().getExtras();

        //region frame layout
        //MARK : UI
        final FrameLayout frm = findViewById(R.id.frm);

        //MARK : trigger
        frm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Intent intent = new Intent();

                //MARK : operation
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "select image from gallery"), 1);
            }
        });
        //endregion

        //region geteditmode
        //MARK : obj
        edit = false;

        //operation
        edit = bundle.getBoolean("edit");
        //endregion

        //region spnTopik
        //MARK : UI
        final MaterialSpinner spnTopik = findViewById(R.id.spnTopik);

        //MARK : obj
        final List<String> spntopik;

        //MARK : operation
        if (edit) {

            //MARK : obj
            final brainMasjid brainMasjid = new brainMasjid();
            spntopik = brainMasjid.loadTopikMasjid(bundle.getString("idm"), true);

            spnTopik.setItems(spntopik);
        } else {

            //MARK : obj
            spntopik = new ArrayList<>();

            spntopik.add("NONE");
            spntopik.add("Sholat Fardu");
            spntopik.add("Sholat Jumat");
            spntopik.add("Hari Besar Islam");
            spntopik.add("Tabliq Akbar");
            spntopik.add("Pengajian Rutin");
            spntopik.add("Kegiatan Sosial Ekonomi");
            spntopik.add("Kegiatan Pendidikan");
            spntopik.add("Pemberdayaan zakat, infaq, Shodaqoh dan Wakaf");
            spntopik.add("Pengumpulan Zakat Fitrah");
            spntopik.add("Penyaluran Querban");
            spntopik.add("Khitanan Massal");
            spntopik.add("Upacara Perkawinan");
            spntopik.add("Sembahyang Jenazah");
            spntopik.add("MTQ");
            spntopik.add("Lainnya");
            spnTopik.setItems(spntopik);

            //MARK : trigger
            spnTopik.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    if (position == 15) {
                        edtTopik.setVisibility(View.VISIBLE);
                    } else {
                        edtTopik.setText("");
                        edtTopik.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }

        //endregion

        //region edit mode
        //MARK : operation
        if (edit) {
            Glide.with(activityTambahPosting.this).load(bundle.getString("url")).thumbnail(1.0f).into(img);
            frm.setClickable(false);
            edtIsi.setText(getIntent().getExtras().getString("status"));
        }
        //endregion

        //region btnSubmit
        //MARK : UI
        final ImageButton ibtnSumbit = findViewById(R.id.ibtnsubmit);

        //MARK : trigger
        ibtnSumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Context context = activityTambahPosting.this;
                final Integer selected = spnTopik.getSelectedIndex();
                final String spinnerSelected = spntopik.get(selected).toLowerCase();


                if (edit) {

                    //MARK : obj
                    final String idStatus = bundle.getString("ids");

                    updatePosting(context, spinnerSelected, idStatus);

                } else {
                    if (selected == 0)
                        alert("tolong pilih topik terlebih dahulu", context);
                    else {
                        if (edtIsi.getText().toString().equals(""))
                            alert("mohon mengisi status", context);
                        else {
                            if (selected == 15) {

                                //MARK : obj
                                final String topik = edtTopik.getText().toString();

                                if (topik.equals(""))
                                    alert("mohon mengisi kolom topik", context);
                                else
                                    submitPosting(getIntent().getExtras().getString("mid"), context, topik.toLowerCase());

                            } else
                                submitPosting(getIntent().getExtras().getString("mid"), context, spinnerSelected);
                        }
                    }
                }
            }
        });

        //endregion

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {

                //region set gambar
                //MARK : obj
                final Uri uri = data.getData();
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                //MARK : operation
                img.setImageBitmap(bitmap);
                //endregion

                //region settxttekan
                //MARK : obj
                final TextView txtTekan = findViewById(R.id.txtTekan);

                //MARK : operation
                txtTekan.setVisibility(View.INVISIBLE);
                //endregion

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void submitPosting(final String mid, final Context context, final String tpk) {
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading . . .");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        AsyncJob.OnBackgroundJob submit = new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //MARK : obj
                final brainBerita brainBerita = new brainBerita();
                final String isi = edtIsi.getText().toString();
                final Boolean hasil;

                //MARK : operation
                if (img.getDrawable() == null) {
                    hasil = brainBerita.inputStatus(isi, mid, tpk);
                } else {

                    //MARK : obj
                    final BitmapDrawable bitmapDrawable = (BitmapDrawable) img.getDrawable();
                    final Bitmap bitmap = bitmapDrawable.getBitmap();

                    //MARK : operation
                    hasil = brainBerita.inputStatusdanGambar(isi, mid, imageToString(bitmap), tpk);
                }

                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        if (hasil) {
                            edtIsi.setText("");
                            img.setImageResource(0);
                            alert("konten berhasil di post", context);
                            finish();
                        } else {
                            alert("konten gagal di post, coba lagi nanti", context);
                        }
                        pDialog.dismiss();
                    }
                });

            }
        };

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        AsyncJob.doInBackground(submit, executorService);
    }

    public void updatePosting(final Context context, final String tpk, final String idStatus) {
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading . . .");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        AsyncJob.OnBackgroundJob submit = new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //MARK : obj
                final brainBerita brainBerita = new brainBerita();
                final String isi = edtIsi.getText().toString();
                final Boolean hasil;

                //MARK : operation
                hasil = brainBerita.updateStatus(isi, tpk, idStatus);

                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        if (hasil) {
                            edtIsi.setText("");
                            img.setImageResource(0);
                            alert("konten berhasil di update", context);
                            finish();
                        } else
                            alert("konten gagal di update, coba lagi nanti", context);

                        pDialog.dismiss();
                    }
                });

            }
        };

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        AsyncJob.doInBackground(submit, executorService);
    }

    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void alert(String msg, Context context) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        //MARK : operation
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
