package com.example.audi.mymasjid;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;

import java.util.ArrayList;

public class activityJadwal_old extends AppCompatActivity {

    public TabLayout tab;
    ArrayList<classJadwal_old> arraylistClassJadwalOld;
    LinearLayoutManager llm;
    TextView txtJudulJadwal;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal_old);

        initvalue();

//        recyclerAdapterJadwal = new recyclerAdapterJadwal(getApplicationContext(), arraylistClassJadwalOld);

        setlistener();

        setfragment("januari");
    }

    public void initvalue() {

        //MARK : Object
        arraylistClassJadwalOld = new ArrayList<>();
        bundle = new Bundle();

        //MARK : UI
        tab = findViewById(R.id.tabs);
        txtJudulJadwal = findViewById(R.id.txtjuduljadwal);
    }

    public void setfragment(String konten) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(
                R.anim.fade_in, R.anim.fade_out
        );
        jadwal_januari jadwal_januari = new jadwal_januari();
        switch (konten) {
            case "januari":
                bundle.putString("bulan", "januari");
                break;
            default:
                bundle.putString("bulan", "februari");
                break;
        }
        jadwal_januari.setArguments(bundle);
        transaction.replace(R.id.llkonten, jadwal_januari).commit();
    }

    public void setlistener() {
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:

                        setfragment("januari");

                        break;
                    case 1:

                        setfragment("februari");

                        break;
                    case 2:

                        setfragment("maret");

                        break;
                    case 3:

                        setfragment("april");

                        break;
                    case 4:

                        setfragment("mei");

                        break;
                    case 5:

                        setfragment("juni");

                        break;
                    case 6:

                        setfragment("juli");

                        break;
                    case 7:

                        setfragment("agustus");

                        break;
                    case 8:

                        setfragment("september");

                        break;
                    case 9:

                        setfragment("oktober");

                        break;
                    case 10:

                        setfragment("november");

                        break;
                    case 11:

                        setfragment("desember");

                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
