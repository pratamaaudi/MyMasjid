package com.example.audi.mymasjid;

/**
 * Created by prata on 10/22/2017.
 */

public class classKegiatan {
    private String idKegiatan;
    private String namaKegiatan;
    private String tanggalMulai;
    private String jamMulai;
    private String tanggalSelesai;
    private String jamSelesai;
    private String idMasjid;
    private String namaMasjid;
    private String deskripsi;

    public classKegiatan(
            String idKegiatan,
            String namaKegiatan,
            String tanggalMulai,
            String jamMulai,
            String tanggalSelesai,
            String jamSelesai,
            String idMasjid,
            String namaMasjid,
            String deskripsi
    ) {
        setIdKegiatan(idKegiatan);
        setIdMasjid(idMasjid);
        setNamaKegiatan(namaKegiatan);
        setTanggalMulai(tanggalMulai);
        setJamMulai(jamMulai);
        setTanggalSelesai(tanggalSelesai);
        setJamSelesai(jamSelesai);
        setNamaMasjid(namaMasjid);
        setDeskripsi(deskripsi);
    }

    public String getIdKegiatan() {
        return idKegiatan;
    }

    public void setIdKegiatan(String idKegiatan) {
        this.idKegiatan = idKegiatan;
    }

    public String getNamaKegiatan() {
        return namaKegiatan;
    }

    public void setNamaKegiatan(String namaKegiatan) {
        this.namaKegiatan = namaKegiatan;
    }

    public String getTanggalMulai() {
        return tanggalMulai;
    }

    public void setTanggalMulai(String tanggalMulai) {
        this.tanggalMulai = tanggalMulai;
    }

    public String getTanggalSelesai() {
        return tanggalSelesai;
    }

    public void setTanggalSelesai(String tanggalSelesai) {
        this.tanggalSelesai = tanggalSelesai;
    }

    public String getIdMasjid() {
        return idMasjid;
    }

    public void setIdMasjid(String idMasjid) {
        this.idMasjid = idMasjid;
    }

    public String getJamMulai() {
        return jamMulai;
    }

    public void setJamMulai(String jamMulai) {
        this.jamMulai = jamMulai;
    }

    public String getJamSelesai() {
        return jamSelesai;
    }

    public void setJamSelesai(String jamSelesai) {
        this.jamSelesai = jamSelesai;
    }

    public String getNamaMasjid() {
        return namaMasjid;
    }

    public void setNamaMasjid(String namaMasjid) {
        this.namaMasjid = namaMasjid;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
