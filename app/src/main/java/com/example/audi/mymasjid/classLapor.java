package com.example.audi.mymasjid;

public class classLapor {
    private String idLapor;
    private String idMasjid;
    private String idProfil;
    private String keterangan;
    private String status;
    private String pelapor;
    private String hapus_komentar;
    private classKomentar classKomentar;

    public classLapor(String idLapor, String idMasjid, String idProfil, String keterangan, String status, String pelapor, String hapus_komentar, classKomentar classKomentar){
        setIdLapor(idLapor);
        setIdMasjid(idMasjid);
        setIdProfil(idProfil);
        setKeterangan(keterangan);
        setStatus(status);
        setPelapor(pelapor);
        setHapus_komentar(hapus_komentar);
        setClassKomentar(classKomentar);
    }

    //region getset
    public String getIdMasjid() {
        return idMasjid;
    }

    public void setIdMasjid(String idMasjid) {
        this.idMasjid = idMasjid;
    }

    public String getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(String idProfil) {
        this.idProfil = idProfil;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public com.example.audi.mymasjid.classKomentar getClassKomentar() {
        return classKomentar;
    }

    public void setClassKomentar(com.example.audi.mymasjid.classKomentar classKomentar) {
        this.classKomentar = classKomentar;
    }

    public String getPelapor() {
        return pelapor;
    }

    public void setPelapor(String pelapor) {
        this.pelapor = pelapor;
    }

    public String getHapus_komentar() {
        return hapus_komentar;
    }

    public void setHapus_komentar(String hapus_komentar) {
        this.hapus_komentar = hapus_komentar;
    }

    public String getIdLapor() {
        return idLapor;
    }

    public void setIdLapor(String idLapor) {
        this.idLapor = idLapor;
    }
    //endregion
}
