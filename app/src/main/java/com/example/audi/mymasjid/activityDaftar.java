package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

public class activityDaftar extends AppCompatActivity {

    //MARK : UI
    TextView txtError;

    //MARK : obj
    brainDaftar brain;
    ArrayList<classProvinsi> classProvinsis;
    ArrayList<classKotakab> classKotakabs;
    ArrayList<classKecamatan> classKecamatans;
    String idd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        //MARK : glb UI
        final Button btnDaftar = findViewById(R.id.btnDaftar);

        //MARK : gbl obj
        brain = new brainDaftar();

        //region radioButton
        //MARK : UI
        final RadioButton rdoAkunMasjid, rdoAkunUser;
        rdoAkunUser = findViewById(R.id.rdoakunuser);
        rdoAkunMasjid = findViewById(R.id.rdoakunmasjid);

        //MARK : trigger
        rdoAkunUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                btnDaftar.setText("Daftar");
            }
        });


        //MARK : trigger
        rdoAkunMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                btnDaftar.setText("Lanjut");
            }
        });
        //endregion

        //region btnDaftar
        //MARK : UI
        txtError = findViewById(R.id.txtError);

        //MARK : trigger
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //region daftarAkun
                //MARK : UI
                final EditText edtUsername, edtPass, edtEmail, edtrepass;
                edtrepass = findViewById(R.id.edtRepass);
                edtUsername = findViewById(R.id.edtUsername);
                edtPass = findViewById(R.id.edtPass);
                edtEmail = findViewById(R.id.edtEmail);

                //MARK : operation
                if (edtUsername.getText().toString().equals("") || edtPass.getText().toString().equals("") || edtrepass.getText().toString().equals("") || edtEmail.getText().toString().equals("")) {
                    //ada yang kosong

                    //region error ga semua diisi
                    //MARK : operation
                    txtError.setText("mohon mengisi semua kolom data");
                    txtError.setVisibility(View.VISIBLE);
                    //endregion
                } else {
                    if (edtPass.getText().toString().equals(edtrepass.getText().toString())) {
                        if (rdoAkunUser.isChecked()) {
                            //user

                            //region jalankan fungsi daftar
                            //MARK : function
                            new daftar().execute(edtEmail.getText().toString(), edtUsername.getText().toString(), edtPass.getText().toString(), idd);
                            //endregion

                        } else {

                            //region pindah ke halaman daftar masjid
                            //MARK : OBJ
                            final Intent i = new Intent(getApplicationContext(), activityDaftarMasjid.class);
                            i.putExtra("username", edtUsername.getText().toString());
                            i.putExtra("password", edtPass.getText().toString());
                            i.putExtra("email", edtEmail.getText().toString());
                            i.putExtra("idd", idd);

                            //MARK : operation
                            startActivity(i);
                            //endregion
                        }
                    } else {

                        //region error pass dan repass tidak sama
                        //MARK : operation
                        txtError.setText("password dan konfirmasi password tidak sama");
                        txtError.setVisibility(View.VISIBLE);
                        //endregion
                    }
                }
                //endregion
            }
        });
        //endregion

        //region set spinner
        //MARK : UI
        final MaterialSpinner spnprovinsi = findViewById(R.id.spnHari);
        final MaterialSpinner spnKotaKab = findViewById(R.id.spnkotakab);
        final MaterialSpinner spnKecamatan = findViewById(R.id.spnkecamatan);

        //MARK : obj
        classProvinsis = new ArrayList<>();
        List<String> provinsiSpinner = new ArrayList<>();

        //MARK : operation
        classProvinsis = brain.loadProvinsi(classProvinsis);
        for (classProvinsi classProvinsi : classProvinsis) {
            provinsiSpinner.add(classProvinsi.getName());
        }
        spnprovinsi.setItems(provinsiSpinner);

        //MARK : trigger
        spnprovinsi.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                //MARK : obj
                classKotakabs = new ArrayList<>();
                List<String> kotakabSpinner = new ArrayList<>();

                //MARK : operation
                classKotakabs = brain.loadKotakab(classKotakabs, classProvinsis.get(position).getId());
                for (classKotakab classKotakab : classKotakabs) {
                    kotakabSpinner.add(classKotakab.getName());
                }
                spnKotaKab.setItems(kotakabSpinner);
                spnKotaKab.setEnabled(true);
            }
        });
        spnKotaKab.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                //MARK : obj
                classKecamatans = new ArrayList<>();
                List<String> kecamatanSpinner = new ArrayList<>();

                //MARK : operation
                classKecamatans = brain.loadKecamatan(classKecamatans,classKotakabs.get(position).getId());
                for(classKecamatan classKecamatan : classKecamatans){
                    kecamatanSpinner.add(classKecamatan.getName());
                }
                spnKecamatan.setItems(kecamatanSpinner);
                spnKecamatan.setEnabled(true);
            }
        });
        spnKecamatan.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                idd = classKecamatans.get(position).getId();
            }
        });
        //endregion
    }

    private class daftar extends AsyncTask<String, String, String> {

        //MARK : Obj
        private ProgressDialog pDialog;
        private Boolean operation1, operation2;

        //MARK : Operation
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //region show loading
            //MARK : obj
            pDialog = new ProgressDialog(activityDaftar.this);

            //MARK : operation
            pDialog.setMessage("Loading . . .");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            //endregion
        }

        @Override
        protected String doInBackground(String... data) {

            //MARK : OBJ
            operation1 = false;
            operation2 = false;

            //MARK : operation
            if (brain.cekEmailUser(data[0])) {

                //MARK : operation
                operation1 = true;
            } else {
                if (brain.daftarUser(data[1], data[0], data[2], data[3])) {

                    //MARK : operation
                    operation2 = true;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //region cek hasil
            //MARK : operation
            pDialog.dismiss();
            if (operation1) {

                //region error email sudah terdaftar
                //MARK : operation
                txtError.setText("email sudah terdaftar");
                txtError.setVisibility(View.VISIBLE);
                //endregion
            } else {
                if (operation2) {

                    //region data berhasil dimasukkan
                    //MARK : operation
                    txtError.setText("data berhasil dimasukkan");
                    txtError.setVisibility(View.VISIBLE);
                    activityDaftar.this.finish();
                    //endregion
                } else {

                    //region data gagal dimasukkan
                    //MARK : operation
                    txtError.setText("data gagal dimasukkan");
                    txtError.setVisibility(View.VISIBLE);
                    //endregion
                }
            }
            //endregion
        }
    }
}
