package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class adapterListChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final ArrayList<classRoom> classRooms;
    final brainChat brainChat;
    final String idp;
    final Context context;
    final Boolean masjid;

    public adapterListChat(ArrayList<classRoom> classRooms, String idp, Context context, Boolean masjid) {
        this.classRooms = classRooms;
        brainChat = new brainChat();
        this.idp = idp;
        this.context = context;
        this.masjid = masjid;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //MARK : obj
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_chat, parent, false);
        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : global
        final classRoom classRoom = classRooms.get(position);
        final String idr = classRoom.getId();
        final ArrayList<String> strings;
        final View view = holder.itemView;
        Boolean unread = false;

        final TextView txtUsername, txtChat, txtNotif;

        //MARK : operation
        if (masjid)
            strings = brainChat.loadDetailRoomMasjid(idr);
        else {
            strings = brainChat.loadDetailRoomUser(idr);
        }

        //region txtUsername
        //MARK : UI
        txtUsername = view.findViewById(R.id.txtUsername);

        //MARK : operation
        txtUsername.setText(strings.get(2));
        //endregion

        //region txtChat
        //MARK : UI
        txtChat = view.findViewById(R.id.txtChat);

        //MARK : operation
        txtChat.setText(strings.get(1));
        //endregion

        //region txtNotif

        //MARK : obj
        final String counter = classRoom.getCounter();

        //MARK : operation
        if (!counter.equals("0")) {
            if (!idp.equals(strings.get(0))) {

                //MARK : UI
                txtNotif = view.findViewById(R.id.txtNotif);

                //MARK : operation
                txtNotif.setText(classRoom.getCounter());
                txtNotif.setVisibility(View.VISIBLE);
                unread = true;

            }
        }
        //endregion

        //region Constraint Layout
        //MARK : UI
        ConstraintLayout cnsChat = view.findViewById(R.id.cnsChat);

        //MARK : obj
        final Boolean unreadroom = unread;

        //MARK : trigger
        cnsChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : operation
                if (unreadroom)
                    brainChat.readRoom(idr);

                //MARK : obj
                final Intent intent = new Intent(context, activityChat.class);

                if (masjid) {
                    intent.putExtra("masjid",masjid);
                } else {
                    intent.putExtra("masjid", false);
                    intent.putExtra("idm", strings.get(3));
                }
                intent.putExtra("subjek", strings.get(2));
                intent.putExtra("idp", idp);
                intent.putExtra("idr", idr);
                intent.putExtra("token", strings.get(4));

                //MARK : operation
                context.startActivity(intent);
            }
        });
        //endregion
    }

    @Override
    public int getItemCount() {
        return classRooms.size();
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
}
