package com.example.audi.mymasjid;

import android.Manifest;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cleveroad.slidingtutorial.Direction;
import com.cleveroad.slidingtutorial.PageOptions;
import com.cleveroad.slidingtutorial.TransformItem;
import com.cleveroad.slidingtutorial.TutorialOptions;
import com.cleveroad.slidingtutorial.TutorialPageOptionsProvider;
import com.cleveroad.slidingtutorial.TutorialSupportFragment;
import com.google.firebase.iid.FirebaseInstanceId;
import com.pixplicity.easyprefs.library.Prefs;

public class activityMain extends AppCompatActivity {

    View view2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MARK : UI
        final DrawerLayout drawer = findViewById(R.id.drawer);

        //region imgmenu (hamburger menu)
        //MARK : UI
        final ImageView imgmenu = findViewById(R.id.imgmenu);

        //MARK : trigger
        imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                drawer.openDrawer(GravityCompat.START);
            }
        });
        //endregion

        //MARK : obj
        final Intent intent = getIntent();
        final classLogin classLogin = intent.getParcelableExtra("classLogin");

        //region tabLayout
        //MARK : UI
        final TabLayout tab = findViewById(R.id.tablayout);

        //MARK : trigger
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                //region ubah fragment sesuai pilihan user
                //MARK : operation
                switch (tab.getPosition()) {

                    case 0:
                        setfragment("dakwah", classLogin);
                        break;
                    case 1:
                        setfragment("berita", classLogin);
                        break;
                    case 2:
                        setfragment("masjid", classLogin);
                    case 3:
                        setfragment("rutin", classLogin);
                        break;
                }
                //endregion
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //endregion

        //region navigation view
        //MARK : UI
        final NavigationView nv = findViewById(R.id.nav_view);

        //MARK : trigger
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                //region pindah halaman sesuai pilihan user
                //MARK : Obj
                final Intent i;

                //MARK : operation
                drawer.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.itemprofil:

                        //region pindah ke halaman profil
                        //MARK : obj
                        i = new Intent(getApplicationContext(), activityProfil.class);

                        //MARK : operation
                        i.putExtra("classLogin", classLogin);
                        startActivity(i);
                        //endregion
                        break;
                    case R.id.itemfollow:

                        //region pindah ke tab masjid
                        //MARK : obj
                        final TabLayout.Tab tabitem = tab.getTabAt(2);

                        //MARK : operation
                        tabitem.select();
                        setfragment("masjid", classLogin);
                        //endregion
                        break;
                    case R.id.itemreminder:

                        //region pindah ke halaman kegiatan user
                        //MARK : obj
                        i = new Intent(getApplicationContext(), activityKegiatanUser.class);

                        //MARK : operation
                        i.putExtra("uid", classLogin.getId());
                        startActivity(i);
                        //endregion
                        break;
                    case R.id.itemjadwal:

                        //MARK : obj
                        i = new Intent(getApplicationContext(), activityJadwal.class);

                        //MARK : operation
                        i.putExtra("classlogin", classLogin);
                        i.putExtra("masjid", false);
                        i.putExtra("haluser", true);
                        i.putExtra("idp", classLogin.getId());
                        startActivity(i);
                        break;
                    case R.id.itemlapor:

                        //MARK : obj
                        i = new Intent(getApplicationContext(), activityListLapor.class);

                        //MARK : operation activity lapor
                        i.putExtra("pid", classLogin.getId());
                        startActivity(i);

                        break;
                    case R.id.itemChat:

                        //MARK : obj
                        i = new Intent(getApplicationContext(), activityListChat.class);
                        i.putExtra("idp", classLogin.getId());
                        i.putExtra("masjid", false);

                        //MARK : operation activity lapor
                        startActivity(i);
                        break;
                    case R.id.itemlogout:

                        //region alert exit
                        //MARK : trigger
                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        //region pindah ke halaman login
                                        //MARK ; obj
                                        brainProfil brainProfil = new brainProfil();
                                        final Intent i = new Intent(getApplicationContext(), activityLogin.class);
                                        new Prefs.Builder().setContext(activityMain.this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();

                                        //MARK : operation
                                        if (brainProfil.logout(classLogin.getId())) {
                                            Prefs.putString("akun", "");
                                            startActivity(i);
                                        }
                                        //endregion
                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        //No button clicked
                                        break;
                                }
                            }
                        };

                        //MARK : obj
                        final AlertDialog.Builder builder = new AlertDialog.Builder(activityMain.this);

                        //MARK : operation
                        builder.setMessage("keluar dari akun " + classLogin.getUsername() + "    ?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
                        //endregion
                        break;
                }
                //endregion
                return false;
            }
        });

        //region header
        //MARK : obj
        final View header = nv.getHeaderView(0);

        //MARK : UI
        final ImageView imgHeader = header.findViewById(R.id.imgHeader);

        //MARK : trigger
        imgHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //endregion
        //endregion

        //region btnCari
        //MARK : UI
        final Button btncari = findViewById(R.id.btncari);

        //MARK : trigger
        btncari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //region pindah ke halaman search

                //MARK : UI
                final TextView txtcari = findViewById(R.id.txtcari);

                //MARK : obj
                final Intent i = new Intent(view.getContext(), Search.class);
                i.putExtra("classLogin", classLogin);
                final Pair<View, String> p1 = Pair.create((View) txtcari, ViewCompat.getTransitionName(txtcari));
                final Pair<View, String> p2 = Pair.create((View) imgmenu, ViewCompat.getTransitionName(imgmenu));
                final ActivityOptionsCompat option = ActivityOptionsCompat.makeSceneTransitionAnimation(activityMain.this, p1, p2);

                //MARK : operation
                startActivity(i, option.toBundle());
                activityMain.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //endregion
            }
        });
        //endregion

        //MARK : function
        setfragment("dakwah", classLogin);

        //region cek autologin / ga
        if (!intent.getBooleanExtra("autologin", false)) {

            //region update token
            //MARK : obj
            final brainLogin brainLogin = new brainLogin();

            //MARK : operation
//            brainLogin.updateToken(FirebaseInstanceId.getInstance().getToken(), classLogin.getId());
            //endregion

            //region show tutorial
            //MARK : operation
            final TutorialPageOptionsProvider tutorialPageOptionsProvider = new TutorialPageOptionsProvider() {
                @NonNull
                @Override
                public PageOptions provide(int position) {
                    @LayoutRes int pageLayoutResId;
                    TransformItem[] tutorialItems;
                    switch (position) {
                        case 0: {
                            pageLayoutResId = R.layout.fragment_fragment_tutorial1;
                            tutorialItems = new TransformItem[]{
                                    TransformItem.create(R.id.txt1, Direction.LEFT_TO_RIGHT, 0.7f)
                            };
                            break;
                        }
                        case 1: {
                            pageLayoutResId = R.layout.fragment_tutorial_2;
                            tutorialItems = new TransformItem[]{
                                    TransformItem.create(R.id.imgChat1, Direction.RIGHT_TO_LEFT, 0.5f),
                                    TransformItem.create(R.id.imgChat2, Direction.RIGHT_TO_LEFT, 0.5f)
                            };
                            break;
                        }
                        case 2: {
                            pageLayoutResId = R.layout.fragment_tutorial_3;
                            tutorialItems = new TransformItem[]{
                                    TransformItem.create(R.id.imgChat1, Direction.RIGHT_TO_LEFT, 0.5f),
                                    TransformItem.create(R.id.imgChat2, Direction.RIGHT_TO_LEFT, 0.5f)
                            };
                            break;
                        }
                        case 3: {
                            pageLayoutResId = R.layout.fragment_tutorial_4;
                            tutorialItems = new TransformItem[]{
                                    TransformItem.create(R.id.imgChat1, Direction.RIGHT_TO_LEFT, 0.5f),
                                    TransformItem.create(R.id.imgChat2, Direction.RIGHT_TO_LEFT, 0.5f)
                            };
                            break;
                        }
                        default: {
                            throw new IllegalArgumentException("Unknown position: " + position);
                        }
                    }

                    return PageOptions.create(pageLayoutResId, position, tutorialItems);
                }
            };

            int[] color = new int[]{
                    Color.parseColor("#E1F5FE"),
                    Color.parseColor("#80CBC4"),
                    Color.parseColor("#AED581"),
                    Color.parseColor("#4CAF50")
            };

            final TutorialSupportFragment tutorialFragment;

            final TutorialOptions tutorialOptions = TutorialSupportFragment
                    .newTutorialOptionsBuilder(activityMain.this)
                    .setPagesColors(color)
                    .setPagesCount(4)
                    .setOnSkipClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //MARK : obj
                            final Intent intent = new Intent(activityMain.this, activityMain.class);

                            intent.putExtra("classLogin", classLogin);
                            intent.putExtra("autologin", true);
                            startActivity(intent);
                        }
                    })
                    .setTutorialPageProvider(tutorialPageOptionsProvider)
                    .setUseAutoRemoveTutorialFragment(true)
                    .build();

            tutorialFragment = TutorialSupportFragment.newInstance(tutorialOptions);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, tutorialFragment)
                    .commit();
            //endregion
        }
        //endregion

        //region cek permission telpon
        //MARK : operation
//        if (ActivityCompat.checkSelfPermission(this,
//                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(activityMain.this,
//                    new String[]{Manifest.permission.CALL_PHONE}, 001);
//            return;
//        }
        //endregion
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 001: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                return;
            }
        }
    }

    public void setfragment(String konten, classLogin classLogin) {

        //region set animasi fade
        //MARK : obj
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //MARK : operation
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        //endregion

        //region pindah fragment
        //MARK : obj
        final Bundle bundle = new Bundle();

        //MARK : operation
        switch (konten) {
            case "berita":

                //region ubah ke fragment berita
                //MARK : obj
                final fragmentBerita fragmentBerita = new fragmentBerita();

                bundle.putParcelable("classLogin", classLogin);
                bundle.putBoolean("masjid", false);
                fragmentBerita.setArguments(bundle);
                transaction.replace(R.id.llkonten, fragmentBerita).commit();
                //endregion
                break;
            case "masjid":

                //region pindah ke fragment masjid
                //MARK : obj
                final fragmentMasjid fragmentMasjid = new fragmentMasjid();

                //MARK : operation
                bundle.putParcelable("classLogin", classLogin);
                fragmentMasjid.setArguments(bundle);
                transaction.replace(R.id.llkonten, fragmentMasjid).commit();
                //endregion
                break;
            case "dakwah":

                //region pindah ke fragment kegiatan
                //MARK : obj
                fragmentKegiatan fragmentKegiatan = new fragmentKegiatan();

                //MARK : operation
                bundle.putString("uid", classLogin.getId());
                bundle.putBoolean("masjid", false);
                fragmentKegiatan.setArguments(bundle);
                transaction.replace(R.id.llkonten, fragmentKegiatan).commit();
                //endregion
                break;
            case "rutin":
                break;
        }
        //endregion
    }

    @Override
    public void onBackPressed() {
        //region alert exit
        //MARK : trigger
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        end();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityMain.this);

        //MARK : operation
        builder.setMessage("keluar dari aplikasi ?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
        //endregion
    }

    public void end() {
        this.finishAffinity();
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
}
