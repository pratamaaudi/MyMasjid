package com.example.audi.mymasjid;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class activityLaporMasjid extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapor_masjid);

        //MARK : glb obj
        final Bundle bundle = getIntent().getExtras();

        //region txtPelapor
        //MARK : UI
        final TextView txtPelapor = findViewById(R.id.txtPelapor);

        //MARK : operation
        txtPelapor.setText(bundle.getString("pelapor"));
        //endregion

        //region txtMasjid
        //MARK : UI
        final TextView txtMasjid = findViewById(R.id.txtMasjid);

        //MARK : operation
        txtMasjid.setText(bundle.getString("masjid"));
        //endregion

        //region btnSubmit
        //MARK : UI
        final ImageButton ibtnSubmit = findViewById(R.id.ibtnSubmit);

        //MARK : trigger
        ibtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : UI
                final EditText edtKeterangan = findViewById(R.id.edtKeterangan);

                //MARK : obj
                final String ket = edtKeterangan.getText().toString();

                //MARK : operation
                if (ket.equals(""))
                    alert("mohon mengisi alasan laporan anda, terimakasih");
                else {

                    //MARK : obj
                    final brainLapor brainLapor = new brainLapor();

                    //MARK : operation
                    if (brainLapor.inputLaporSuperadmin(bundle.getString("idp"), bundle.getString("idm"), ket)) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(activityLaporMasjid.this);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                back();
                            }
                        });
                        builder.setMessage("laporan diterima, terimakasih atas laporan anda").show();
                        brainLapor.kirimNotifikasiSuperadmin("", "");
                    } else
                        alert("laporan gagal dikirimkan");
                }

            }
        });
        //endregion
    }

    private void alert(String msg) {

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityLaporMasjid.this);

        //MARK : operation
        builder.setMessage(msg).show();
    }

    private void back() {
        this.finish();
    }
}
