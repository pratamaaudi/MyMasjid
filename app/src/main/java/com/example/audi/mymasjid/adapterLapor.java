package com.example.audi.mymasjid;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;

import java.util.ArrayList;

public class adapterLapor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    String temp;
    private final Context context;
    private final ArrayList<classLapor> classLapors;
    private final Boolean masjid;

    public adapterLapor(Context context, ArrayList<classLapor> classLapors, Boolean masjid) {
        this.context = context;
        this.classLapors = classLapors;
        this.masjid = masjid;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.adapter_list_lapor, parent, false
        );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : obj
        final View view = holder.itemView;
        final classLapor classLapor = classLapors.get(position);
        final classKomentar classKomentar = classLapor.getClassKomentar();
        final String idMasjid, idKomen, status, username;
        //region String
        idMasjid = classLapor.getIdMasjid();
        idKomen = classKomentar.getIdkomen();
        status = classLapor.getStatus();
        username = classKomentar.getUsername();
        //endregion

        //MARK : UI
        final EditText edtKeterangan;
        final TextView txtUser, txtKomen, txtStatus;
        final ImageButton btnEdit, btnHapus;
        //region Button
        btnEdit = view.findViewById(R.id.btnEdit);
        btnEdit.setTag(true);
        //endregion

        //region cek selesai
        //MARK : operation
        if (status.equals("selesai"))
            btnEdit.setTag(true);
        else
            btnEdit.setTag(false);
        //endregion

        //region edtKeterangan
        //MARK : UI
        edtKeterangan = view.findViewById(R.id.edtKeterangan);

        //MARK : operation
        edtKeterangan.setFocusable(false);
        edtKeterangan.setText(classLapor.getKeterangan());
        //endregion

        //region txtStatus
        //MARK : UI
        txtStatus = view.findViewById(R.id.txtStatus);

        //MARK : operation
        txtStatus.setText(status);
        //endregion

        //MARK : obj
        temp = "";

        //region btnBlokir
        //MARK : UI
        final ImageButton btnBlokir = view.findViewById(R.id.btnHapusAkun);

        //MARK : operation
        if (masjid) {
            if (Boolean.valueOf(String.valueOf(btnEdit.getTag())))
                disableButton(btnBlokir, returnDrawable(R.drawable.ic_block_white_24dp));
            else {
                //MARK : obj
                final brainBlokir brainBlokir = new brainBlokir();
                final String idp = classLapor.getClassKomentar().getProfil_id();

                //MARK : operation
                if (brainBlokir.cekBlokir(idp, idMasjid))
                    disableButton(btnBlokir, returnDrawable(R.drawable.ic_block_white_24dp));
                else {
                    btnBlokir.setEnabled(true);
                    changeButton(btnBlokir, returnDrawable(R.color.red), returnDrawable(R.drawable.ic_block_white_24dp));

                    //MARK : trigger
                    btnBlokir.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //MARK : obj
                            final brainMasjid brainMasjid = new brainMasjid();

                            brainMasjid.hapusMasjid(idp, idMasjid);
                            if (brainBlokir.blokir(idp, idMasjid)) {
                                alert("user " + username + " telah di blokir");
                                disableButton(btnBlokir, returnDrawable(R.drawable.ic_block_white_24dp));
                            }

                        }
                    });
                }
            }
        } else
            btnBlokir.setVisibility(View.GONE);

        //endregion

        //region btnHapus
        //koneksi dengan tombol hapus
        //MARK : UI
        btnHapus = view.findViewById(R.id.btnHapus);

        //cek pengakses halaman
        //MARK : operation
        if (masjid) {

            //cek status laporan
            if (Boolean.valueOf(String.valueOf(btnEdit.getTag())) || classLapor.getHapus_komentar().equals("1"))
                disableButton(btnHapus, returnDrawable(R.drawable.ic_delete_forever_white_24dp));
            else {
                btnHapus.setEnabled(true);
                changeButton(btnHapus, returnDrawable(R.color.red), returnDrawable(R.drawable.ic_delete_forever_white_24dp));

                //MARK : trigger
                //hapus komentar
                btnHapus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //MARK : obj
                        brainKomentar brainKomentar = new brainKomentar();

                        //MARK : operation
                        if (brainKomentar.hapusKomentar(idKomen)) {

//                            //MARK : obj
//                            final brainLapor brainLapor = new brainLapor();
//                            if (brainLapor.updateHapusKomentar(classLapor.getIdLapor()))
                            disableButton(btnHapus, returnDrawable(R.drawable.ic_delete_forever_white_24dp));
                            classLapors.get(position).setHapus_komentar("1");
                        } else
                            alert("komentar gagal dihapus");
                    }
                });
            }
        } else
            btnHapus.setVisibility(View.GONE);
        //endregion

        //region btnEdit
        //MARK : operation

        if (masjid) {
            if (Boolean.valueOf(String.valueOf(btnEdit.getTag())))
                disableButton(btnEdit, returnDrawable(R.drawable.ic_check_white_24dp));
            else {
                btnEdit.setEnabled(true);
                changeButton(btnEdit, returnDrawable(R.color.green), returnDrawable(R.drawable.ic_check_white_24dp));
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //MARK : obj
                        final brainLapor brainLapor = new brainLapor();

                        //MARK : operation
                        if (brainLapor.updateStatLapor(idMasjid, idKomen)) {
                            alert("status berhasil di update");
                            txtStatus.setText("selesai");
                            disableButton(btnBlokir, returnDrawable(R.drawable.ic_block_white_24dp));
                            btnHapus.setEnabled(false);
                            disableButton(btnEdit, returnDrawable(R.drawable.ic_check_white_24dp));
                            btnEdit.setTag(true);
                            classLapors.get(position).setStatus("selesai");
                        } else
                            alert("status gagal di update periksa koneksi anda");
                    }
                });
            }
        } else {
            if (Boolean.valueOf(String.valueOf(btnEdit.getTag()))) {
                disableButton(btnEdit, returnDrawable(R.drawable.ic_edit_white_24dp));
            } else {
                btnEdit.setTag(true);
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Boolean.valueOf(String.valueOf(btnEdit.getTag()))) {

                            if (temp.equals("")) {

                                //MARK : obj
                                temp = gettext(edtKeterangan);

                                //MARK : operation edit mode on
                                edtKeterangan.setFocusableInTouchMode(true);
                                btnEdit.setTag(false);
                                changeButton(btnEdit, returnDrawable(R.color.green), returnDrawable(R.drawable.ic_save_black_24dp));
//                                btnEdit.setText("SIMPAN");

                            } else
                                alert("mohon menyelesaikan edit keterangan lain terlebihdahulu");

                        } else {

                            //MARK : obj
                            final String keterangan = gettext(edtKeterangan);

                            //MARK : operation cek hasil editan user
                            if (keterangan.equals(""))
                                alert("keterangan tidak boleh kosong");
                            else {

                                //region update keterangan
                                //MARK : obj
                                final brainLapor brainLapor = new brainLapor();

                                if (brainLapor.updateKeteranganLapor(idMasjid, idKomen, keterangan))
                                    alert("keterangan berhasil di update");
                                else {
                                    alert("keterangan gagal di update, periksa koneksi anda");
                                    edtKeterangan.setText(temp);
                                    edtKeterangan.setFocusable(false);
                                }
                                //endregion

                                //region cleaning
                                //MARK : operation cleaning
                                temp = "";
                                btnEdit.setTag(true);
                                changeButton(btnEdit, returnDrawable(R.color.yellow), returnDrawable(R.drawable.ic_edit_white_24dp));
//                                btnEdit.setText("EDIT");
                                //endregion
                            }
                        }
                    }
                });
            }
        }

        //endregion

        //region txtUser
        //MARK : UI
        txtUser = view.findViewById(R.id.txtUser);

        //MARK : operation
        txtUser.setText(username);
        //endregion

        //region txtKomen
        //MARK : UI
        txtKomen = view.findViewById(R.id.txtMasjid);

        //MARK : operation
        txtKomen.setText(classKomentar.getKomentar());
        //endregion

    }

    @Override
    public int getItemCount() {
        return classLapors.size();
    }

    public Context getContext() {
        return context;
    }

    public void alert(String msg) {
        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        //MARK : operation
        builder.setMessage(msg).show();
    }

    public Boolean getMasjid() {
        return masjid;
    }

    public String gettext(EditText edtKeterangan) {
        return edtKeterangan.getText().toString();
    }

    private Drawable returnDrawable(int id) {
        return context.getResources().getDrawable(id);
    }

    private void changeButton(ImageButton button, Drawable warna, Drawable gambar) {
        button.setBackground(warna);
        button.setImageDrawable(gambar);
    }

    private void disableButton(ImageButton button, Drawable gambar) {
        button.setEnabled(false);
        changeButton(button, returnDrawable(R.color.blue_gray), gambar);
    }
}
