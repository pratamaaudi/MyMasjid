package com.example.audi.mymasjid;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentMasjid extends android.support.v4.app.Fragment {
    ArrayList<classMasjid> classMasjids;
    View view;
    classLogin classLogin;

    public fragmentMasjid() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_masjid, container, false);

        //MARK : obj
        classMasjids = new ArrayList<>();
        classLogin = this.getArguments().getParcelable("classLogin");

        //region load list masjid
        //MARK : obj
        final brainMasjid brainMasjid = new brainMasjid();
        final String userId = classLogin.getId();
        final adapterMasjid adapterMasjid = new adapterMasjid(view.getContext(), classMasjids, classLogin);

        //MARK : function
        new loadMasjid(brainMasjid, adapterMasjid).execute(userId);
        //endregion

        //region FAB
        //MARK : UI
        final FloatingActionButton fabMasjid, fabCancel, fabTambah, fabHapus;
        //region FloatingActionButton
        fabMasjid = view.findViewById(R.id.fabMasjid);
        fabCancel = view.findViewById(R.id.fabCancel);
        fabTambah = view.findViewById(R.id.fabTambah);
        fabHapus = view.findViewById(R.id.fabHapus);
        //endregion
        final LinearLayout linearLayout = view.findViewById(R.id.llblock);
        final TextView txtCancel, txtHapus, txtTambah;
        //region TextView
        txtCancel = view.findViewById(R.id.txtCancel);
        txtHapus = view.findViewById(R.id.txtHapus);
        txtTambah = view.findViewById(R.id.txtTambah);
        //endregion

        //MARK : trigger
        fabMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                linearLayout.setVisibility(View.VISIBLE);
                fabMasjid.animate().alpha(0.0f);
                fabMasjid.setVisibility(View.INVISIBLE);
                fabCancel.setVisibility(View.VISIBLE);
                fabCancel.animate().alpha(1.0f);
                fabTambah.setVisibility(View.VISIBLE);
                fabTambah.animate().alpha(1.0f);
                fabHapus.setVisibility(View.VISIBLE);
                fabHapus.animate().alpha(1.0f);
                txtCancel.setVisibility(View.VISIBLE);
                txtHapus.setVisibility(View.VISIBLE);
                txtTambah.setVisibility(View.VISIBLE);
            }
        });

        fabCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                linearLayout.setVisibility(View.INVISIBLE);
                fabCancel.animate().alpha(0.0f);
                fabCancel.setVisibility(View.INVISIBLE);
                fabTambah.animate().alpha(0.0f);
                fabTambah.setVisibility(View.INVISIBLE);
                fabHapus.animate().alpha(0.0f);
                fabHapus.setVisibility(View.INVISIBLE);
                fabMasjid.setVisibility(View.VISIBLE);
                fabMasjid.animate().alpha(1.0f);
                txtCancel.setVisibility(View.INVISIBLE);
                txtHapus.setVisibility(View.INVISIBLE);
                txtTambah.setVisibility(View.INVISIBLE);
            }
        });

        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                Intent intent = new Intent(getContext(), activityEditMasjid.class);

                //MARK : operation
                intent.putExtra("requestCode", 001);
                startActivityForResult(intent, 001);
            }
        });

        fabHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                Intent intent = new Intent(getContext(), activityEditMasjid.class);

                //MARK : operation
                intent.putExtra("requestCode", 002);
                intent.putExtra("userId", userId);
                startActivityForResult(intent, 002);
            }
        });
        //endregion

        return view;
    }


    //memproses hasil dari halaman lain
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //mempersiapkan variabel
        //MARK : obj
        final String mid = data.getStringExtra("idMasjid");
        final String namaMasjid = data.getStringExtra("namaMasjid");
        final brainMasjid brainMasjid = new brainMasjid();
        final String userId = classLogin.getId();

        //memeriksa kode hasil dari halaman lain
        //MARK : operation
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                //jika kode 001 jalankan tambah masjid
                //region tambah masjid
                case 001:

                    //MARK : obj
                    final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();

                    //MARK : operation
                    if (brainMasjid.tambahMasjid(userId, mid, brainMasjid.loadKegiatanTerakhirMasjid(mid), brainMasjid.loadKegiatanRutinTerakhirMasjid(mid)))
                        alertDialog.setMessage("masjid " + namaMasjid + " berhasil diikuti");
                    else
                        alertDialog.setMessage("masjid " + namaMasjid + " gagal diikuti");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    break;
                //endregion

                //jika kode 002  jalankan hapus masjid
                //region hapus masjid
                case 002:
                    if (brainMasjid.hapusMasjid(userId, mid)) {

                        //MARK : obj
                        final AlertDialog alertDialog2 = new AlertDialog.Builder(getContext()).create();

                        //MARK : operation
                        alertDialog2.setMessage("masjid " + namaMasjid + " berhasil dihapus");
                        alertDialog2.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog2.show();
                    }
                    break;
                //endregion
                default:
                    break;
            }

            //muat ulang daftar masjid
            //MARK : operation
            classMasjids.clear();

            //MARK : obj
            final adapterMasjid adapterMasjid = new adapterMasjid(view.getContext(), classMasjids, classLogin);

            //MARK : function
            new loadMasjid(brainMasjid, adapterMasjid).execute(userId);
        }
    }

    private class loadMasjid extends AsyncTask<String, String, String> {

        //region set data

        //MARK : UI
        final RecyclerView recyclerView = view.findViewById(R.id.rclmasjid);
        final ProgressBar loading = view.findViewById(R.id.loading);
        final LinearLayoutManager llm = new LinearLayoutManager(view.getContext());

        //MARK : obj
        final brainMasjid brainMasjid;
        final adapterMasjid adapterMasjid;

        public loadMasjid(brainMasjid brainMasjid, adapterMasjid adapterMasjid) {
            this.brainMasjid = brainMasjid;
            this.adapterMasjid = adapterMasjid;
        }
        //endregion

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... data) {
            classMasjids = brainMasjid.loadFollowMasjid(classMasjids, data[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            recyclerView.setLayoutManager(llm);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapterMasjid);
            loading.setVisibility(View.INVISIBLE);

            //region constrantnokonten
            //MARK : UI
            final ConstraintLayout constrantnokonten = view.findViewById(R.id.constrantnokonten);

            //MARK : operation
            if (classMasjids.size() < 1)
                constrantnokonten.setVisibility(View.VISIBLE);
            else
                constrantnokonten.setVisibility(View.INVISIBLE);

            //endregion
        }
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
}
