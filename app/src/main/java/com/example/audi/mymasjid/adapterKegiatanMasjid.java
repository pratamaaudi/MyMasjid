package com.example.audi.mymasjid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class adapterKegiatanMasjid extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final Context context;
    final ArrayList<classKegiatan> classKegiatans;
    final Boolean masjid;
    final String idUser;

    public adapterKegiatanMasjid(Context context, ArrayList<classKegiatan> classKegiatans, Boolean masjid, String idUser){
        this.context = context;
        this.classKegiatans = classKegiatans;
        this.masjid = masjid;
        this.idUser = idUser;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_kegiatan_masjid, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TextView txtJudulKegiatan, txtNamaMasjid, txtTanggalMulai, txtTanggalSelesai, txtJamMulai,
                txtJamSelesai, txtDeskripsi;
        View itemView = holder.itemView;

        ImageButton btnHapus = itemView.findViewById(R.id.btnHapus);
        txtJudulKegiatan = itemView.findViewById(R.id.txtJudulKegiatan);
        txtNamaMasjid = itemView.findViewById(R.id.txtNamaMasjid);
        txtTanggalMulai = itemView.findViewById(R.id.txtTanggalMulai);
        txtTanggalSelesai = itemView.findViewById(R.id.txtTanggalSelesai);
        txtJamMulai = itemView.findViewById(R.id.txtJamMulai);
        txtJamSelesai = itemView.findViewById(R.id.txtJamSelesai);
        txtDeskripsi = itemView.findViewById(R.id.txtDeskripsi);

        //MARK : tampung class yang sedang aktif
        final classKegiatan classKegiatan = classKegiatans.get(position);

        //MARK : ambil data dari class yang sedang aktif
        txtJudulKegiatan.setText(classKegiatan.getNamaKegiatan());
        txtNamaMasjid.setText(classKegiatan.getNamaMasjid());
        txtTanggalMulai.setText(classKegiatan.getTanggalMulai());
        txtTanggalSelesai.setText(classKegiatan.getTanggalSelesai());
        txtJamMulai.setText(classKegiatan.getJamMulai());
        txtJamSelesai.setText(classKegiatan.getJamSelesai());
        txtDeskripsi.setText(classKegiatan.getDeskripsi());

        //MARK : UI
        ImageButton btnEdit = itemView.findViewById(R.id.btnEdit);

        if(masjid){
            btnHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    //MARK : obj
                                    final brainKegiatan brainKegiatan = new brainKegiatan();

                                    //MARK : operation
                                    if(brainKegiatan.hapusKegiatanMasjid(classKegiatan.getIdKegiatan(), classKegiatan.getIdMasjid()))
                                        alert("kegiatan telah dihapus");
                                    else
                                        alert("kegiatan gagal dihapus");
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setMessage("hapus kegiatan "+classKegiatans.get(position).getNamaKegiatan())
                            .setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener)
                            .show();
                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //MARK : obj
                    Intent intent = new Intent(context, activityTambahKegitan.class);

                    //MARK : operation
                    intent.putExtra("edit", true);
                    intent.putExtra("idm", classKegiatan.getIdMasjid());
                    intent.putExtra("idk", classKegiatan.getIdKegiatan());
                    context.startActivity(intent);
                }
            });

            ImageButton btnDetail = itemView.findViewById(R.id.btnDetail);
            btnDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, activityDetailKegiatan.class);
                    intent.putExtra("masjidId", classKegiatan.getIdMasjid());
                    intent.putExtra("namaMasjid",classKegiatan.getNamaMasjid());
                    intent.putExtra("deskripsi",classKegiatan.getDeskripsi());
                    intent.putExtra("idKegiatan",classKegiatan.getIdKegiatan());
                    intent.putExtra("idUser",idUser);
                    context.startActivity(intent);
                }
            });
        }else{
            btnHapus.setVisibility(View.INVISIBLE);
            btnEdit.setVisibility(View.INVISIBLE);
        }

        //region img kegiatan
        //MARK : UI
        final ImageView img = itemView.findViewById(R.id.img);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/masjid/Foto_Kegiatan/" + classKegiatan.getIdKegiatan() + ".jpg";

        //MARK : operation
        Glide.with(context).load(url).thumbnail(1.0f).into(img);

        //MARK : trigger
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : OBJ
                final Intent i = new Intent(context, activityDetailGambar.class);

                //MARK : operation
                i.putExtra("url", url);
                context.startActivity(i);
            }
        });
        //endregion
    }

    @Override
    public int getItemCount() {
        return classKegiatans.size();
    }

    private void alert(String msg) {
        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //MARK : operation
        builder.setMessage(msg).show();
    }
}
