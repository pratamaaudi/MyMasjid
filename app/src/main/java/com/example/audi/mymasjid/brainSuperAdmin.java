package com.example.audi.mymasjid;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public class brainSuperAdmin {

    classIpAddress classIpAddress = new classIpAddress();

    public Boolean hapusAkunMasjid(String idm) {
        //mempersiapkan variabel
        String respon = "";

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/superadmin/hapusakunmasjid.php");
        try {

            //mempersiapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }

        //memeriksa hasil dan mengembalikan hasil
        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }
}
