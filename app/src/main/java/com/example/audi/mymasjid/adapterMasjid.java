package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by MOFOSEFES on 10/18/2017.
 */

public class adapterMasjid extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<classMasjid> classMasjids;
    classLogin classLogin;

    public adapterMasjid(Context context, ArrayList<classMasjid> classMasjids, classLogin classLogin) {
        this.context = context;
        this.classMasjids = classMasjids;
        this.classLogin = classLogin;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.masjidlist_layout, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TextView txtNamaMasjid = holder.itemView.findViewById(R.id.txtNamaMasjid);
        ImageView imgMasjid = holder.itemView.findViewById(R.id.imgMasjid);

        String url = "http://103.52.146.34/mymasjid/Foto_Masjid/"
                + classMasjids.get(position).getId()
                + ".jpg";

        Log.e("mymasjidlog","load gambar : "+url);

        txtNamaMasjid.setText(classMasjids.get(position).getNama());
        Glide.with(context).load(url).thumbnail(0.1f).into(imgMasjid);

        imgMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, activityDetailMasjid.class);
                i.putExtra("id", classMasjids.get(position).getId());
                i.putExtra("judul", classMasjids.get(position).getNama());
                i.putExtra("classLogin", classLogin);
                i.putExtra("jenisAkun", "user");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
        Log.e("mymasjidlog","masjid ke "+position);
    }

    @Override
    public int getItemCount() {
        return classMasjids.size();
    }
}
