package com.example.audi.mymasjid;

public class classPengurusMasjid {
    private String id;
    private String nama;
    private String jbtn;
    private String kntk;

    public classPengurusMasjid(){
        setId("");
        setNama("");
        setJbtn("");
        setKntk("");
    }

    public classPengurusMasjid(String id, String nama, String jbtn, String kntk){
        setId(id);
        setNama(nama);
        setJbtn(jbtn);
        setKntk(kntk);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJbtn() {
        return jbtn;
    }

    public void setJbtn(String jbtn) {
        this.jbtn = jbtn;
    }

    public String getKntk() {
        return kntk;
    }

    public void setKntk(String kntk) {
        this.kntk = kntk;
    }
}
