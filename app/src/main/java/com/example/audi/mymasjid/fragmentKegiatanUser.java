package com.example.audi.mymasjid;


import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentKegiatanUser extends Fragment {


    public fragmentKegiatanUser() {
        // Required empty public constructor
    }

    ArrayList<classKegiatan> classKegiatans;
    RecyclerView rclKont;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_kegiatan_user, container, false);

        //MARK : obj
        final String idUser = this.getArguments().getString("uid");
        final brainKegiatan brainKegiatan = new brainKegiatan();

        //MARK : UI
        rclKont = view.findViewById(R.id.rclKont);

        //region isi arraylist classkegiatan
        //MARK : OBJ
        classKegiatans = new ArrayList<>();

        //MARK : Operation
        refreshData(idUser, brainKegiatan, view);
        //endregion

        //region setSpinner
        //MARK : obj
        final List<String> strings = brainKegiatan.loadDaftarMasjidKegiatanUser(idUser);

        //MARK : UI
        final MaterialSpinner spnMasjid = view.findViewById(R.id.spnMasjid);

        //MARK : operation
        spnMasjid.setItems(strings);
        if (strings.size() > 1)
            spnMasjid.setVisibility(View.VISIBLE);

        //MARK : trigger
        spnMasjid.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                //MARK : operation
                if (position > 0) {

                    //MARK : obj
                    final Integer pos = position;

                    //MARK : operation
                    new AsyncJob.AsyncJobBuilder<Boolean>();
                    AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
                        @Override
                        public void doOnBackground() {

                            //MARK : operation
                            classKegiatans.clear();

                            //MARK : obj
                            classKegiatans = brainKegiatan.loadKegiatanbyMasjid(idUser, strings.get(pos), classKegiatans);

                            AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                                @Override
                                public void doInUIThread() {
                                    refreshRecycler(idUser);
                                }
                            });
                        }
                    });

                } else {
                    refreshData(idUser, brainKegiatan, view);
                }
            }
        });
        //endregion

        //MARK : operation
        if (classKegiatans.size() == 0) {
        } else {
            //region set konten recycleview

            //MARK : OBJ
            final adapterKegiatanUser adapterKegiatanUser = new adapterKegiatanUser(getContext(), classKegiatans, idUser);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());

            //MARK : Operation
            rclKont.setLayoutManager(linearLayoutManager);
            rclKont.setHasFixedSize(true);
            rclKont.setAdapter(adapterKegiatanUser);
            //endregion
        }

        //region hide loading
        //MARK : UI
        ProgressBar prbLoad = view.findViewById(R.id.prbLoad);

        //MARK : operation
        prbLoad.setVisibility(View.INVISIBLE);
        //endregion

        return view;
    }

    //region FUNCTION
    private void refreshData(String idUser, brainKegiatan brainKegiatan, View view) {

        //MARK : operation
        classKegiatans.clear();
        classKegiatans = brainKegiatan.loadKegiatanUser(idUser, classKegiatans);
        refreshRecycler(idUser);

        cekNoKonten(view);
    }

    private void refreshRecycler(String idUser) {
        final adapterKegiatanUser adapterKegiatanUser = new adapterKegiatanUser(getContext(), classKegiatans, idUser);
        rclKont.setAdapter(adapterKegiatanUser);
        rclKont.invalidate();
    }

    private void cekNoKonten(View view){
        if (classKegiatans.size() < 1) {

            //MARK : UI
            final ConstraintLayout constrantnokonten = view.findViewById(R.id.constrantnokonten);

            //MARK : operation
            constrantnokonten.setVisibility(View.VISIBLE);
        }
    }
    //endregion

}
