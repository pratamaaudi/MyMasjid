package com.example.audi.mymasjid;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class firebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if(remoteMessage.getData().size()>0){}

        if(remoteMessage.getNotification()!=null){
            sendnotification(remoteMessage.getNotification().getBody());
            Log.e("mymasjidlog","buat notif"+remoteMessage.getNotification().getBody());
        }
    }

    private void sendnotification(String body) {

        NotificationCompat.Builder notifbuilder =  new NotificationCompat.Builder(this);
        notifbuilder.setSmallIcon(R.mipmap.ic_launcher);
        notifbuilder.setContentTitle("Firebase notif");
        notifbuilder.setContentText(body);
        notifbuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,notifbuilder.build());

    }


}
