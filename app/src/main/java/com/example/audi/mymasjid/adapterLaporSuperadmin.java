package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

public class adapterLaporSuperadmin extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<classLaporSuperadmin> classLaporSuperadmins;
    Context context;

    public adapterLaporSuperadmin(ArrayList<classLaporSuperadmin> classLaporSuperadmins, Context context) {
        this.classLaporSuperadmins = classLaporSuperadmins;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //MARK : obj
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_lapor_superadmin, parent, false);
        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        //MARK : operation
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : UI
        //koneksi dengan tombol hapus
        final View view = holder.itemView;
        final Button btnHapusAkun = view.findViewById(R.id.btnHapusAkun);
        final Button btnLihatAkun = view.findViewById(R.id.btnLihatAkun);

        //MARK : trigger
        //hapus masjid
        btnHapusAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final brainSuperAdmin brainSuperAdmin = new brainSuperAdmin();

                //MARK : operation
                brainSuperAdmin.hapusAkunMasjid(classLaporSuperadmins.get(position).getIdm());
            }
        });

        btnLihatAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Intent intent = new Intent(context, activityDetailMasjid.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classLaporSuperadmins.size();
    }
}
