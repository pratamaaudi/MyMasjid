package com.example.audi.mymasjid;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by prata on 10/19/2017.
 */

public class adapterEditMasjid extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //region setData
    Context context;
    ArrayList<classMasjid> classMasjids;
    Activity activity;
    Integer requestCode;

    public adapterEditMasjid(Context context, ArrayList<classMasjid> classMasjids, Activity activity, Integer requestCode) {
        this.context = context;
        this.classMasjids = classMasjids;
        this.activity = activity;
        this.requestCode = requestCode;
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.masjidlist_layout, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //region set txtnamamasjid
        //MARK : UI
        final TextView txtNamaMasjid = holder.itemView.findViewById(R.id.txtNamaMasjid);

        //MARK : operation
        txtNamaMasjid.setText(classMasjids.get(position).getNama());
        //endregion

        //region imageview imgmasjid
        //MARK : UI
        final ImageView imgMasjid = holder.itemView.findViewById(R.id.imgMasjid);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/Foto_Masjid/" + classMasjids.get(position).getId() + ".jpg";

        //MARK : operation
        Glide.with(context).load(url).thumbnail(0.1f).into(imgMasjid);

        //MARK : trigger
        imgMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : trigger
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:

                                //MARK : obj
                                final Intent intent = new Intent();

                                //MARK : operation
                                intent.putExtra("namaMasjid", classMasjids.get(position).getNama());
                                intent.putExtra("idMasjid", classMasjids.get(position).getId());
                                activity.setResult(Activity.RESULT_OK, intent);
                                activity.finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                //MARK : obj
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);

                //MARK : operation
                switch (requestCode) {
                    case 001:
                        builder.setMessage("Ikuti masjid " + classMasjids.get(position).getNama() + " ?").setPositiveButton("Yes", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();
                        break;
                    case 002:
                        builder.setMessage("Hapus masjid " + classMasjids.get(position).getNama() + " ?").setPositiveButton("Yes", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();
                        break;
                }
            }
        });
        //endregion
    }

    @Override
    public int getItemCount() {
        return classMasjids.size();
    }
}
