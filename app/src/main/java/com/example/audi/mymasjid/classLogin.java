package com.example.audi.mymasjid;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MOFOSEFES on 10/14/2017.
 */

public class classLogin implements Parcelable {
    private String username;
    private String tipe;
    private String id;
    private String email;
    private String jenis;
    private String idMasjid;
    private String provinsi;
    private String kotakab;
    private String kecamatan;

    public classLogin(String username, String tipe, String id, String email, String jenis, String idMasjid, String provinsi, String kotakab, String kecamatan) {
        setUsername(username);
        setTipe(tipe);
        setId(id);
        setEmail(email);
        setJenis(jenis);
        setIdMasjid(idMasjid);
        setProvinsi(provinsi);
        setKotakab(kotakab);
        setKecamatan(kecamatan);
    }

    protected classLogin(Parcel in) {
        username = in.readString();
        tipe = in.readString();
        id = in.readString();
        email = in.readString();
        jenis = in.readString();
        idMasjid = in.readString();
        provinsi = in.readString();
        kotakab = in.readString();
        kecamatan = in.readString();
    }

    public static final Creator<classLogin> CREATOR = new Creator<classLogin>() {
        @Override
        public classLogin createFromParcel(Parcel in) {
            return new classLogin(in);
        }

        @Override
        public classLogin[] newArray(int size) {
            return new classLogin[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(tipe);
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(jenis);
        dest.writeString(idMasjid);
        dest.writeString(provinsi);
        dest.writeString(kotakab);
        dest.writeString(kecamatan);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getIdMasjid() {
        return idMasjid;
    }

    public void setIdMasjid(String idMasjid) {
        this.idMasjid = idMasjid;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKotakab() {
        return kotakab;
    }

    public void setKotakab(String kotakab) {
        this.kotakab = kotakab;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
}
