package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;
import com.bumptech.glide.Glide;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by MOFOSEFES on 10/14/2017.
 */

public class adapterPosting extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //region setData
    final Context context;
    final ArrayList<classPosting> classPostings;
    final classLogin classLogin;
    final Boolean masjid;
    final Boolean halMasjid;

    public adapterPosting(Context context, ArrayList<classPosting> classPostings, classLogin classLogin, Boolean masjid, Boolean halMasjid) {
        this.context = context;
        this.classPostings = classPostings;
        this.classLogin = classLogin;
        this.masjid = masjid;
        this.halMasjid = halMasjid;
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.prototype_news, parent, false
        );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : obj
        final View view = holder.itemView;
        final classPosting classPosting = classPostings.get(position);
        final String stt, namaMasjid, idMasjid, idStatus;
        final brainKomentar brainKomentar = new brainKomentar();
        //region Strings
        stt = classPosting.getStatus();
        namaMasjid = classPosting.getNamaMasjid();
        idMasjid = classPosting.getMasjidId();
        idStatus = classPosting.getStatusId();
        //endregion

        //region set tanggal postingan
        //MARK : UI
        final TextView txtTanggal = view.findViewById(R.id.txtTanggal);

        //MARK : operation
        txtTanggal.setText(classPosting.getTanggal());
        //endregion

        //region set foto profil masjid
        //MARK : UI
        final ImageView img = view.findViewById(R.id.img);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/Foto_Masjid/"
                + idMasjid
                + ".jpg";

        //MARK : operation
        Glide.with(context).load(url).thumbnail(0.1f).into(img);
        //endregion

        //region set foto postingan + trigger foto
        //MARK : UI
        final ImageView imgStatus = view.findViewById(R.id.imgStatus);

        //MARK : obj
        final String url2 = "http://103.52.146.34/mymasjid/masjid/Foto_Status/" + idStatus + ".jpg";

        //MARK : operation
        Glide.with(context).load(url2).thumbnail(1.0f).into(imgStatus);

        //MARK : trigger
        imgStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : OBJ
                final Intent i = new Intent(context, activityDetailGambar.class);

                //MARK : operation
                i.putExtra("url", url2);
                context.startActivity(i);
            }
        });
        //endregion

        //region llHeaderPosting
        //MARK : UI
        final LinearLayout llHeaderPosting = view.findViewById(R.id.llHeaderPosting);

        if (!halMasjid) {
            //MARK : trigger
            llHeaderPosting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //MARK : obj
                    final Intent i = new Intent(context, activityDetailMasjid.class);

                    //MARK : operation
                    i.putExtra("id", idMasjid);
                    i.putExtra("judul", namaMasjid);
                    i.putExtra("jenisAkun", "user");
                    i.putExtra("classLogin", classLogin);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            });
        }
        //endregion

        //region llStatus
        //MARK : UI
        final LinearLayout llStatus = view.findViewById(R.id.llstatus);

        //MARK : trigger
        llStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDetailPosting(position, masjid);
            }
        });
        //endregion

        //region set nama masjid
        //MARK : UI
        final TextView namamasjid = view.findViewById(R.id.namaMasjid);

        //MARK : operation
        namamasjid.setText(namaMasjid);
        //endregion

        //region set status masjid
        //MARK : UI
        final TextView status = view.findViewById(R.id.status);

        //MARK : operation
        status.setText(stt);
        //endregion

        //region LLKomen
        //MARK : UI
        final LinearLayout llKomen = view.findViewById(R.id.llKomen);

        new AsyncJob.AsyncJobBuilder<Boolean>();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //MARK : obj
                final brainKomentar brainKomentar = new brainKomentar();
                final classKomentar classKomentar = brainKomentar.loadKomentarTerakhir(idStatus);

                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {

                        if (classKomentar.getIdkomen().equals(""))
                            llKomen.setVisibility(View.GONE);
                        else {

                            //MARK : UI
                            final TextView txtNama, txtKomen;

                            //region txtNama
                            //MARK : UI
                            txtNama = view.findViewById(R.id.txtNama);

                            //MARK : operation
                            txtNama.setText(classKomentar.getUsername());
                            //endregion

                            //region txtKomen
                            //MARK : IO
                            txtKomen = view.findViewById(R.id.textview2);

                            //MARK : operation
                            txtKomen.setText(classKomentar.getKomentar());
                            //endregion

                            //MARK : trigger
                            llKomen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openDetailPosting(position, masjid);
                                }
                            });
                        }
                    }
                });
            }
        });
        //endregion

        //region button komen
        //MARK : UI
        final ImageButton btnKomen = view.findViewById(R.id.btnKomen);

        //MARK : trigger
        btnKomen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDetailPosting(position, masjid);
            }
        });
        //endregion

        //region set jml like
        //MARK : UI
        final Button btnjmllike = view.findViewById(R.id.btnjmllike);

        btnjmllike.setText(brainKomentar.loadjmllikeposting(idStatus));
        //endregion

        //region btnLike
        //MARK : UI
        final ImageButton btnLike = view.findViewById(R.id.btnLike);

        //operation
        if (brainKomentar.cekLike(classLogin.getId(), idStatus))
            likeButton(btnLike, btnjmllike);
//            btnLike.setText("UNLIKE");


        //MARK : trigger
        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                like(classLogin.getId(), idStatus, context, btnLike, btnjmllike);
            }
        });

        btnjmllike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                like(classLogin.getId(), idStatus, context, btnLike, btnjmllike);
            }
        });
        //endregion

        //region txtTopik
        //MARK : UI
        final TextView txtTopik = view.findViewById(R.id.txtTopik);

        //MARK : operation
        txtTopik.setText("Topik : " + classPosting.getTopik());
        //endregion

        //region setPopup
        //MARK : UI
        final ImageView imgMore = view.findViewById(R.id.imgMore);

        //MARK : operation
        if (masjid) {
            //MARK : obj
            final DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(context, imgMore);
            final DroppyMenuPopup droppyMenu = droppyBuilder.fromMenu(R.menu.menu_adapter_posting)
                    .triggerOnAnchorClick(false)
                    .setOnClick(new DroppyClickCallbackInterface() {
                        @Override
                        public void call(View v, final int id) {
                            switch (id) {
                                case R.id.edit:

                                    //MARK : obj
                                    final Intent intent = new Intent(context, activityTambahPosting.class);

                                    //MARK : operation
                                    intent.putExtra("edit", true);
                                    intent.putExtra("url", url2);
                                    intent.putExtra("status", stt);
                                    intent.putExtra("ids", idStatus);
                                    intent.putExtra("idm", idMasjid);
                                    context.startActivity(intent);

                                    break;
                                case R.id.hapus:

                                    //MARK : obj
                                    final brainBerita brainBerita = new brainBerita();

                                    //MARK : operation
                                    if (brainBerita.hapusStatus(idStatus)) {
                                        classPostings.remove(position);
                                        notifyItemRemoved(position);
                                        notifyItemChanged(position, classPosting);
                                    }
                                    break;

                            }
                        }
                    })
                    .setOnDismissCallback(new DroppyMenuPopup.OnDismissCallback() {
                        @Override
                        public void call() {
                        }
                    })
                    .setPopupAnimation(new DroppyFadeInAnimation())
                    .setXOffset(5)
                    .setYOffset(5)
                    .build();

            //MARK : trigger
            imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    droppyMenu.show();
                }
            });
        } else
            imgMore.setVisibility(View.INVISIBLE);
        //endregion
    }

    @Override
    public int getItemCount() {
        return classPostings.size();
    }

    //region FUNCTION
    public void openDetailPosting(int position, boolean masjid) {

        //MARK : obj
        final Intent i = new Intent(context, activityDetailPosting.class);
        final classPosting classPosting = classPostings.get(position);

        //MARK : operation
        i.putExtra("namaMasjid", classPosting.getNamaMasjid());
        i.putExtra("masjidId", classPosting.getMasjidId());
        i.putExtra("status", classPosting.getStatus());
        i.putExtra("statusid", classPosting.getStatusId());
        i.putExtra("topik", classPosting.getTopik());
        i.putExtra("userid", classLogin.getId());
        i.putExtra("username", classLogin.getUsername());
        i.putExtra("masjid", masjid);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public void like(final String uid, final String ssid, final Context context, final ImageButton btnLike, final Button btnjmllike) {

        //MARK : obj
        final ProgressDialog pDialog = new ProgressDialog(context);

        //MARK : operation
        pDialog.setMessage("Loading . . .");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        //MARK : async
        AsyncJob.OnBackgroundJob like = new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //MARK : obj
                final brainKomentar brainKomentar = new brainKomentar();
                final Boolean liking;

                //MARK : operation
                if (brainKomentar.cekLike(uid, ssid)) {
                    brainKomentar.dislike(uid, ssid);
                    liking = false;
                } else {
                    brainKomentar.like(uid, ssid);
                    liking = true;
                }

                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        if (liking) {
//                            btnLike.setText("UNLIKE");
                            likeButton(btnLike, btnjmllike);
                            alert("status telah di like", context);

                            //MARK : UI
                            btnjmllike.setText(String.valueOf(Integer.valueOf(btnjmllike.getText().toString()) + 1));
                        } else {
//                            btnLike.setText("LIKE");
                            btnLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_up_white_24dp));
                            btnLike.setBackground(context.getResources().getDrawable(R.color.blue));
                            alert("status di unlike", context);
                            btnjmllike.setText(String.valueOf(Integer.valueOf(btnjmllike.getText().toString()) - 1));
                            btnjmllike.setBackground(context.getResources().getDrawable(R.color.blue));
                        }
                        pDialog.dismiss();
                    }
                });
            }
        };
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        AsyncJob.doInBackground(like, executorService);
    }

    private void alert(String msg, Context context) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        //MARK : operation
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void likeButton(ImageButton btnLike, Button btnjmllike) {
        btnLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_thumb_up_white_24dp));
        btnLike.setBackground(context.getResources().getDrawable(R.color.green));
        btnjmllike.setBackground(context.getResources().getDrawable(R.color.green));
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
    //endregion
}
