package com.example.audi.mymasjid;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arasthel.asyncjob.AsyncJob;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentKegiatanMasjid extends Fragment {

    public fragmentKegiatanMasjid() {
        // Required empty public constructor
    }

    ArrayList<classKegiatan> classKegiatans;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_fragment_kegiatan_masjid, container, false);

        //MARK : glb-obj
        final Bundle bundle = this.getArguments();
        final Context context = view.getContext();

        new AsyncJob.AsyncJobBuilder<Boolean>();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //MARK : glb-obj
                final String idMasjid = bundle.getString("idm");
                final Boolean masjid = bundle.getBoolean("masjid");
                classKegiatans = new ArrayList<>();

                //region isi arraylist class kegiatans
                //MARK : obj
                final brainKegiatan brain = new brainKegiatan();
                classKegiatans = brain.loadKegiatanMasjid(idMasjid, classKegiatans);

                //MARK : operation
                if (!masjid)
                    if (classKegiatans.size() > 0)
                        brain.updateReadKegiatan(idMasjid, bundle.getString("idp"), classKegiatans.get((classKegiatans.size() - 1)).getIdKegiatan());
                //endregion

                //MARK : UI
                final ConstraintLayout constrantnokonten = view.findViewById(R.id.constrantnokonten);

                //MARK : obj
                final String userId = bundle.getString("idp");
                final adapterKegiatanMasjid adapterKegiatanMasjid = new adapterKegiatanMasjid(context, classKegiatans, masjid,userId);
                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

                //region UI THREAD
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {

                        //region recyclerview
                        //MARK : UI
                        final RecyclerView rclKegiatan = view.findViewById(R.id.rclKegiatan);

                        //MARK : operation
                        rclKegiatan.setLayoutManager(linearLayoutManager);
                        rclKegiatan.setHasFixedSize(true);
                        rclKegiatan.setAdapter(adapterKegiatanMasjid);
                        //endregion

                        //region progressbar loading
                        //MARK : UI
                        final ProgressBar loading = view.findViewById(R.id.loading);

                        //MARK : operation
                        loading.setVisibility(View.INVISIBLE);
                        //endregion

                        //region constrantnokonten
                        //MARK : operation
                        brainLog brainLog = new brainLog();
                        brainLog.Log("frgkegiatanmasjid", "classkegiatan", String.valueOf(classKegiatans.size()));
                        if (classKegiatans.size() == 0)
                            constrantnokonten.setVisibility(View.VISIBLE);
                        else
                            constrantnokonten.setVisibility(View.INVISIBLE);
                        //endregion
                    }
                });
                //endregion
            }
        });
        return view;
    }
}
