package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 11/18/2017.
 */

public class brainProfil {

    classIpAddress classIpAddress = new classIpAddress();

    public boolean cekEmailEdit(String email, String idp) {
        boolean hasil = false;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://" + classIpAddress.getIp() + "/mymasjid/cekemailedit.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        return hasil;
    }

    public boolean updateProfil(classLogin classLogin) {

        //penentu update berhasil atau gak
        boolean hasil = false;

        //MARK : ambil data dari classLogin
        String username = classLogin.getUsername();
        String email = classLogin.getEmail();
        String id = classLogin.getId();

        //penampung hasil php
        String respon;

        //koneksi ke php
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://" + classIpAddress.getIp() + "/mymasjid/user/updateprofile.php");
        try {

            //data yang dikirimkan
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("username", username.trim()));
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            nameValuePairs.add(new BasicNameValuePair("id", id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //jalankan koneksi php
            HttpResponse response = httpClient.execute(httppost);

            //tampung respon php ke respon
            respon = EntityUtils.toString(response.getEntity());

            //kalau respon php true
            if (respon.equals("true")) {

                //hasil di ubah true
                hasil = true;
            }

        } catch (Exception e) {
        }

        //kembalikan hasil
        return hasil;
    }

    public List<String> loadUnreadKegiatan(String idp, String idm) {

        brainLog brainLog = new brainLog();
        brainLog.Log("brainprofil", "idp", idp);
        brainLog.Log("brainprofil", "idm", idm);

        List<String> strings = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://" + classIpAddress.getIp() + "/mymasjid/loadlastreaduser.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            brainLog.Log("brainprofil", "respon", respon);

            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String idk = c.getString("kegiatan_id");
                String idj = c.getString("jadwal_id");

                strings.add(idk);
                strings.add(idj);
            }
        } catch (Exception e) {
        }
        return strings;
    }

    public boolean updateProfilMasjid(classLogin classLogin) {

        //penentu update berhasil atau gak
        boolean hasil = false;

        //MARK : ambil data dari classLogin
        String username = classLogin.getUsername();
        String email = classLogin.getEmail();
        String id = classLogin.getId();
        String idm = classLogin.getIdMasjid();

        //penampung hasil php
        String respon;

        //koneksi ke php
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://" + classIpAddress.getIp() + "/mymasjid/masjid/updateprofilemasjid.php");
        try {

            //data yang dikirimkan
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("username", username.trim()));
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            nameValuePairs.add(new BasicNameValuePair("id", id.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //jalankan koneksi php
            HttpResponse response = httpClient.execute(httppost);

            //tampung respon php ke respon
            respon = EntityUtils.toString(response.getEntity());

            //kalau respon php true
            if (respon.equals("true")) {

                //hasil di ubah true
                hasil = true;
            }

        } catch (Exception e) {
        }

        //kembalikan hasil
        return hasil;
    }

    public Boolean logout(String idp) {
//penentu update berhasil atau gak
        boolean hasil = false;

        //penampung hasil php
        String respon;

        //koneksi ke php
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://" + classIpAddress.getIp() + "/mymasjid/logout.php");
        try {

            //data yang dikirimkan
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //jalankan koneksi php
            HttpResponse response = httpClient.execute(httppost);

            //tampung respon php ke respon
            respon = EntityUtils.toString(response.getEntity());

            //kalau respon php true
            if (respon.equals("true")) {

                //hasil di ubah true
                hasil = true;
            }

        } catch (Exception e) {
        }

        //kembalikan hasil
        return hasil;
    }

//    public classLogin selectProfil(String idMasjid, classLogin classLogin) {
//        String respon;
//        HttpClient httpClient = new DefaultHttpClient();
//        HttpPost httppost = new HttpPost("http://103.52.146.34/mymasjid/profil/selectprofil.php");
//
//        try {
//            List<NameValuePair> nameValuePairs = new ArrayList<>();
//            nameValuePairs.add(new BasicNameValuePair("idMasjid", idMasjid.trim()));
//            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
//            HttpResponse response = httpClient.execute(httppost);
//            respon = EntityUtils.toString(response.getEntity());
//            Log.e("mymasjidlog","respon server : "+respon);
//            JSONObject json = new JSONObject(respon);
//            JSONArray json2 = json.getJSONArray("post");
//
//            for (int i = 0; i < json2.length(); i++) {
//                JSONObject c = json2.getJSONObject(i);
//                String username = c.getString("nama");
//                String id = c.getString("id");
//                String email = c.getString("email");
//                String jenis = c.getString("jenis");
//
//                classLogin = new classLogin(username,id,email,jenis,idMasjid);
//            }
//        } catch (Exception e) {
//        }
//        return classLogin;
//    }
}
