package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 10/9/2017.
 */

public class brainDaftar {

    classIpAddress classIpAddress = new classIpAddress();

    public boolean cekEmailUser(String email) {
        boolean hasil = false;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekemailuser.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }
        } catch (Exception e) {
        }

        return hasil;
    }

    public boolean daftarUser(String nama, String email, String password, String idd) {

        //mempersiapkan variabel
        boolean hasil = false;
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/daftaruser.php");
        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("nama", nama.trim()));
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            nameValuePairs.add(new BasicNameValuePair("password", password.trim()));
            nameValuePairs.add(new BasicNameValuePair("idd", idd.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }

        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public boolean cekEmailMasjid(String email) {
        boolean hasil = false;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekemailmasjid.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if (respon.equals("true")) {
                hasil = true;
            }

        } catch (Exception e) {
        }

        return hasil;
    }

    public boolean daftarMasjid(String nama, String email, String password, String imgFotoMasjid, String namaMasjid, String alamat, String kontak, String idd) {

        //mempersiapkan variabel
        boolean hasil = false;
        String respon;

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/daftarmasjid.php");
        try {

            //memperisapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("nama", nama.trim()));
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            nameValuePairs.add(new BasicNameValuePair("password", password.trim()));
            nameValuePairs.add(new BasicNameValuePair("imgFotoMasjid", imgFotoMasjid));
            nameValuePairs.add(new BasicNameValuePair("namaMasjid", namaMasjid));
            nameValuePairs.add(new BasicNameValuePair("alamat", alamat));
            nameValuePairs.add(new BasicNameValuePair("kontak", kontak));
            nameValuePairs.add(new BasicNameValuePair("idd", idd));

            //mengirim data dan menampung hasil
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            brainLog brainLog = new brainLog();
            brainLog.Log("braindaftar","respon",respon);

            //memeriksa hasil
            if (respon.equals("true")) {
                hasil = true;
            }

        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public ArrayList<classProvinsi> loadProvinsi(ArrayList<classProvinsi> classProvinsis){
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadprovinsi.php");

        try {
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String name = c.getString("name");

                classProvinsis.add(new classProvinsi(id,name));
            }
        } catch (Exception e) {
        }
        return classProvinsis;
    }

    public ArrayList<classKotakab> loadKotakab(ArrayList<classKotakab> classKotakabs, String province_id){
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkotakab.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("province_id", province_id.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String name = c.getString("name");
                String province_id2 = c.getString("province_id");

                classKotakabs.add(new classKotakab(id,name,province_id2));
            }
        } catch (Exception e) {
        }
        return classKotakabs;
    }

    public ArrayList<classKecamatan> loadKecamatan(ArrayList<classKecamatan> classKecamatans, String idr){
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadkecamatan.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idr", idr.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String name = c.getString("name");

                classKecamatans.add(new classKecamatan(id,name,idr));
            }
        } catch (Exception e) {
        }
        return classKecamatans;
    }
}
