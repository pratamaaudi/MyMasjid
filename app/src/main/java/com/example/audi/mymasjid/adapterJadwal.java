package com.example.audi.mymasjid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class adapterJadwal extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //region setData
    //MARK : obj
    final Context context;
    final ArrayList<classjadwal> classjadwals;
    final Boolean masjid;
    final Boolean halUser;
    final String pid;
    final classLogin classLogin;
    final String mid;

    public adapterJadwal(Context context, ArrayList<classjadwal> classjadwals, Boolean masjid, String pid, Boolean halUser, classLogin classLogin, String mid) {
        this.context = context;
        this.classjadwals = classjadwals;
        this.masjid = masjid;
        this.pid = pid;
        this.halUser = halUser;
        this.classLogin = classLogin;
        this.mid = mid;
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //MARK : obj
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_jadwal, parent, false);
        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        //MARK : operation
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //MARK : obj
        final View view = holder.itemView;
        final classjadwal classjadwal = classjadwals.get(position);
        final brainJadwal brainJadwal = new brainJadwal();
        final String idj = classjadwal.getId();

        //region covert hari
        //MARK : obj
        final String hari;

        //MARK : operation
//        switch (classjadwal.getHari()) {
//            case "Monday":
//                hari = "Senin";
//                break;
//            case "Tuesday":
//                hari = "Selasa";
//                break;
//            case "Wednesday":
//                hari = "Rabu";
//                break;
//            case "Thursday":
//                hari = "Kamis";
//                break;
//            case "Friday":
//                hari = "Jumat";
//                break;
//            case "Saturday":
//                hari = "Sabtu";
//                break;
//            default:
//                hari = "Minggu";
//                break;
//        }
        //endregion

        //region set TextView
        //MARK : UI
        final TextView txtJud, txtKet, txtTanggal;
        //region TextView
        txtJud = view.findViewById(R.id.txtjud);
        txtKet = view.findViewById(R.id.txtket);
        txtTanggal = view.findViewById(R.id.txtTanggal);
        //endregion

        //MARK : operation
        txtJud.setText(classjadwal.getJudul());
        txtKet.setText(classjadwal.getKeterangan());

        //MARK : obj
//        Integer hourOfDay = Integer.parseInt(classjadwal.getJam());
//        Integer minute = Integer.parseInt(classjadwal.getMenit());
        final String jamString;
        final String menitString;

        //MARK: operation
//        if (hourOfDay < 10)
//            jamString = "0" + String.valueOf(hourOfDay);
//        else
//            jamString = String.valueOf(hourOfDay);
//        if (minute < 10)
//            menitString = "0" + String.valueOf(minute);
//        else
//            menitString = String.valueOf(minute);
//        txtTanggal.setText(hari + ", " + jamString + ":" + menitString);
        //endregion

        //region imageView
        //MARK : UI
        final ImageView img = view.findViewById(R.id.img);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/masjid/Foto_Jadwal/" + classjadwal.getId() + ".jpg";

        //MARK : operation
        Glide.with(context).load(url).thumbnail(1.0f).into(img);

        //MARK : trigger
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Intent intent = new Intent(context, activityDetailGambar.class);

                //MARK : operation
                intent.putExtra("url", url);
                context.startActivity(intent);
            }
        });
        //endregion

        //region btnHapus
        //MARK : UI
        final ImageButton btnHapus = view.findViewById(R.id.btnHapus);

        //MARK : operation
        if (masjid) {
            btnHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (brainJadwal.hapusJadwal(classjadwal.getId())) {

                        //MARK : function
                        alert("data berhasil di hapus");

                        //MARK : operation
                        classjadwals.remove(position);
                        notifyItemRemoved(position);
                        notifyItemChanged(position, classjadwals);
                    } else {
                        alert("data gagal dihapus");
                    }
                }
            });
        } else if (halUser) {

            //MARK : function
            setNotificationText(brainJadwal.cekNotification(pid, idj), btnHapus);

            //MARK : trigger
            btnHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Boolean.valueOf(String.valueOf(btnHapus.getTag()))) {
                        if (brainJadwal.updateNotif(pid, idj, "0"))
                            setNotificationText(false, btnHapus);
                    } else {
                        if (brainJadwal.updateNotif(pid, idj, "1"))
                            setNotificationText(true, btnHapus);
                    }
                }
            });
        } else {
            btnHapus.setVisibility(View.GONE);
        }
        //endregion

        //region btnReminder
        //MARK : UI
        final ImageButton btnReminder = view.findViewById(R.id.btnReminder);

        //MARK : operation
        if (masjid)
            btnReminder.setVisibility(View.INVISIBLE);
        else if (halUser) {

            //MARK : operation
            btnReminder.setBackground(context.getResources().getDrawable(R.color.red));

            //MARK : trigger
            btnReminder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (brainJadwal.unreminder(pid, idj)) {
                        alert("kegiatan rutin dihapus dari jadwal");
                        classjadwals.remove(position);
                        notifyItemRemoved(position);
                        notifyItemChanged(position, classjadwals);
                    } else
                        alert("terjadi kesalahan, tidak dapat terhubung ke server");
                }
            });
        } else {
            if (brainJadwal.cekReminder(pid, idj)) {
                btnReminder.setEnabled(false);
                btnReminder.setBackground(context.getResources().getDrawable(R.color.red));
                btnReminder.setVisibility(View.GONE);
            } else {

                //MARK : trigger
                btnReminder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (brainJadwal.followJadwal(pid, idj)) {
                            alert("reminder telah di set untuk acara ini");
                            btnReminder.setEnabled(false);
//                            btnReminder.setText("REMINDERED");
                            btnReminder.setBackground(context.getResources().getDrawable(R.color.red));
                            btnReminder.setVisibility(View.GONE);
                        } else {
                            alert("gagal menyetel reminder");
                        }
                    }
                });
            }
        }

        //endregion

        //region Button Detail
        ImageButton btnDetail = view.findViewById(R.id.btnDetail);
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, activityDetailJadwal.class);
                intent.putExtra("jadwalId", classjadwal.getId());
                intent.putExtra("halUser", halUser);
                intent.putExtra("masjid", masjid);
                intent.putExtra("profilId", pid);
                intent.putExtra("masjidId",mid);
                context.startActivity(intent);
            }
        });
        //endregion

        //region btnEdit
        //MARK : UI
        final ImageButton btnEdit = view.findViewById(R.id.btnEdit);

        //MARK : operation
        if (masjid) {

            //MARK : trigger
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //MARK : obj
                    final Intent intent = new Intent(context, activityTambahJadwal.class);

                    //MARK : operationS
                    intent.putExtra("idmasjid", classjadwal.getMasjid_id());
                    intent.putExtra("judul", classjadwal.getJudul());
                    intent.putExtra("ket", classjadwal.getKeterangan());
//                    intent.putExtra("hari", classjadwal.getHari());
//                    intent.putExtra("jam", jamString);
//                    intent.putExtra("menit", menitString);
                    intent.putExtra("edit", true);
                    intent.putExtra("idj", classjadwal.getId());
                    context.startActivity(intent);

                }
            });
        } else
            btnEdit.setVisibility(View.GONE);

        //endregion

        //region load Image Masjid
        //MARK : UI
        final ImageView imgMasjid = view.findViewById(R.id.imgMasjid);

        //MARK : obj
        final String urlImgMasjid = "http://103.52.146.34/mymasjid/Foto_Masjid/" + classjadwal.getMasjid_id() + ".jpg";

        //MARK : operation
        Glide.with(context).load(urlImgMasjid).thumbnail(1.0f).into(imgMasjid);
        //endregion

    }

    @Override
    public int getItemCount() {
        return classjadwals.size();
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }

    private void alert(String msg) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        //MARK : operation
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void setNotificationText(Boolean notification, ImageButton btnHapus) {
        if (notification) {
            btnHapus.setTag(true);
            btnHapus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_notifications_active_white_24dp));
            btnHapus.setBackground(context.getResources().getDrawable(R.color.yellow));
        } else {
            btnHapus.setTag(false);
            btnHapus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_notifications_off_black_24dp));
            btnHapus.setBackground(context.getResources().getDrawable(R.color.dark_yellow));
        }
    }
}
