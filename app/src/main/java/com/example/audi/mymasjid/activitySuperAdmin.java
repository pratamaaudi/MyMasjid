package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;

public class activitySuperAdmin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_admin);

        final Context context = this;

        //MARK : UI
        final RecyclerView rcl = findViewById(R.id.rcl);

        //MARK : obj
        ArrayList<classLaporSuperadmin>classLaporSuperadmins=new ArrayList<>();
        final adapterLaporSuperadmin adapterLaporSuperadmin = new adapterLaporSuperadmin(classLaporSuperadmins, this);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activitySuperAdmin.this);

        //MARK : operation
        rcl.setLayoutManager(linearLayoutManager);
        rcl.setHasFixedSize(true);
        rcl.setAdapter(adapterLaporSuperadmin);
        rcl.setNestedScrollingEnabled(true);

        final ImageButton btnLogOut = findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, activityLogin.class);
                context.startActivity(intent);
            }
        });
    }
}
