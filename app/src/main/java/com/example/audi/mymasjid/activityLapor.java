package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;

public class activityLapor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapor);

        //region Textview
        //MARK : UI
        final TextView txtTerlapor, txtKomen;

        //region txtAsal
        //MARK : UI
        txtTerlapor = findViewById(R.id.txtPelapor);

        //MARK : operation
        txtTerlapor.setText(getextra("username"));
        //endregion

        //region txtKomen
        //MARK : UI
        txtKomen = findViewById(R.id.txtMasjid);

        //MARK : operation
        txtKomen.setText(getextra("komen"));
        //endregion
        //endregion

        //region btnSubmit
        //koneksi dengan tombol simpan
        //MARK : UI
        final Button btnSubmit = findViewById(R.id.btnSubmit);

        //MARK : trigger
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //koneksi dengan editteks dan mengambil isi editteks
                //MARK : UI
                final EditText edtKeterangan = findViewById(R.id.edtKeterangan);
                final String keterangan = edtKeterangan.getText().toString();

                //cek kosong editteks
                //MARK : operation
                if (keterangan.equals(""))
                    alert("mohon mengisi keterangan laporan anda");
                else {
                    //munculkan loding
                    //MARK : obj
                    final ProgressDialog pDialog = new ProgressDialog(activityLapor.this);
                    pDialog.setMessage("Loading . . .");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(false);

                    //MARK ; operation
                    pDialog.show();

                    //tambahkan laporan ke database
                    new AsyncJob.AsyncJobBuilder<Boolean>();
                    AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
                        @Override
                        public void doOnBackground() {

                            //MARK : obj
                            final brainKomentar brainKomentar = new brainKomentar();
                            final Boolean result;

                            //MARK : operation
                            if (brainKomentar.laporKomentar(getextra("pid"), getextra("mid"), getextra("kid"), keterangan))
                                result = true;
                            else
                                result = false;

                            AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                                @Override
                                public void doInUIThread() {

                                    if (result) {
                                        alert("komentar berhasil dilaporkan");
                                        finish();
                                    }
                                    else
                                        alert("laporan gagal di kirim, periksa koneksi anda");
                                    pDialog.dismiss();

                                }
                            });
                        }
                    });
                }
            }
        });
        //endregion
    }

    //region function
    public String getextra(String key) {
        return getIntent().getExtras().getString(key);
    }

    private void alert(String msg) {

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityLapor.this);

        //MARK : operation
        builder.setMessage(msg).show();
    }
    //endregion
}
