package com.example.audi.mymasjid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 10/22/2017.
 */

public class adapterKegiatan extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //region set data
    final Context context;
    final ArrayList<classKegiatan> classKegiatans;
    final String uid;
    final Boolean masjid;

    public adapterKegiatan(Context context, ArrayList<classKegiatan> classKegiatans, String uid, Boolean masjid) {
        this.context = context;
        this.classKegiatans = classKegiatans;
        this.uid = uid;
        this.masjid = masjid;
    }
    //endregion

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_kegiatan, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //MARK : obj
        final View itemView = holder.itemView;
        final classKegiatan classKegiatan = classKegiatans.get(position);
        final brainKegiatan brainKegiatan = new brainKegiatan();
        final Button btnjmllike;
        final String idKegiatan = classKegiatan.getIdKegiatan();

        //region isi UI
        //MARK : UI
        final TextView txtJudulKegiatan, txtTanggalMulai, txtTanggalSelesai, txtJamMulai, txtJamSelesai, txtDeskripsi;
        //region Textview
        txtJudulKegiatan = itemView.findViewById(R.id.txtJudulKegiatan);
//        txtNamaMasjid = itemView.findViewById(R.id.txtNamaMasjid);
        txtTanggalMulai = itemView.findViewById(R.id.txtTanggalMulai);
        txtTanggalSelesai = itemView.findViewById(R.id.txtTanggalSelesai);
        txtJamMulai = itemView.findViewById(R.id.txtJamMulai);
        txtJamSelesai = itemView.findViewById(R.id.txtJamSelesai);
        txtDeskripsi = itemView.findViewById(R.id.txtDeskripsi);
        //endregion

        //MARK : operation
        txtJudulKegiatan.setText(classKegiatan.getNamaKegiatan());
//        txtNamaMasjid.setText(classKegiatan.getNamaMasjid());
        txtTanggalMulai.setText(classKegiatan.getTanggalMulai());
        txtTanggalSelesai.setText(classKegiatan.getTanggalSelesai());
        txtJamMulai.setText(classKegiatan.getJamMulai());
        txtJamSelesai.setText(classKegiatan.getJamSelesai());
        txtDeskripsi.setText(classKegiatan.getDeskripsi());
        //endregion

        //region set btnjmlfollowmasjid
        //MARK : UI
        final Button btnjmlfollowkegiatan = itemView.findViewById(R.id.btnfollowkegiatan);

        btnjmlfollowkegiatan.setText(brainKegiatan.loadjmlfollowkegiatan(idKegiatan));

        btnjmlfollowkegiatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bukaActPengikutKegiatan(idKegiatan);
            }
        });
        //endregion

        //region Button Jumlah Follow Kegiatan
        final ImageButton imgbtnjmlfollowkegiatan = itemView.findViewById(R.id.imgbtnfollowkegiatan);

        imgbtnjmlfollowkegiatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bukaActPengikutKegiatan(idKegiatan);
            }
        });
        //endregion

        //MARK : UI
        final ImageButton btnFollow, btnChat;
        //region btnFollow
        //MARK : UI
        btnFollow = itemView.findViewById(R.id.btnFollow);

        //MARK : trigger
        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //memeriksa jadwal tabrakan
                //MARK : obj
                final brainKegiatan brainKegiatan = new brainKegiatan();
                List<String> strings = new ArrayList<>();
                strings.clear();
                strings = brainKegiatan.cekJamTabrakan(classKegiatan.getTanggalMulai() + " " + classKegiatan.getJamMulai(), classKegiatan.getTanggalSelesai() + " " + classKegiatan.getJamSelesai(), uid);

                //jika ada yang tabrakan, keluarkan notifikasi
                //MARK : operation
                if (strings.size() > 0) {

                    //memproses respon user, tambah kegiatan jika user menyetujui peringatan
                    //MARK : trigger
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    tambahKegiatan(brainKegiatan, idKegiatan, btnFollow, btnjmlfollowkegiatan);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    //MARK : obj
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    //MARK : operation
                    builder.setMessage("Anda telah mengikuti kegiatan : \nNama Kegiatan : " + strings.get(0) + "\nTanggal Mulai : " + strings.get(1) + "\nTanggal Selesai : " + strings.get(2) + "\noleh masjid : " + strings.get(3) + "\n\ntetap menambahkan kegiatan ke jadwal anda ?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
                } else

                    //jika tidak ada yang tabrakan, tambahkan kegiatan
                    tambahKegiatan(brainKegiatan, idKegiatan, btnFollow, btnjmlfollowkegiatan);
            }
        });

        //MARK : operation
        if (brainKegiatan.cekFollowKegiatan(uid, idKegiatan))
            kegiatanFollowed(btnFollow);
        //endregion

        //region btnChat
        //MARK : UI
        btnChat = itemView.findViewById(R.id.btnChat);

        //MARK : trigger
        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Intent intent = new Intent(context, activityChat.class);

                //MARK : operation
                intent.putExtra("idp", uid);
                intent.putExtra("idm", classKegiatan.getIdMasjid());
                intent.putExtra("kegiatan", true);
                intent.putExtra("namakeg", classKegiatan.getNamaKegiatan());
                intent.putExtra("deskkeg", classKegiatan.getDeskripsi());
                intent.putExtra("mulai", classKegiatan.getTanggalMulai() + " " + classKegiatan.getJamMulai());
                intent.putExtra("selesai", classKegiatan.getTanggalSelesai() + " " + classKegiatan.getJamSelesai());
                intent.putExtra("masjid", false);
                intent.putExtra("subjek", classKegiatan.getNamaMasjid());
                context.startActivity(intent);
            }
        });
        //endregion

        //region img kegiatan
        //MARK : UI
        final ImageView img = itemView.findViewById(R.id.img);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/masjid/Foto_Kegiatan/" + idKegiatan + ".jpg";

        //MARK : operation
        Glide.with(context).load(url).thumbnail(1.0f).into(img);

        //MARK : trigger
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : OBJ
                final Intent i = new Intent(context, activityDetailGambar.class);

                //MARK : operation
                i.putExtra("url", url);
                context.startActivity(i);
            }
        });
        //endregion

        //region set btn jumlah like
        //MARK : UI
        btnjmllike = itemView.findViewById(R.id.btnjmllike);

        btnjmllike.setText(brainKegiatan.loadjmllikekegiatan(idKegiatan));
        //endregion

        //region btnLike
        //koneksi dengan tombol like
        //MARK : UI
        final ImageButton btnLike = itemView.findViewById(R.id.btnLike);

        //cek apakah kegiatan sudah di like ?
        //MARK : operation
        if (brainKegiatan.cekLike(uid, idKegiatan))
            ubahLike(btnLike, true);
        else
            ubahLike(btnLike, false);

        //MARK : trigger
        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //like jika belum di like
                if (Boolean.valueOf(String.valueOf(btnLike.getTag()))) {
                    if (brainKegiatan.like(uid, idKegiatan)) {
                        ubahLike(btnLike, true);
                        btnjmllike.setText(String.valueOf((Integer.valueOf(btnjmllike.getText().toString()) + 1)));
                    }
                }
                //unlike jika sudah di like
                else {
                    if (brainKegiatan.unlike(uid, idKegiatan)) {
                        ubahLike(btnLike, false);
                        btnjmllike.setText(String.valueOf((Integer.valueOf(btnjmllike.getText().toString()) - 1)));
                    }
                }
            }
        });
        //endregion

        //region load Image Masjid
        //MARK : obj
        final ImageView imgMasjid = itemView.findViewById(R.id.imgMasjid);

        //MARK : obj
        final String urlImgMasjid = "http://103.52.146.34/mymasjid/Foto_Masjid/"+classKegiatan.getIdMasjid()+".jpg";

        //MARK : operation
        Glide.with(context).load(urlImgMasjid).thumbnail(1.0f).into(imgMasjid);
        //endregion

        //region Button Komen
        ImageButton btnKomen = itemView.findViewById(R.id.btnKomen);
        btnKomen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(context, activityDetailKegiatan.class);
                intent.putExtra("masjidId", classKegiatan.getIdMasjid());
                intent.putExtra("namaMasjid",classKegiatan.getNamaMasjid());
                intent.putExtra("deskripsi",classKegiatan.getDeskripsi());
                intent.putExtra("idKegiatan",classKegiatan.getIdKegiatan());
                intent.putExtra("idUser",uid);
                context.startActivity(intent);
            }
        });
        //endregion
    }

    @Override
    public int getItemCount() {
        return classKegiatans.size();
    }

    private void tambahKegiatan(brainKegiatan brainKegiatan, String idKegiatan, ImageButton btnFollow, Button btnjmlfollowkegiatan) {

        //mempersiapkan dialog
        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //menambahkan kegiatan dan memunculkan notifikasi
        //MARK : operation
        if (brainKegiatan.ikutKegiatan(uid, idKegiatan)) {
            builder.setMessage("anda berhasil mengikuti kegiatan tersebut");
            kegiatanFollowed(btnFollow);
            btnjmlfollowkegiatan.setText(String.valueOf((Integer.valueOf(btnjmlfollowkegiatan.getText().toString()) + 1)));
        } else
            builder.setMessage("anda sudah mengikuti kegiatan tersebut");

        builder.show();
    }

    private void ubahLike(ImageButton btnLike, Boolean like) {

        //MARK : operation
        if (like) {
            btnLike.setTag(false);
            btnLike.setBackground(context.getResources().getDrawable(R.color.green));
        } else {
            btnLike.setTag(true);
            btnLike.setBackground(context.getResources().getDrawable(R.color.blue));
        }

    }

    private void kegiatanFollowed(ImageButton btnFollow) {
        //MARK : obj
        final Resources resources = context.getResources();

        //MARK : operation
        btnFollow.setBackground(resources.getDrawable(R.color.green));
        btnFollow.setImageDrawable(resources.getDrawable(R.drawable.ic_check_white_24dp));

//        btnFollow.setOnClickListener(null);
    }

    private void bukaActPengikutKegiatan(String idKegiatan){
        Intent intent = new Intent(context, activityPengikutKegiatan.class);
        intent.putExtra("idk", idKegiatan);
        context.startActivity(intent);
    }
}
