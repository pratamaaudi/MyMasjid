package com.example.audi.mymasjid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by prata on 9/22/2017.
 */

public class recyclerAdapterJadwal extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<classJadwal_old> arraylistClassJadwalOld;

    public recyclerAdapterJadwal(Context context, ArrayList<classJadwal_old> arraylistClassJadwalOld) {
        setContext(context);
        setArraylistClassJadwalOld(arraylistClassJadwalOld);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.
                from(parent.getContext())
                .inflate(
                        R.layout.layout_jadwal_jumatan,
                        parent, false
                );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return getArraylistClassJadwalOld().size();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<classJadwal_old> getArraylistClassJadwalOld() {
        return arraylistClassJadwalOld;
    }

    public void setArraylistClassJadwalOld(ArrayList<classJadwal_old> arraylistClassJadwalOld) {
        this.arraylistClassJadwalOld = arraylistClassJadwalOld;
    }
}
