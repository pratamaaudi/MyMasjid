package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class brainLapor {

    classIpAddress classIpAddress = new classIpAddress();

    public ArrayList<classLapor> loadLapor(String pid, ArrayList<classLapor> classLapors) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadlaporuser.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("pid", pid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String idMasjid = c.getString("masjid_id");
                String idprofil = c.getString("profil_id");
                String keterangan = c.getString("keterangan");
                String status = c.getString("status");
                String pelapor = c.getString("pelapor");
                String terlapor = c.getString("terlapor");
                String komentar = c.getString("komentar");
                String idterlapor = c.getString("idterlapor");
                String idkomen = c.getString("idkomen");
                String hapus_komentar = c.getString("hapus");
                final classKomentar classKomentar = new classKomentar(terlapor, komentar, idterlapor, idkomen);
                classLapors.add(new classLapor(id,idMasjid, idprofil, keterangan, status, pelapor, hapus_komentar, classKomentar));
            }

        } catch (Exception e) {
        }
        return classLapors;
    }

    public Boolean updateKeteranganLapor(String mid, String kid, String ket) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/updateketlapor.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("kid", kid.trim()));
            nameValuePairs.add(new BasicNameValuePair("ket", ket.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            Log.e("mymasjidlog", "respon : " + respon);

        } catch (Exception e) {
        }

        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<classLapor> loadLaporMasjid(String mid, ArrayList<classLapor> classLapors) {
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadlapormasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String idMasjid = c.getString("masjid_id");
                String idprofil = c.getString("profil_id");
                String keterangan = c.getString("keterangan");
                String status = c.getString("status");
                String pelapor = c.getString("pelapor");
                String terlapor = c.getString("terlapor");
                String komentar = c.getString("komentar");
                String idterlapor = c.getString("idterlapor");
                String idkomen = c.getString("idkomen");
                String hapus_komentar = c.getString("hapus");
                final classKomentar classKomentar = new classKomentar(terlapor, komentar, idterlapor, idkomen);
                classLapors.add(new classLapor(id, idMasjid, idprofil, keterangan, status, pelapor, hapus_komentar, classKomentar));
            }

        } catch (Exception e) {
        }
        return classLapors;
    }

    public Boolean updateStatLapor(String mid, String kid) {

        //mempersiapkan variabel
        String respon = "";

        //mempersiapkan koneksi ke webservice
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/updatestatlapor.php");
        try {

            //mempersiapkan kiriman data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("kid", kid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));

            //mengirim data dan menampung hasil
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }

        //memeriksa hasil dan mengembalikan hasil
        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean updateHapusKomentar(String idl) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/updatehapuskomentar.php");
        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idl", idl.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }

        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean inputLaporSuperadmin(String idp, String idm, String ket) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/inputlaporsuperadmin.php");
        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            nameValuePairs.add(new BasicNameValuePair("ket", ket.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }

        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean kirimNotifikasiSuperadmin(String mid, String kid) {
        String respon = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/updatestatlapor.php");
        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("kid", kid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }

        if (respon.equals("true")) {
            return true;
        } else {
            return false;
        }
    }
}
