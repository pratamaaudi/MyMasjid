package com.example.audi.mymasjid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Calendar;

public class activityLogin extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //MARK : semi glb obj
        new Prefs.Builder().setContext(activityLogin.this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();

        //region btnLogin
        //MARK : UI
        final Button btnLogin = findViewById(R.id.btnLogin);

        //MARK : trigger
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                new login().execute();

            }
        });
        //endregion

        //region btnDaftar
        //MARK : UI
        Button btnDaftar = findViewById(R.id.btnDaftar);

        //MARK : trigger
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : obj
                final Intent i = new Intent(getApplicationContext(), activityDaftar.class);

                //MARK : operation
                startActivity(i);
            }
        });
        //endregion

        //region internet setting
        //MARK : OBJ
        final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        //MARK : Operation
        StrictMode.setThreadPolicy(policy);
        //endregion

        //region cek sudah pernah login / belum
        //MARK : obj
        final String akun = Prefs.getString("akun", "");

        brainLog brainLog = new brainLog();
        brainLog.Log("","","");

        //MARK : operation
        if (!akun.equals("")) {

            //MARK : obj
            final Intent i;
            final Context context = getApplicationContext();
            final classLogin classLogin;

            //MARK : operation
            if (akun.equals("user")) {

                //MARK : obj
                classLogin = new classLogin(Prefs.getString("username",""),Prefs.getString("akun",""),Prefs.getString("id",""),Prefs.getString("email",""),Prefs.getString("akun",""),"",Prefs.getString("provinsi",""),Prefs.getString("kotakab",""),Prefs.getString("kecamatan",""));

                //MARK : operation
                i = new Intent(context, activityMain.class);
            } else {

                //MARK : obj
                final String idmasjid = Prefs.getString("idmasjid","");
               classLogin = new classLogin(Prefs.getString("username",""),Prefs.getString("akun",""),Prefs.getString("id",""),Prefs.getString("email",""),Prefs.getString("akun",""),idmasjid,Prefs.getString("provinsi",""),Prefs.getString("kotakab",""),Prefs.getString("kecamatan",""));

                //MARK : operation
                i = new Intent(context, activityDetailMasjid.class);
                i.putExtra("jenisAkun", "masjid");
                i.putExtra("id", idmasjid);
            }

            //MARK : obj
            i.putExtra("classLogin", classLogin);
            i.putExtra("autologin", true);

            //MARK : operation
            startActivity(i);
            finish();

        }
        //endregion
    }

    private class login extends AsyncTask<String, String, String> {

        //region login async
        //MARK : Obj
        private ProgressDialog pDialog;
        classLogin classLogin = null;

        //MARK : Operation
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //region show loading
            //MARK : obj
            pDialog = new ProgressDialog(activityLogin.this);

            //MARK : operation
            pDialog.setMessage("Checking . . .");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            //endregion
        }

        @Override
        protected String doInBackground(String... data) {

            //MARK : UI
            TextView txtUsername, txtPass;
            txtUsername = findViewById(R.id.txtUsername);
            txtPass = findViewById(R.id.txtPass);

            //MARK : obj
            final String username, password;
            username = txtUsername.getText().toString();
            password = txtPass.getText().toString();
            final brainLogin brainLogin = new brainLogin();

            //MARK : operation
            classLogin = brainLogin.login(username, password);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //MARK : operation
            pDialog.dismiss();
            if (classLogin != null) {

                //region tentukan akun user / masjid
                //MARK : obj
                final Intent i;
                final Context context = getApplicationContext();
                final String tipe = classLogin.getJenis();

                //MARK : operation
                if (tipe.equals("user")) {

                    //region set user ke activity main
                    //MARK : Obj
                    i = new Intent(context, activityMain.class);
                    //endregion

                    Prefs.putString("akun", tipe);
                    Prefs.putString("username", classLogin.getUsername());
                    Prefs.putString("email", classLogin.getEmail());
                    Prefs.putString("id", classLogin.getId());
                    Prefs.putString("provinsi", classLogin.getProvinsi());
                    Prefs.putString("koakab", classLogin.getKotakab());
                    Prefs.putString("kecamatan", classLogin.getKecamatan());

                } else if(tipe.equals("masjid")) {

                    //region set user ke detail masjid
                    //MARK : obj
                    i = new Intent(context, activityDetailMasjid.class);
                    final String idMasjid = classLogin.getIdMasjid();
                    i.putExtra("jenisAkun", "masjid");
                    i.putExtra("id", idMasjid);
                    //endregion

                    Prefs.putString("akun", tipe);
                    Prefs.putString("username", classLogin.getUsername());
                    Prefs.putString("email", classLogin.getEmail());
                    Prefs.putString("id", classLogin.getId());
                    Prefs.putString("idmasjid", classLogin.getIdMasjid());
                    Prefs.putString("provinsi", classLogin.getProvinsi());
                    Prefs.putString("koakab", classLogin.getKotakab());
                    Prefs.putString("kecamatan", classLogin.getKecamatan());
                }else{
                    i = new Intent(context, activitySuperAdmin.class);
                }


                //region Pindah activity
                //MARK : obj
                final brainLogin brainLogin = new brainLogin();

                //MARK : operation
                brainLogin.updateToken(FirebaseInstanceId.getInstance().getToken(), classLogin.getId());
                i.putExtra("classLogin", classLogin);
                startActivity(i);
                finish();
                //endregion
                //endregion

            } else {
                //region set error di UI
                //MARK : UI
                TextView txtgagallogin = findViewById(R.id.txtgagallogin);

                //MARK : operation
                txtgagallogin.setVisibility(View.VISIBLE);
                pDialog.dismiss();
                //endregion
            }
        }
        //endregion
    }

    @Override
    public void onBackPressed() {

        //MARK : trigger
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        //MARK : function
                        end();

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityLogin.this);

        //MARK : operation
        builder.setMessage("keluar dari aplikasi ?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    public void end() {
        this.finishAffinity();
    }

    public void setAlarm() {
        final Context context = activityLogin.this;
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Intent intent = new Intent(context, alarmReciever.class);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 40);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

        Log.e("mymasjidlog", "alarm dibuat");
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
}
