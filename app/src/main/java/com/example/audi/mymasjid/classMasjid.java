package com.example.audi.mymasjid;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MOFOSEFES on 10/18/2017.
 */

public class classMasjid implements Parcelable {
    private String id;
    private String nama;
    private String alamat;
    private String kontak;

    public classMasjid(String id, String nama, String alamat, String kontak) {
        setId(id);
        setNama(nama);
        setAlamat(alamat);
        setKontak(kontak);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.nama);
        dest.writeString(this.alamat);
        dest.writeString(this.kontak);
    }

    protected classMasjid(Parcel in) {
        this.id = in.readString();
        this.nama = in.readString();
        this.alamat = in.readString();
        this.kontak = in.readString();
    }

    public static final Parcelable.Creator<classMasjid> CREATOR = new Parcelable.Creator<classMasjid>() {
        @Override
        public classMasjid createFromParcel(Parcel source) {
            return new classMasjid(source);
        }

        @Override
        public classMasjid[] newArray(int size) {
            return new classMasjid[size];
        }
    };
}
