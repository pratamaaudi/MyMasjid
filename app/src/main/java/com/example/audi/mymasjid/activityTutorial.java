package com.example.audi.mymasjid;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.cleveroad.slidingtutorial.Direction;
import com.cleveroad.slidingtutorial.PageOptions;
import com.cleveroad.slidingtutorial.TransformItem;
import com.cleveroad.slidingtutorial.TutorialOptions;
import com.cleveroad.slidingtutorial.TutorialPageOptionsProvider;
import com.cleveroad.slidingtutorial.TutorialSupportFragment;

public class activityTutorial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        ViewPager vp = (ViewPager) findViewById(R.id.viewPager);

        final TutorialPageOptionsProvider tutorialPageOptionsProvider = new TutorialPageOptionsProvider() {
            @NonNull
            @Override
            public PageOptions provide(int position) {
                @LayoutRes int pageLayoutResId;
                TransformItem[] tutorialItems;
                switch (position) {
                    case 0: {
                        pageLayoutResId = R.layout.fragment_tutorial_2;
                        tutorialItems = new TransformItem[]{
                                TransformItem.create(R.id.imgChat1, Direction.LEFT_TO_RIGHT, 0.5f)
                        };
                        break;
                    }
                    case 1: {
                        pageLayoutResId = R.layout.fragment_tutorial_2;
                        tutorialItems = new TransformItem[]{
                                TransformItem.create(R.id.imgChat1, Direction.LEFT_TO_RIGHT, 0.5f)
                        };
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException("Unknown position: " + position);
                    }
                }

                return PageOptions.create(pageLayoutResId, position, tutorialItems);
            }
        };

        final TutorialOptions tutorialOptions = TutorialSupportFragment
                .newTutorialOptionsBuilder(activityTutorial.this)
                .setPagesCount(2)
                .setTutorialPageProvider(tutorialPageOptionsProvider)
                .setUseAutoRemoveTutorialFragment(true)
                .build();

        final TutorialSupportFragment tutorialFragment = TutorialSupportFragment.newInstance(tutorialOptions);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, tutorialFragment)
                .commit();

        //vp.setAdapter(adapterPager);
    }
}
