package com.example.audi.mymasjid;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class activityListChat extends AppCompatActivity {

    Boolean onCreate;
    Boolean masjid;
    String idp;
    String idm;
    ArrayList<classRoom> classRooms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_chat);

        //MARK : global
        onCreate = true;
        final Bundle bundle = getIntent().getExtras();
        masjid = bundle.getBoolean("masjid");

        //region set idp & classroms
        //MARK : obj
        final brainChat brainChat = new brainChat();

        //MARK : operation
        if (masjid) {

            //MARK : obj
            idp = bundle.getString("idpm");
            idm = bundle.getString("idm");

            //MARK : operation
            classRooms = brainChat.loadRoomMasjid(idm);

        } else {

            //MARK : obj
            idp = bundle.getString("idp");

            //MARK : operation
            classRooms = brainChat.loadRoomUser(idp);
        }
        //endregion

        //region generate UI
        //MARK : UI
        final RecyclerView rclListMasjid = findViewById(R.id.rclListChat);

        //MARK : obj
        final Context context = activityListChat.this;
        adapterListChat adapterListChat = new adapterListChat(classRooms, idp, context, masjid);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        //MARK : operation
        rclListMasjid.setLayoutManager(linearLayoutManager);
        rclListMasjid.setHasFixedSize(true);
        rclListMasjid.setAdapter(adapterListChat);
        rclListMasjid.setNestedScrollingEnabled(true);
        //endregion

        //region constrantnokonten
        //MARK : UI
        final ConstraintLayout constrantnokonten = findViewById(R.id.constrantnokonten);

        //MARK : operation
        if (classRooms.size() < 1)
            constrantnokonten.setVisibility(View.VISIBLE);
        else
            constrantnokonten.setVisibility(View.INVISIBLE);

        //endregion
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!onCreate) {

            //MARK : obj
            final brainChat brainChat = new brainChat();

            classRooms.clear();
            if (masjid) {
                classRooms = brainChat.loadRoomMasjid(idm);
            } else {
                classRooms = brainChat.loadRoomUser(idp);
            }

            adapterListChat adapterListChat = new adapterListChat(classRooms, idp, activityListChat.this, masjid);

            final RecyclerView rclListMasjid = findViewById(R.id.rclListChat);
            rclListMasjid.setAdapter(adapterListChat);
            rclListMasjid.invalidate();

        } else
            onCreate = false;
    }
}
