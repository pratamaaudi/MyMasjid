package com.example.audi.mymasjid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class adapterDetailJadwal extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    Boolean halUser, masjid;
    ArrayList<classDetailJadwal> classDetailJadwals;

    public adapterDetailJadwal(Context context, ArrayList<classDetailJadwal> classDetailJadwals, Boolean halUser, Boolean masjid){
        this.context = context;
        this.classDetailJadwals = classDetailJadwals;
        this.halUser = halUser;
        this.masjid = masjid;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //MARK : obj
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_detail_jadwal, parent, false);
        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };

        //MARK : operation
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        View view = holder.itemView;
        final classDetailJadwal classDetailJadwal = classDetailJadwals.get(position);

        //region Set Teks Hari
        TextView txtHari = view.findViewById(R.id.txtHari);
        txtHari.setText(classDetailJadwal.getHari());
        //endregion

        //region Convert Jam Menit
        String jam = classDetailJadwal.getJam();
        if(Integer.parseInt(jam)<10){
            jam = "0"+jam;
        }
        String menit = classDetailJadwal.getMenit();
        if(Integer.parseInt(menit)<10){
            menit = "0"+menit;
        }
        //endregion

        //region Set Teks Jam
        TextView txtJam = view.findViewById(R.id.txtJam);
        txtJam.setText(jam+":"+menit);
        //endregion

        //region ImageButton Hapus
        ImageButton ibtnHapus = view.findViewById(R.id.ibtnHapus);
        ibtnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brainJadwal brainJadwal = new brainJadwal();
                brainJadwal.hapusDetailJadwal(classDetailJadwal.getId());
            }
        });
        //endregion

        //region Jika Diakses User
        if(halUser || !masjid){
            ibtnHapus.setVisibility(View.GONE);
        }
        //endregion
    }

    @Override
    public int getItemCount() {
        return classDetailJadwals.size();
    }
}
