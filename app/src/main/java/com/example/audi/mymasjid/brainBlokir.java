package com.example.audi.mymasjid;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class brainBlokir {

    classIpAddress classIpAddress = new classIpAddress();

    public Boolean blokir(String pid, String mid) {

        //mempersiapkan variabel
        //MARK : obj
        Boolean hasil = false;

        //mempersiapkan koneksi ke webservice
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/inputblokir.php");

        //MARK : operation
        try {

            //memperisapkan kiriman data
            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("pid", pid.trim()));
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            //mengirim data dan menampung hasil
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            //MARK : operation
            if (respon.equals("true"))
                hasil = true;
            //endregion

        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public ArrayList<classLogin> loadBlokir(String mid, ArrayList<classLogin> classLogins) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadblokir.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");
            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String nama = c.getString("nama");
                String email = c.getString("email");
                String jenis = c.getString("jenis");
                //MARK : masjid_id bakalan NULL kalau yang login bertipe user
                String idMasjid = c.getString("masjid_id");
                String provinsi = c.getString("provinsi");
                String kotakab = c.getString("kotakab");
                String kecamatan = c.getString("kecamatan");
                classLogins.add(new classLogin(nama, jenis, id, email, jenis, idMasjid, provinsi, kotakab, kecamatan));
            }
        } catch (Exception e) {
        }

        return classLogins;
    }

    public Boolean unblokir(String mid, String pid) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/hapusblokir.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            nameValuePairs.add(new BasicNameValuePair("pid", pid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }

        return hasil;
    }

    public Boolean cekBlokir(String idp, String idm) {

        //MARK : obj
        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/cekblokir.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
            }
            //endregion

        } catch (Exception e) {
        }

        return hasil;
    }

}
