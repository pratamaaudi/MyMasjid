package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class activityJadwal extends AppCompatActivity {

    Boolean onCreate;
    Boolean haluser;
    Boolean masjid;
    String idp;
    String mid;
    ArrayList<classjadwal> classjadwals;
    RecyclerView rclKont;
    classLogin classLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal);

        //MARK : semi glb obj
        classjadwals = new ArrayList<>();
        final Context context = activityJadwal.this;
        final Bundle bundle = getIntent().getExtras();
        masjid = bundle.getBoolean("masjid");
        haluser = bundle.getBoolean("haluser");
        idp = bundle.getString("idp");

        //region isi arraylist
        //MARK : obj
        mid = bundle.getString("idmasjid");
//        final brainJadwal brainJadwal = new brainJadwal();
//
//        //MARK : operation
//        if (haluser)
//            brainJadwal.loadJadwalUser(idp, classjadwals);
//        else
//            brainJadwal.loadJadwal(mid, classjadwals);
        refreshData();
        //endregion

        //region set Spinner Masjid
        //MARK : UI
        final MaterialSpinner spnMasjid = findViewById(R.id.spnMasjid);

        //MARK : operation
        if (haluser) {
            //MARK : obj
            final brainJadwal brainJadwal = new brainJadwal();
            final List<String> strings = brainJadwal.loadDaftarMasjidJadwalUser(idp);

            //MARK : operation
            spnMasjid.setItems(strings);
            if (strings.size() > 1)
                spnMasjid.setVisibility(View.VISIBLE);

            //MARK : trigger
            spnMasjid.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                    //MARK : operation
                    if (position > 0) {

                        //MARK : operation
                        classjadwals.clear();

                        //region refresh data
                        //MARK : obj
                        final brainJadwal brainJadwal1 = new brainJadwal();
                        classjadwals = brainJadwal.loadJadwalUserbyMasjid(idp, strings.get(position), classjadwals);

                        //MARK : operation
                        refreshRecycler();
                        //endregion
                    } else {
                        refreshData();
                        refreshRecycler();
                    }
                }
            });
        } else
            spnMasjid.setVisibility(View.GONE);

        //endregion

        //region set recyclerview
        //MARK : obj
        classLogin = bundle.getParcelable("classlogin");
        final adapterJadwal adapterJadwal = new adapterJadwal(context, classjadwals, masjid, idp, haluser, classLogin, mid);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        //MARK : UI
        rclKont = findViewById(R.id.rclKont);

        //MARK : operation
        rclKont.setLayoutManager(linearLayoutManager);
        rclKont.setHasFixedSize(true);
        rclKont.setAdapter(adapterJadwal);
        rclKont.setNestedScrollingEnabled(true);
        //endregion

        cekNoKonten();

        //region set menu sesuai masjid / user
        //MARK : UI
        final FloatingActionButton fab = findViewById(R.id.fab);
        final TextView txtJudul = findViewById(R.id.txtjudul);

        //MARK : operation
        if (!masjid) {

            //MARK : operation
            fab.setVisibility(View.INVISIBLE);
            txtJudul.setText("Pengingat");
        } else {

            //MARK : trigger
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //MARK : obj
                    final Intent intent = new Intent(context, activityTambahJadwal.class);
                    intent.putExtra("idmasjid", bundle.getString("idmasjid"));

                    //MARK : operation
                    startActivity(intent);
                }
            });
        }
        //endregion

        onCreate = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!onCreate) {
            refreshData();
            refreshRecycler();
        } else
            onCreate = false;
    }

    private void refreshData() {

        //mempersiapkan variabel
        //MARK : obj
        final brainJadwal brainJadwal = new brainJadwal();

        //mengosongkan data
        //MARK : operation
        classjadwals.clear();

        //memuat data sesuai pengakses
        if (haluser)
            brainJadwal.loadJadwalUser(idp, classjadwals);
        else
            brainJadwal.loadJadwal(mid, classjadwals);
        if (!masjid)
            if (classjadwals.size() > 0)
                brainJadwal.updateUnreadKegiatanRutin(mid, idp, classjadwals.get(0).getId());
    }

    private void refreshRecycler() {
        final adapterJadwal adapterJadwal = new adapterJadwal(activityJadwal.this, classjadwals, masjid, idp, haluser, classLogin, mid);
        rclKont.setAdapter(adapterJadwal);
        rclKont.invalidate();
        cekNoKonten();
    }

    private void cekNoKonten() {
        brainLog brainLog = new brainLog();
        brainLog.Log("actjadwal", "classjadwals", String.valueOf(classjadwals.size()));
        if (classjadwals.size() < 1) {
            //region constrantnokonten
            //MARK : UI
            final ConstraintLayout constrantnokonten = findViewById(R.id.constrantnokonten);

            //MARK : operation
            if (classjadwals.size() < 1)
                if (masjid) {

                    //region txtnokonten
                    //MARK : obj
                    final TextView txtnokonten = findViewById(R.id.txtnokonten);

                    //MARK : operation
                    txtnokonten.setText("Tidak ada data jadwal rutin masjid");
                    //endregion

                    constrantnokonten.setVisibility(View.VISIBLE);
                } else
                    constrantnokonten.setVisibility(View.VISIBLE);

            //endregion
        }
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }
}
