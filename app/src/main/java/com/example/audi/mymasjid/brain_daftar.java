package com.example.audi.mymasjid;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 10/9/2017.
 */

public class brain_daftar {

    classIpAddress classIpAddress = new classIpAddress();

    public boolean cekemail(String email) {
        boolean hasil = false;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/cekemail.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList();
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if(respon.equals("true")){
                hasil=true;
            }

        } catch (Exception e) {
        }

        return hasil;
    }

    public boolean daftar_user(String nama, String email, String password){
        boolean hasil = false;

        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/daftar.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("nama", nama.trim()));
            nameValuePairs.add(new BasicNameValuePair("email", email.trim()));
            nameValuePairs.add(new BasicNameValuePair("password", password.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());

            if(respon.equals("true")){
                hasil=true;
            }

        } catch (Exception e) {
        }

        return hasil;
    }
}
