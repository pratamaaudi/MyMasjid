package com.example.audi.mymasjid;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentKegiatan extends Fragment {

    ArrayList<classKegiatan> classKegiatans;
    View view;
    String uid;
    Boolean oncreate;
    Boolean masjid;

    public fragmentKegiatan() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        masjid = this.getArguments().getBoolean("masjid", false);

        //MARK : global obj
        classKegiatans = new ArrayList<>();
        view = inflater.inflate(R.layout.fragment_fragment_kegiatan, container, false);

        //MARK : obj
        uid = this.getArguments().getString("uid");
        final adapterKegiatan adapterKegiatan = new adapterKegiatan(getContext(), classKegiatans, uid, masjid);

        //MARK : function
        new loadKegiatan(adapterKegiatan).execute(uid);

        oncreate = true;

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (oncreate) {
            oncreate = false;
        } else {
            final adapterKegiatan adapterKegiatan = new adapterKegiatan(getContext(), classKegiatans, uid, masjid);
            classKegiatans.clear();
            new loadKegiatan(adapterKegiatan).execute(uid);
        }
    }

    private class loadKegiatan extends AsyncTask<String, String, String> {

        //region set data
        //MARK : obj
        final adapterKegiatan adapterKegiatan;

        public loadKegiatan(adapterKegiatan adapterKegiatan) {
            this.adapterKegiatan = adapterKegiatan;
        }
        //endregion

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... data) {

            //MARK : obj
            final brainKegiatan brainKegiatan = new brainKegiatan();

            //MARK : operation
            classKegiatans = brainKegiatan.loadKegiatan(data[0], classKegiatans);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //MARK : UI
            final RecyclerView rclKegiatan = view.findViewById(R.id.rclKegiatan);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
            final ProgressBar loading = view.findViewById(R.id.loading);

            //MARK : operation
            rclKegiatan.setLayoutManager(linearLayoutManager);
            rclKegiatan.setHasFixedSize(true);
            rclKegiatan.setAdapter(adapterKegiatan);
            loading.setVisibility(View.INVISIBLE);

            //region constrantnokonten
            //MARK : UI
            final ConstraintLayout constrantnokonten = view.findViewById(R.id.constrantnokonten);

            //MARK : operation
            if (classKegiatans.size() < 1)
                constrantnokonten.setVisibility(View.VISIBLE);
            else
                constrantnokonten.setVisibility(View.INVISIBLE);

            //endregion
        }
    }

}
