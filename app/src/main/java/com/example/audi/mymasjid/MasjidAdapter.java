package com.example.audi.mymasjid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Audi on 8/9/2017.
 */

public class MasjidAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    Integer[] i;

    public MasjidAdapter(
            Context context,
            Integer[] i
    ) {
        this.context = context;
        this.i = i;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(
            ViewGroup parent,
            int viewType
    ) {

        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(
                        R.layout.masjidlist_layout,
                        parent,
                        false
                );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(
            RecyclerView.ViewHolder holder,
            int position
    ) {

    }

    @Override
    public int getItemCount() {
        return i.length;
    }
}
