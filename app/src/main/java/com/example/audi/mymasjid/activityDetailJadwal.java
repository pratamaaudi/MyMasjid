package com.example.audi.mymasjid;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Calendar;

public class activityDetailJadwal extends AppCompatActivity {

    String hari, jamString, menitString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jadwal);

        final String jadwalId = getIntent().getStringExtra("jadwalId");
        final Context context = activityDetailJadwal.this;
        final Boolean halUser = getIntent().getBooleanExtra("halUser", false);
        final Boolean masjid = getIntent().getBooleanExtra("masjid", false);
        final String profilId = getIntent().getStringExtra("profilId");
        final String masjidId = getIntent().getStringExtra("masjidId");

        //region spinner hari
        //MARK : UI
        final MaterialSpinner spnHari = findViewById(R.id.spnHari);

        //MARK : operation
        spnHari.setItems("Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu");
        spnHari.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                switch (position) {
                    case 0:
                        hari = "Monday";
                        break;
                    case 1:
                        hari = "Tuesday";
                        break;
                    case 2:
                        hari = "Wednesday";
                        break;
                    case 3:
                        hari = "Thursday";
                        break;
                    case 4:
                        hari = "Friday";
                        break;
                    case 5:
                        hari = "Saturday";
                        break;
                    case 6:
                        hari = "Sunday";
                        break;
                }
            }
        });
        //endregion

        //region TXT pilih jam
        //        MARK : UI
        final TextView txtPilihJam = findViewById(R.id.txtJam);

        //MARK : trigger
        txtPilihJam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MARK : obj
                final Calendar calendar = Calendar.getInstance();
                final Integer jam = calendar.get(Calendar.HOUR_OF_DAY);
                final Integer menit = calendar.get(Calendar.MINUTE);
                final TimePickerDialog timePickerDialog = new TimePickerDialog(activityDetailJadwal.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        //MARK: operation
                        if (hourOfDay < 10) {
                            jamString = "0" + String.valueOf(hourOfDay);
                        } else {
                            jamString = String.valueOf(hourOfDay);
                        }

                        if (minute < 10) {
                            menitString = "0" + String.valueOf(minute);
                        } else {
                            menitString = String.valueOf(minute);
                        }
                        txtPilihJam.setText("Jam : " + jamString + ":" + menitString);
                    }
                }, jam, menit, true);

                //MARK : operation
                timePickerDialog.show();

            }
        });
        //endregion

        //region Image Button Tambah Jam
        ImageButton ibtnTambahJam = findViewById(R.id.ibtnTambahJam);

        ibtnTambahJam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brainJadwal brainJadwal = new brainJadwal();
                if(brainJadwal.tambahDetailJadwal(jadwalId,"2018-01-01 "+jamString+":"+menitString,hari)){
                    txtPilihJam.setText("Jam");
                    spnHari.setSelectedIndex(0);
                }
            }
        });
        //endregion

        //region Isi Array List Detail Jadwal
        brainJadwal brainJadwal = new brainJadwal();
        ArrayList<classDetailJadwal> classDetailJadwals = brainJadwal.loadDetailJadwalbyIdJadwal(jadwalId);
        //endregion

        //region Set Recyclerview Detail Jadwal
        RecyclerView rclDetailJadwal = findViewById(R.id.rclDetailJadwal);
        rclDetailJadwal.setLayoutManager(new LinearLayoutManager(context));
        rclDetailJadwal.setHasFixedSize(true);
        rclDetailJadwal.setAdapter(new adapterDetailJadwal(context,classDetailJadwals, halUser, masjid));
        rclDetailJadwal.setNestedScrollingEnabled(true);
        //endregion

        //region Isi ArrayList Komentar
        brainKomentar brainKomentar = new brainKomentar();
        ArrayList<classKomentar> classKomentars = new ArrayList<>();
        brainKomentar.loadKomentarKegiatanRutin(jadwalId,classKomentars);
        //endregion

        //region Set Recyclerview Komen
        RecyclerView rclKomen = findViewById(R.id.rclKomentar);
        rclKomen.setLayoutManager(new LinearLayoutManager(context));
        rclKomen.setHasFixedSize(true);
        rclKomen.setAdapter(new adapterKomentar(context,classKomentars,profilId,masjidId,masjid));
        rclKomen.setNestedScrollingEnabled(true);
        //endregion

        //region Button Komen
        final EditText edtKomentar = findViewById(R.id.txtKomentar);
        ImageButton ibtnSend = findViewById(R.id.ibtnSend);
        ibtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brainKomentar brainKomentar = new brainKomentar();
                String idKomentarBaru = brainKomentar.komentarKegiatanRutin(edtKomentar.getText().toString(),profilId,jadwalId);
                edtKomentar.setText("");
            }
        });
        //endregion

        //region Jika diakses user
        ConstraintLayout cnsAdmin = findViewById(R.id.cnsAdmin);
        if(halUser || !masjid){
            txtPilihJam.setVisibility(View.GONE);
            spnHari.setVisibility(View.GONE);
            ibtnTambahJam.setVisibility(View.GONE);
            cnsAdmin.setVisibility(View.GONE);
        }
        //endregion
    }
}
