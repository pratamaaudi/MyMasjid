package com.example.audi.mymasjid;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prata on 10/7/2017.
 */

public class brainLogin {

    classIpAddress classIpAddress = new classIpAddress();

    public classLogin login(String user, String pass) {
        boolean hasil = false;
        classLogin classLogin = null;
        if (user.equals("")) {
            hasil = false;
        } else if (pass.equals("")) {
            hasil = false;
        } else {
            String respon;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/login.php");
            try {
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("username", user.trim()));
                nameValuePairs.add(new BasicNameValuePair("password", pass.trim()));
                httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
                HttpResponse response = httpClient.execute(httppost);
                respon = EntityUtils.toString(response.getEntity());

                brainLog brainLog = new brainLog();
                brainLog.Log("brainlogin","respon",respon);

                JSONObject json = new JSONObject(respon);
                JSONArray json2 = json.getJSONArray("post");
                for (int i = 0; i < json2.length(); i++) {
                    JSONObject c = json2.getJSONObject(i);
                    String id = c.getString("id");
                    String nama = c.getString("nama");
                    String email = c.getString("email");
                    String jenis = c.getString("jenis");
                    //MARK : masjid_id bakalan NULL kalau yang login bertipe user
                    String idMasjid = c.getString("masjid_id");
                    String provinsi = c.getString("provinsi");
                    String kotakab = c.getString("kotakab");
                    String kecamatan = c.getString("kecamatan");
                    classLogin = new classLogin(nama, jenis, id, email, jenis, idMasjid,provinsi,kotakab,kecamatan);
                }
            } catch (Exception e) {
            }
        }
        return classLogin;
    }

    public Boolean updateToken(String token, String pid){
        String respon="";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/updatetoken.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("fcm_token", token.trim()));
            nameValuePairs.add(new BasicNameValuePair("profil_id", pid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
        }

        if(respon.equals("true")){
            return true;
        }else{
            return false;
        }
    }
}
