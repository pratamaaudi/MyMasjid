package com.example.audi.mymasjid;

public class classjadwal {
    private String id;
    private String judul;
    private String keterangan;
//    private String jam;
//    private String menit;
//    private String hari;
    private String masjid_id;
    private String namaMasjid;

    public classjadwal(String id, String judul, String keterangan,String masjid_id, String namaMasjid){
        setId(id);
        setJudul(judul);
        setKeterangan(keterangan);
//        setJam(jam);
//        setMenit(menit);
//        setHari(hari);
        setMasjid_id(masjid_id);
        setNamaMasjid(namaMasjid);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getMasjid_id() {
        return masjid_id;
    }

    public void setMasjid_id(String masjid_id) {
        this.masjid_id = masjid_id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

//    public String getJam() {
//        return jam;
//    }
//
//    public void setJam(String jam) {
//        this.jam = jam;
//    }
//
//    public String getMenit() {
//        return menit;
//    }
//
//    public void setMenit(String menit) {
//        this.menit = menit;
//    }
//
//    public String getHari() {
//        return hari;
//    }
//
//    public void setHari(String hari) {
//        this.hari = hari;
//    }

    public String getNamaMasjid() {
        return namaMasjid;
    }

    public void setNamaMasjid(String namaMasjid) {
        this.namaMasjid = namaMasjid;
    }
}
