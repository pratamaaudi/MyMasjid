package com.example.audi.mymasjid;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.intentservice.chatui.models.ChatMessage;

public class brainChat {

    classIpAddress classIpAddress = new classIpAddress();

    public String loadIdRoom(String pid, String mid) {

        //MARK : obj
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadidroom.php");
        String idroom = "";

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("pid", pid.trim()));
            nameValuePairs.add(new BasicNameValuePair("mid", mid.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //region kirim post dan get hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //MARK : operation
            idroom = respon;

        } catch (Exception e) {
        }

        return idroom;
    }

    public Boolean inputChat(String chat, String time, String idpu, String idr, String token) {

        //MARK : obj
        // mempersiapkan variabel
        Boolean hasil = false;

        //mempersiapkan koneksi ke webservice
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/inputchat.php");

        //MARK : operation
        try {

            //memperisapkan kiriman data
            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("chat", chat.trim()));
            nameValuePairs.add(new BasicNameValuePair("time", time.trim()));
            nameValuePairs.add(new BasicNameValuePair("idp", idpu.trim()));
            nameValuePairs.add(new BasicNameValuePair("idr", idr.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //mengirim data dan menampung hasil
            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

            //memeriksa hasil
            //MARK : operation
            if (respon.equals("true")) {
                hasil = true;
                sendNotif(token, "Mymasjid Chat", chat);
            }

        } catch (Exception e) {
        }

        //mengembalikan hasil
        return hasil;
    }

    public ArrayList<ChatMessage> loadChat(String idp, String idr) {

        Log("idp : " + idp);
        Log("idr : " + idr);

        ArrayList<ChatMessage> chatMessages = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadchat.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idr", idr.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String chat = c.getString("chat");
                String time = c.getString("timestamp");
                String profil_id = c.getString("profil_id");
                if (profil_id.equals(idp))
                    chatMessages.add(new ChatMessage(chat, Long.parseLong(time), ChatMessage.Type.SENT));
                else
                    chatMessages.add(new ChatMessage(chat, Long.parseLong(time), ChatMessage.Type.RECEIVED));
            }

            Log("respon : " + respon);

        } catch (Exception e) {
        }
        return chatMessages;
    }

    public ArrayList<classRoom> loadRoomUser(String idp) {

        ArrayList<classRoom> classRooms = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loadroomuser.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idp", idp.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String counter = c.getString("counter");
                classRooms.add(new classRoom(id, counter));
            }

            Log("respon : " + respon);

        } catch (Exception e) {
        }
        return classRooms;
    }

    public ArrayList<String> loadDetailRoomUser(String idr) {

        Boolean hasil = false;
        ArrayList<String> strings = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/loaddetailroomuser.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idr", idr.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                strings.add(c.getString("profil_id"));
                strings.add(c.getString("chat"));
                strings.add(c.getString("nama"));
                strings.add(c.getString("id"));
                strings.add(c.getString("fcm_token"));
                hasil = true;
            }

            Log("respon : " + respon);

        } catch (Exception e) {
        }
        return strings;
    }

    public Boolean readRoom(String idr) {

//        Log("chat : "+chat);
//        Log("time : "+time);
//        Log("idp : "+idpu);
//        Log("idr : "+ idr);

        //MARK : obj
//        Log("read room");
//        Log("idr : "+idr);

        Boolean hasil = false;
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/readroom.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("idr", idr.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

//            Log("respon : "+ respon);

            //MARK : operation
            if (respon.equals("true"))
                hasil = true;

        } catch (Exception e) {
        }

        return hasil;
    }

    public ArrayList<classRoom> loadRoomMasjid(String idm) {

        ArrayList<classRoom> classRooms = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loadroommasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idm", idm.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                String id = c.getString("id");
                String counter = c.getString("counter");
                classRooms.add(new classRoom(id, counter));
            }

            Log("respon : " + respon);

        } catch (Exception e) {
        }
        return classRooms;
    }

    public ArrayList<String> loadDetailRoomMasjid(String idr) {

        Boolean hasil = false;
        ArrayList<String> strings = new ArrayList<>();
        String respon;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/masjid/loaddetailroommasjid.php");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idr", idr.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            HttpResponse response = httpClient.execute(httppost);
            respon = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(respon);
            JSONArray json2 = json.getJSONArray("post");

            for (int i = 0; i < json2.length(); i++) {
                JSONObject c = json2.getJSONObject(i);
                strings.add(c.getString("profil_id"));
                strings.add(c.getString("chat"));
                strings.add(c.getString("nama"));
                strings.add(c.getString("id"));
                strings.add(c.getString("fcm_token"));
                hasil = true;
            }

            Log("respon : " + respon);

        } catch (Exception e) {
        }
        return strings;
    }

    public void sendNotif(String token, String title, String msg) {

//        Log("send notif");
//        Log("token : "+token);
//        Log("title : "+title);
//        Log("msg : "+msg);

        //MARK : obj
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpPost httppost = new HttpPost("http://"+classIpAddress.getIp()+"/mymasjid/notification.php");

        //MARK : operation
        try {

            //region set POST
            //MARK : obj
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            //MARK : operation
            nameValuePairs.add(new BasicNameValuePair("key", token.trim()));
            nameValuePairs.add(new BasicNameValuePair("title", title.trim()));
            nameValuePairs.add(new BasicNameValuePair("msg", msg.trim()));
            httppost.setEntity(new UrlEncodedFormEntity((nameValuePairs)));
            //endregion

            //MARK : obj
            final HttpResponse response = httpClient.execute(httppost);
            final String respon = EntityUtils.toString(response.getEntity());

        } catch (Exception e) {
        }
    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }

}
