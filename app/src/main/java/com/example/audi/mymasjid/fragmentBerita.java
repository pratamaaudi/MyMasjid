package com.example.audi.mymasjid;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arasthel.asyncjob.AsyncJob;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentBerita extends android.support.v4.app.Fragment {

    public fragmentBerita() {
        // Required empty public constructor
    }

    Boolean oncreate;
    ArrayList<classPosting> arrayListPosting;
    classLogin classLogin;
    View view;
    Context context;
    Boolean masjid;
    int lastFirstVisiblePosition;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_berita, container, false);

        //MARK : obj
        final Bundle bundle = this.getArguments();
        context = view.getContext();

        //MARK : operation
        new AsyncJob.AsyncJobBuilder<Boolean>();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {

                //region Load posting
                //MARK : obj
                classLogin = bundle.getParcelable("classLogin");
                arrayListPosting = new ArrayList<>();
                masjid = bundle.getBoolean("masjid");
                final adapterPosting adapterPosting = new adapterPosting(context, arrayListPosting, classLogin, masjid, false);
                final brainBerita brain = new brainBerita();

                //MARK : operation
                brain.loadPosting(classLogin.getId().toString(), arrayListPosting);
                //endregion

                //region UI THREAD
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {

                        //region progresbar loading
                        //MARK : UI
                        final ProgressBar loading = view.findViewById(R.id.loading);

                        //MARK : operation
                        loading.setVisibility(View.INVISIBLE);
                        //endregion

                        //region Recyclerview
                        //MARK : UI
                        RecyclerView recyclerView = view.findViewById(R.id.rclberita);

                        //MARK : obj
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

                        //MARK : operation
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(adapterPosting);
                        recyclerView.setNestedScrollingEnabled(true);
                        //endregion

                        //region constrantnokonten
                        //MARK : UI
                        final ConstraintLayout constrantnokonten = view.findViewById(R.id.constrantnokonten);

                        //MARK : operation
                        if (arrayListPosting.size() < 1)
                            constrantnokonten.setVisibility(View.VISIBLE);
                        else
                            constrantnokonten.setVisibility(View.INVISIBLE);

                        //endregion
                    }
                });
                //endregion
            }
        });

        oncreate = true;

        //region load postingan
        //MARK : obj

        //MARK : function
//        new loadBerita(arrayListPosting, view, adapterPosting, classKomentars).execute(classLogin.getId().toString());
        //endregion

//        final ArrayList<classPosting> classPostings = storage.get("mymasjidprefs", null);
//        Log.e("mymasjidlog","arraylitsposting = "+String.valueOf(classPostings.size()));

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

//        RecyclerView recyclerView = view.findViewById(R.id.rclberita);
//        lastFirstVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!oncreate) {

            //region show loading
            //MARK : UI
            final ProgressBar loading = view.findViewById(R.id.loading);

            //MARK : operation
            loading.setVisibility(View.VISIBLE);
            //endregion

            //region async
            //MARK : operation
            new AsyncJob.AsyncJobBuilder<Boolean>();
            AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
                @Override
                public void doOnBackground() {

                    //region refreshData
                    //MARK : operation
                    arrayListPosting.clear();
                    refreshData(classLogin.getId().toString());
                    //endregion

                    //MARK : obj
                    final adapterPosting adapterPosting = new adapterPosting(context, arrayListPosting, classLogin, masjid, false);

                    //region UI THREAD
                    //MASK : operation
                    AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                        @Override
                        public void doInUIThread() {

                            //region refresh recyclerview
                            //MARK : UI
                            RecyclerView recyclerView = view.findViewById(R.id.rclberita);

                            brainLog brainLog = new brainLog();
                            brainLog.Log("frag berita", "lastfirstvisible", String.valueOf(lastFirstVisiblePosition));

                            //MARK : operation
                            recyclerView.setAdapter(adapterPosting);
                            recyclerView.invalidate();
                            if (lastFirstVisiblePosition != -1)
                                ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(lastFirstVisiblePosition, 0);
                            lastFirstVisiblePosition = 0;
                            //endregion

                            //region hide loading
                            //MARK : operation
                            loading.setVisibility(View.INVISIBLE);
                            //endregion
                        }
                    });
                    //endregion
                }
            });
            //endregion

        } else {
            oncreate = false;
        }

    }

    private class loadBerita extends AsyncTask<String, String, String> {

        //region load berita async
        //region set variabel
        //MARK : obj
        final ArrayList<classPosting> arrayListPosting;
        final View view;
        final adapterPosting adapterPosting;
        final ArrayList<String> idkomens;

        //MARK : construct
        public loadBerita(ArrayList<classPosting> arrayListPosting, View view, adapterPosting adapterPosting, ArrayList<String> idkomens) {
            this.arrayListPosting = arrayListPosting;
            this.view = view;
            this.adapterPosting = adapterPosting;
            this.idkomens = idkomens;
        }
        //endregion

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... data) {

            //region load posting
            //MARK : obj
            brainBerita brain = new brainBerita();

            //MARK : operation
            brain.loadPosting(data[0], arrayListPosting);
            //endregion
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //region matikan loading dan set recyclerview
            //MARK : UI
            final ProgressBar loading = view.findViewById(R.id.loading);
            final RecyclerView recyclerView = view.findViewById(R.id.rclberita);

            //MARK : obj
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());

            //MARK : operation
            loading.setVisibility(View.INVISIBLE);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapterPosting);
            recyclerView.setNestedScrollingEnabled(true);
            //endregion
        }
        //endregion
    }

    private void refreshData(String idp) {

        //MARK : obj
        final brainBerita brain = new brainBerita();

        //MARK : operation
        brain.loadPosting(idp, arrayListPosting);
    }
}
