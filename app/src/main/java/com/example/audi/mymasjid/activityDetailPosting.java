package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class activityDetailPosting extends AppCompatActivity {

    ArrayList<classKomentar> classKomentars;
    adapterKomentar adapterKomentar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_posting);

        //MARK : obj
        final brainKomentar brain = new brainKomentar();
        final Intent intent = getIntent();
        final String statusId, userId, namaMasjid, topik;
        //region String
        statusId = intent.getStringExtra("statusid");
        userId = intent.getStringExtra("userid");
        namaMasjid = intent.getStringExtra("namaMasjid");
        topik = intent.getStringExtra("topik");
        //endregion

        //region ibtn like
        //MARK : UI
        final ImageButton ibtnLike = findViewById(R.id.ibtnLike);

        //MARK : operation
        if (brain.cekLike(userId, statusId)) {
            ibtnLike.setSelected(true);
            ibtnLike.setBackgroundResource(R.drawable.ic_thumb_up_white_24dp);
        } else {
            ibtnLike.setSelected(false);
        }

        //MARK : trigger
        ibtnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //MARK : operation
                if (ibtnLike.isSelected()) {
                    if (brain.dislike(userId, statusId)) {
                        ibtnLike.setBackgroundResource(R.drawable.ic_thumb_up_black_24dp);
                        ibtnLike.setSelected(false);
                    }

                } else {
                    if (brain.like(userId, statusId)) {
                        ibtnLike.setSelected(true);
                        ibtnLike.setBackgroundResource(R.drawable.ic_thumb_up_white_24dp);
                    }
                }
            }
        });
        //endregion

        //region img post
        //MARK : UI
        final ImageView imgPost = findViewById(R.id.imgPost);

        //MARK : obj
        final String url = "http://103.52.146.34/mymasjid/masjid/Foto_Status/" + statusId + ".jpg";

        //MARK : operation
        Glide.with(this).load(url).thumbnail(0.1f).into(imgPost);

        //MARK : trigger
        imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MARK : OBJ
                Intent i = new Intent(activityDetailPosting.this, activityDetailGambar.class);
                i.putExtra("url", url);

                //MARK : operation
                startActivity(i);
            }
        });
        //endregion

        //region set foto profil masjid
        //MARK : UI
        ImageView imgFotoMasjid = findViewById(R.id.imgFotoMasjid);

        //MARK : obj
        final String masjidId = intent.getStringExtra("masjidId");
        final String urlprof = "http://103.52.146.34/mymasjid/Foto_Masjid/" + masjidId + ".jpg";

        //MARK : operation
        Glide.with(this).load(urlprof).thumbnail(0.1f).into(imgFotoMasjid);
        //endregion

        //region set nama masjid
        //MARK : UI
        final TextView txtNamaMasjid = findViewById(R.id.txtNamaMasjid);

        //MARK : operation
        txtNamaMasjid.setText(namaMasjid);
        //endregion

        //region txtTopik
        //MARK : UI
        final TextView txtTopik = findViewById(R.id.txtTopik);

        //MARK : operation
        txtTopik.setText("Topik : "+topik);
        //endregion

        //region set status postingan
        //MARK : UI
        final TextView txtStatus = findViewById(R.id.txtStatus);
        String status = intent.getStringExtra("status");

        txtStatus.setText("\n" + status);
        //endregion

        //region async load komen
        //MARK : operation
        new AsyncJob.AsyncJobBuilder<Boolean>();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                classKomentars = new ArrayList<>();
                classKomentars = brain.loadKomentar(statusId, classKomentars);

                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {

                       adapterKomentar = new adapterKomentar(activityDetailPosting.this, classKomentars, userId, masjidId, intent.getExtras().getBoolean("masjid"));

                        //region ser recyclerview
                        //MARK : UI
                        final RecyclerView rclKomen = findViewById(R.id.rclKomen);
                        final LinearLayoutManager llm = new LinearLayoutManager(activityDetailPosting.this);

                        //MARK : operation
                        rclKomen.setLayoutManager(llm);
                        rclKomen.setHasFixedSize(false);
                        rclKomen.setAdapter(adapterKomentar);
                        rclKomen.setNestedScrollingEnabled(false);
                        //endregion

                        //region btn send
                        //koneksi ke tombol kirim
                        //MARK : UI
                        final ImageButton ibtnSend = findViewById(R.id.ibtnSend);

                        //MARK : trigger
                        ibtnSend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //koneksi ke kolom komentar
                                //MARK : UI
                                final EditText txtKomentar = findViewById(R.id.txtKomentar);
                                final NestedScrollView scrPosting = findViewById(R.id.scrPosting);

                                //persiapan variabel
                                //MARK : obj
                                final String komen, username;
                                //region String
                                komen = txtKomentar.getText().toString();
                                username = intent.getStringExtra("username");
                                //endregion

                                //cek kolom komentar
                                //MARK : operation
                                if (!komen.equals("")) {

                                    //tambahkan komentar di database dan di aplikasi
                                    String idkomen = brain.komentar(komen, userId, statusId);
                                    if (!idkomen.equals(null)) {

                                        Log.e("mymasjidlog","idkomen setelah input = "+idkomen);

                                        //MARK : operation
                                        classKomentars.add(new classKomentar(username, komen, userId, idkomen));
                                        adapterKomentar.notifyItemChanged(classKomentars.size());
                                        scrPosting.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                scrPosting.fullScroll(scrPosting.FOCUS_DOWN);
                                            }
                                        });
                                        txtKomentar.setText("");
                                    }
                                }else
                                    alert("mohon mengisi kolom komentar terlebihdahulu");
                            }
                        });
                        //endregion
                    }
                });
            }
        });
        //endregion
    }

    private void alert(String msg) {

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(activityDetailPosting.this);

        //MARK : operation
        builder.setMessage(msg).show();
    }
}
