package com.example.audi.mymasjid;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;
import com.pixplicity.easyprefs.library.Prefs;

public class activityEditProfilMasjid extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil_masjid);

        //region semi global variable
        //MARK : obj
        final Bundle bundle = getIntent().getExtras();
        final classLogin classLogin = bundle.getParcelable("classLogin");
        final Context context = activityEditProfilMasjid.this;
        final Resources resources = context.getResources();

        //MARK : UI
        final EditText edtAlamatMasjid, edtKontakMasjid,edtEmail,edtUsername;
        //region Edittext
        edtAlamatMasjid = findViewById(R.id.edtAlamatMasjid);
        edtKontakMasjid = findViewById(R.id.edtKontakMasjid);
        edtUsername = findViewById(R.id.edtUsername);
        edtEmail = findViewById(R.id.edtEmail);
        //endregion
        //endregion

        //region set data ke UI
        //MARK : obj
        final classMasjid classMasjid = bundle.getParcelable("classMasjid");

        //MARK : operation
        edtUsername.setText(classLogin.getUsername());
        edtEmail.setText(classLogin.getEmail());
        edtAlamatMasjid.setText(classMasjid.getAlamat());
        edtKontakMasjid.setText(classMasjid.getKontak());
        //endregion

        //region btn edit profil
        //MARK : UI
        final ImageButton btnEditProfil = findViewById(R.id.btnEditProfil);

        //MARK : operation
        btnEditProfil.setTag(true);

        //MARK : trigger
        btnEditProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Boolean.valueOf(String.valueOf(btnEditProfil.getTag()))) {

                    //enable editteks
                    edtUsername.setFocusableInTouchMode(true);
                    edtEmail.setFocusableInTouchMode(true);
                    changeButton(btnEditProfil, resources.getDrawable(R.color.green), resources.getDrawable(R.drawable.ic_save_black_24dp));
                    btnEditProfil.setTag(false);
                } else {

                    //MARK : obj
                    final classLogin classLoginTemp = new classLogin(edtUsername.getText().toString(), "", classLogin.getId(), edtEmail.getText().toString(), "", classLogin.getIdMasjid(), "","","");
                    final brainProfil brainProfil = new brainProfil();

                    //MARK : operation
                    if (brainProfil.updateProfilMasjid(classLoginTemp)) {

                        //region notifikasi user
                        //MARK : function
                        alert("Data berhasil di ubah", context);
                        //endregion

                        //region paksa user login lagi
                        //MARK : obj
                        new Prefs.Builder().setContext(activityEditProfilMasjid.this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();
                        Intent i = new Intent(getApplicationContext(), activityLogin.class);

                        //MARK : operation
                        Prefs.putString("akun", "");
                        startActivity(i);
                        finish();
                        //endregion

                    } else {
                        alert("Pengubahan data gagal, mohonperiksa koneksi internet anda", context);
                    }

                    //MARK : function
                    //disable editteks
                    disableEditProfil(edtUsername, edtEmail);

                    //MARK : operation
                    btnEditProfil.setTag(true);
                    changeButton(btnEditProfil, resources.getDrawable(R.color.blue), resources.getDrawable(R.drawable.ic_edit_white_24dp));
//                    btnEditProfil.setText("edit");
                }
            }
        });
        //endregion

        //region btn edit masjid
        //MARK : UI
        final ImageButton btnEditMasjid = findViewById(R.id.btnEditMasjid);

        //MARK : operation
        btnEditMasjid.setTag(true);

        //MARK : trigger
        btnEditMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Boolean.valueOf(String.valueOf(btnEditMasjid.getTag()))) {

                    //MARK : operation
                    //enable editteks
                    edtAlamatMasjid.setFocusableInTouchMode(true);
                    edtKontakMasjid.setFocusableInTouchMode(true);
                    btnEditMasjid.setTag(false);
                    changeButton(btnEditMasjid, resources.getDrawable(R.color.green), resources.getDrawable(R.drawable.ic_save_black_24dp));
//                    btnEditMasjid.setText("simpan");
                } else {

                    //region set UI
                    //MARK : function
                    //disable editteks
                    disableEditMasjid(edtAlamatMasjid,edtKontakMasjid);

                    //MARK : operation
//                    btnEditMasjid.setText("edit");
                    changeButton(btnEditMasjid, resources.getDrawable(R.color.blue), resources.getDrawable(R.drawable.ic_edit_white_24dp));
                    btnEditMasjid.setTag(true);
                    //endregion

                    //region post data + notif
                    //MARK : obj
                    brainMasjid brainMasjid = new brainMasjid();

                    //MARK : operation
                    if (brainMasjid.updateProfilMasjid(classLogin.getIdMasjid(), edtUsername.toString(), edtAlamatMasjid.getText().toString(), edtKontakMasjid.getText().toString())) {
                        alert("Data berhasil di ubah", context);
                    } else {
                        alert("Pengubahan data gagal, mohonperiksa koneksi internet anda", context);
                    }
                    //endregion
                }
            }
        });
        //endregion

        //region set jenis akun
        //MARK : UI
        final TextView txtJenisAkun = findViewById(R.id.txtJenisAkun);

        //MARK : operation
        txtJenisAkun.setText(classLogin.getJenis());
        //endregion

        //region disable button
        //MARK : disable edittext
        disableEditProfil(edtUsername, edtEmail);
        disableEditMasjid(edtAlamatMasjid,edtKontakMasjid);
        //endregion

    }

    public void Log(String msg) {
        Log.e("mymasjidlog", msg);
    }

    public void alert(String msg, Context context) {

        //MARK : obj
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //MARK : operation
        builder.setMessage(msg).show();
    }

    public void disableEditProfil(EditText edtUsername, EditText edtEmail) {
        edtUsername.setFocusable(false);
        edtEmail.setFocusable(false);
    }

    public void disableEditMasjid(EditText edtAlamatMasjid, EditText edtKontakMasjid) {
        //MARK : operation
        edtAlamatMasjid.setFocusable(false);
        edtKontakMasjid.setFocusable(false);
    }

    private void changeButton(ImageButton button, Drawable bg, Drawable icon){
        button.setBackground(bg);
        button.setImageDrawable(icon);
    }
}
