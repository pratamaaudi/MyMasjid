package com.example.audi.mymasjid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Audi on 7/26/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<Posting> p;

    public NewsAdapter(
            Context context,
            ArrayList<Posting> p
    ) {
        this.context = context;
        this.p = p;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(
            ViewGroup parent,
            int viewType
    ) {

        View v = LayoutInflater.
                from(parent.getContext())
                .inflate(
                        R.layout.newslist_layout,
                        parent, false
                );

        final RecyclerView.ViewHolder mViewHolder = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(
            RecyclerView.ViewHolder holder,
            final int position
    ) {

        ImageView img = (ImageView) holder
                .itemView
                .findViewById(R.id.img);

        img.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(
                                context,
                                activityDetailMasjid.class
                        );

                        i.putExtra(
                                "id",
                                p.get(position).getId()
                        );

                        i.putExtra(
                                "judul",
                                p.get(position).getNamaMasjid()
                        );

                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }
                }
        );

        TextView namaMasjid = (TextView) holder
                .itemView
                .findViewById(R.id.namaMasjid);

        namaMasjid.setText(p
                .get(position)
                .getNamaMasjid()
        );

        TextView status = (TextView) holder
                .itemView
                .findViewById(R.id.status);

        status.setText(p
                .get(position)
                .getStatus()
        );
    }

    @Override
    public int getItemCount() {
        return p.size();
    }
}
