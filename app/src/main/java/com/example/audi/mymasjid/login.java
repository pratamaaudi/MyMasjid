package com.example.audi.mymasjid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class login extends AppCompatActivity {

    Button btnLogin, btnDaftar;
    TextView txtUsername, txtPass, txtgagallogin;
    brain_login brain_login;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initvalue();

        setlistener();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void initvalue() {

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);

        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtPass = (TextView) findViewById(R.id.txtPass);
        txtgagallogin = (TextView) findViewById(R.id.txtgagallogin);

        brain_login = new brain_login();

        progressDialog = new ProgressDialog(login.this);
        progressDialog.setMessage("Checking. . .");
        progressDialog.setCancelable(false);
    }

    public void setlistener() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();

                if (brain_login.login(
                        txtUsername
                                .getText()
                                .toString(),
                        txtPass
                                .getText()
                                .toString()
                )) {
                    /*progressDialog.dismiss();*/

                    Intent i;
                    i = new Intent(getApplicationContext(), MainActicity_alt.class);

                    startActivity(i);

                } else {
                    txtgagallogin.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                }
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i;
                i = new Intent(getApplicationContext(), Daftar.class);

                startActivity(i);
            }
        });
    }
}
