package com.example.audi.mymasjid;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class jadwal_januari extends Fragment {

    ArrayList<classJadwal_old> arraylistClassJadwalOld;
    LinearLayoutManager llm;
    recyclerAdapterJadwal recyclerAdapterJadwal;
    RecyclerView rclJadwalJanuari;
    View view;
    String bulan;


    public jadwal_januari() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(
                R.layout.fragment_jadwal_januari, container, false
        );

        //MARK : method init
        initvalue();
        isiarray();

        //MARK : set recyclerview
        rclJadwalJanuari.setLayoutManager(llm);
        rclJadwalJanuari.setHasFixedSize(true);
        rclJadwalJanuari.setAdapter(recyclerAdapterJadwal);
        rclJadwalJanuari.setNestedScrollingEnabled(false);

        // Inflate the layout for this fragment
        return view;
    }

    public void initvalue() {

        //MARK : Object
        arraylistClassJadwalOld = new ArrayList<>();
        recyclerAdapterJadwal = new recyclerAdapterJadwal(
                getContext(), arraylistClassJadwalOld
        );
        llm = new LinearLayoutManager(getContext());
        bulan = getArguments().getString("bulan");

        //MARK : UI
        rclJadwalJanuari = view.findViewById(R.id.rclJadwalJanuari);
    }

    public void isiarray() {
        if (bulan.equals("januari")) {

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat", "12 September 2017", "Baharudin"
            ));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));

            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Baharudin"));
        } else {
            arraylistClassJadwalOld.add(new classJadwal_old(
                    "Shalat Jumat",
                    "12 September 2017",
                    "Muslim"));
        }
    }
}
