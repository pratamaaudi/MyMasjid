package com.example.audi.mymasjid;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

public class activityEditMasjid extends AppCompatActivity {

    ArrayList<classMasjid> classMasjids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_masjid);

        //MARK : obj
        classMasjids = new ArrayList<>();

        new loadMasjid().execute();
    }

    @Override
    public void onBackPressed() {

        //MARK : obj
        final Intent intent = new Intent();

        //MARK : operation
        this.setResult(Activity.RESULT_CANCELED, intent);
        this.finish();
    }

    private class loadMasjid extends AsyncTask<String, String, String> {

        //koneksikan dengan UI loading
        //MARK : UI
        final ProgressBar loading =  findViewById(R.id.loading);

        //persiapkan data yang dibutuhkan
        //MARK : obj
        final Intent intent = getIntent();
        final Integer requestCode = intent.getIntExtra("requestCode", 000);

        //kode yang jalan sebelum memuat data
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //munculkan loading
            loading.setVisibility(View.VISIBLE);
        }

        //proses memuat data
        @Override
        protected String doInBackground(String... data) {

            //buat class brain masjid
            //MARK : obj
            final brainMasjid brainMasjid = new brainMasjid();

            //cek requestcode yang diterima
            //MARK : operation
            switch (requestCode) {
                case 001:

                    //load semua daftar masjid
                    classMasjids = brainMasjid.loadMasjid(classMasjids);
                    break;
                case 002:

                    //load daftar masjid yang diikuti pengguna
                    //MARK : obj
                    classMasjids = brainMasjid.loadFollowMasjid(classMasjids, intent.getStringExtra("userId"));
                    break;
                default:
                    break;
            }
            return null;
        }

        //proses setelah memuat data
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //koneksikan dengan UI recyclerview
            //MARK : UI
            final RecyclerView rclMasjid = findViewById(R.id.rclMasjid);

            //persiapkan data yang dibutuhkan recyclerview
            //MARK : obj
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activityEditMasjid.this);
            final adapterEditMasjid adapterEditMasjid = new adapterEditMasjid(activityEditMasjid.this, classMasjids, activityEditMasjid.this, requestCode);

            //setting recyclerview untuk menampilkan konten
            //MARK : operation
            rclMasjid.setLayoutManager(linearLayoutManager);
            rclMasjid.setHasFixedSize(true);
            rclMasjid.setAdapter(adapterEditMasjid);
            loading.setVisibility(View.INVISIBLE);
        }
    }
}
