package com.example.audi.mymasjid;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;

import java.util.ArrayList;

public class jadwal extends AppCompatActivity {

    public TabLayout tab;
    ArrayList<classJadwal_old> arraylistClassJadwalOld;
    //RecyclerView rclJadwalJumatan;
    LinearLayoutManager llm;
    recyclerAdapterJadwal recyclerAdapterJadwal;
    TextView txtJudulJadwal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal_old);

        initvalue();

        isiarray();

        recyclerAdapterJadwal = new recyclerAdapterJadwal(
                getApplicationContext(),
                arraylistClassJadwalOld);

        setlistener();

        setfragment("januari");
    }

    public void initvalue() {
        arraylistClassJadwalOld = new ArrayList<classJadwal_old>();
        tab = (TabLayout) findViewById(R.id.tabs);
        txtJudulJadwal = (TextView) findViewById(R.id.txtjuduljadwal);
    }

    public void isiarray() {
        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));
    }

    public void setfragment(String konten) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(
                R.anim.fade_in,
                R.anim.fade_out
        );

        switch (konten) {
            case "januari":
                jadwal_januari jadwal_januari = new jadwal_januari();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_januari
                        )
                        .commit();
                break;
            case "februari":
                jadwal_februari jadwal_februari = new jadwal_februari();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_februari
                        )
                        .commit();
                break;
            case "maret":
                jadwal_maret jadwal_maret = new jadwal_maret();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_maret
                        )
                        .commit();
                break;
            case "april":
                jadwal_april jadwal_april = new jadwal_april();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_april
                        )
                        .commit();
                break;
            case "mei":
                jadwal_mei jadwal_mei = new jadwal_mei();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_mei
                        )
                        .commit();
                break;
            case "juni":
                jadwal_juni jadwal_juni = new jadwal_juni();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_juni
                        )
                        .commit();
                break;
            case "juli":
                jadwal_juli jadwal_juli = new jadwal_juli();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_juli
                        )
                        .commit();
                break;
            case "agustus":
                jadwal_agustus jadwal_agustus = new jadwal_agustus();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_agustus
                        )
                        .commit();
                break;
            case "september":
                jadwal_september jadwal_september = new jadwal_september();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_september
                        )
                        .commit();
                break;
            case "oktober":
                jadwal_oktober jadwal_oktober = new jadwal_oktober();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_oktober
                        )
                        .commit();
                break;
            case "november":
                jadwal_november jadwal_november = new jadwal_november();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_november
                        )
                        .commit();
                break;
            case "desember":
                jadwal_desember jadwal_desember = new jadwal_desember();

                transaction
                        .replace(
                                R.id.llkonten,
                                jadwal_desember
                        )
                        .commit();
        }
    }

    public void setlistener() {
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:

                        setfragment("januari");

                        break;
                    case 1:

                        setfragment("februari");

                        break;
                    case 2:

                        setfragment("maret");

                        break;
                    case 3:

                        setfragment("april");

                        break;
                    case 4:

                        setfragment("mei");

                        break;
                    case 5:

                        setfragment("juni");

                        break;
                    case 6:

                        setfragment("juli");

                        break;
                    case 7:

                        setfragment("agustus");

                        break;
                    case 8:

                        setfragment("september");

                        break;
                    case 9:

                        setfragment("oktober");

                        break;
                    case 10:

                        setfragment("november");

                        break;
                    case 11:

                        setfragment("desember");

                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
