package com.example.audi.mymasjid;

/**
 * Created by MOFOSEFES on 10/15/2017.
 */

public class classKomentar {
    private String username;
    private String komentar;
    private String profil_id;
    private String idkomen;

    public classKomentar(String username, String komentar, String profil_id, String idkomen) {
        setUsername(username);
        setKomentar(komentar);
        setProfil_id(profil_id);
        setIdkomen(idkomen);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getProfil_id() {
        return profil_id;
    }

    public void setProfil_id(String profil_id) {
        this.profil_id = profil_id;
    }

    public String getIdkomen() {
        return idkomen;
    }

    public void setIdkomen(String idkomen) {
        this.idkomen = idkomen;
    }
}
