package com.example.audi.mymasjid;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class activityPengurusMasjid extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengurus_masjid);

        //MARK : obj
        Context context = activityPengurusMasjid.this;

        //region isi array pengurus
        //MARK : OBJ
        brainMasjid brainMasjid = new brainMasjid();
        ArrayList<classPengurusMasjid> classPengurusMasjids = new ArrayList<>();
        String idMasjid = getIntent().getExtras().getString("id");

        //MARK : Operation
        brainMasjid.loadPengurusMasjid(classPengurusMasjids, idMasjid);
        //endregion

        //region set recycler view
        //MARK : obj
        adapterPengurusMasjid adapterPengurusMasjid = new adapterPengurusMasjid(classPengurusMasjids,context, getIntent().getExtras().getBoolean("masjid"));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        //MARK : UI
        RecyclerView rclKont = findViewById(R.id.rclKont);

        //MARK : operation
        rclKont.setLayoutManager(linearLayoutManager);
        rclKont.setHasFixedSize(true);
        rclKont.setAdapter(adapterPengurusMasjid);
        //endregion
    }
}
