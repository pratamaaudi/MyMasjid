package com.example.audi.mymasjid;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class jadwal_mei extends Fragment {

    ArrayList<classJadwal_old> arraylistClassJadwalOld;
    LinearLayoutManager llm;
    recyclerAdapterJadwal recyclerAdapterJadwal;
    RecyclerView rclJadwalMei;
    View view;

    public jadwal_mei() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_jadwal_mei, container, false);

        initvalue();

        isiarray();

        setRecyclerView();

        // Inflate the layout for this fragment
        return view;
    }

    public void initvalue() {
        arraylistClassJadwalOld = new ArrayList<>();
        recyclerAdapterJadwal = new recyclerAdapterJadwal(
                getContext(),
                arraylistClassJadwalOld
        );
        rclJadwalMei = view.findViewById(R.id.rclJadwalMei);
        llm = new LinearLayoutManager(getContext());
    }

    public void isiarray() {
        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "19 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "21 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));

        arraylistClassJadwalOld.add(new classJadwal_old(
                "Shalat Jumat",
                "12 September 2017",
                "Baharudin"));
    }

    public void setRecyclerView() {
        rclJadwalMei.setLayoutManager(llm);
        rclJadwalMei.setHasFixedSize(true);
        rclJadwalMei.setAdapter(recyclerAdapterJadwal);
        rclJadwalMei.setNestedScrollingEnabled(false);
    }

}
